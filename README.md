# CHARMM to GROMACS Force Field Converter

Note: This is work in progress. Do not use this code, not even at your own
risk! The resulting force field is not guaranteed to be correct at all.

On-line documentation: https://awacha.gitlab.io/charmm2gmx
