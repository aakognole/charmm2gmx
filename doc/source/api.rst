.. coding=utf-8
.. _api:

Charmm2GMX Internals
====================

Please find the API documentation under the links below.

.. autosummary::
    :recursive:
    :toctree: api

    charmm2gmx
