.. coding=utf-8
.. _backgroundinfo:

Some Background Information
===========================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    backgroundinfo/differences
    backgroundinfo/forcefieldorganization
    backgroundinfo/topology
    backgroundinfo/charmm2gmx_input



