.. coding=utf-8
.. _quickstart:

Quickstart
==========

- :doc:`Install <installation>` the package
- Download the CHARMM force field distribution (toppar files) from the
  `official site <http://mackerell.umaryland.edu/charmm_ff.shtml>`_
- Construct a :doc:`charmm2gmx.in <../backgroundinfo/charmm2gmx_input>` input file
- Run :doc:`charmm2gmx convert <cli/convert>`
- Find missing parameters and generate dummy parameters for dihedrals with
  :doc:`charmm2gmx findmissingpars <cli/findmissingpars>`. Copy the resulting `\*.itp` (created with switch `-d`) into
  the `.ff` directory and name it `ffmissingdihedrals.itp`. If some residue topologies produce errors (e.g. DT3, DU3,
  DG3, DC3, DA3, CYSL, GLYM, SAM...), it is typically because the program categorizes them incorrectly in terms of
  chainability (left, right, mid or stand-alone). Check the residue topology entry and try to mark them explicitely with
  the appropriate command-line switches (`-c`, `-n`, `-s` or `-m`).
- Generate hydrogen addition database with :doc:`charmm2gmx genhdb <cli/genhdb>`. See the above command if errors occur
  in some topologies.
- Compare with another force field distribution with :doc:`charm2gmx compare <cli/compare>`
