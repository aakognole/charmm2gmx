.. coding=utf-8
.. _cite:

How to Cite
===========

If you use this program directly or indirectly (e.g. carrying out MD simulations in GROMACS using a force field port made by it), we would be grateful if you could cite the following original article:

András F. Wacha and Justin A. Lemkul. "charmm2gmx: An Automated Method to Port the CHARMM Additive Force Field to GROMACS". Journal of Chemical Information and Modeling 2023. 63(14), 4246-4252, DOI: `10.1021/acs.jcim.3c00860 <https://doi.org/10.1021/acs.jcim.3c00860>`_


