.. coding=utf-8
.. _installation:

Installing Charmm2GMX
=====================

Prerequisites
-------------

- Python >= 3.8
- `https://click.palletsprojects.com <click>`_
- `https://github.com/borntyping/python-colorlog <colorlog>`_
- `https://github.com/pypa/setuptools_scm <setuptools_scm>`_
- `https://sphinx-doc.org <Sphinx>`_ for building the documentation

All of the above can be installed e.g. with the `https://conda.io <conda>`_ package and environment manager.

Installing from source
----------------------

At present the simplest way of installing is to check out the source distribution via git and build and install
using the standard `setup.py`:

.. code-block:: bash

    git clone https://gitlab.com/awacha/charmm2gmx.git
    cd charmm2gmx
    python setup.py build
    python setup.py install
    python setup.py build_sphinx  # build the documentation; optional

It is recommended that charmm2gmx is installed in its own virtual environment, e.g. with `conda`:

.. code-block:: bash

    conda create -n charmm2gmx python>=3.9 colorlog click setuptools_scm sphinx
    conda activate charmm2gmx

After this clone the repository and install it as above.
