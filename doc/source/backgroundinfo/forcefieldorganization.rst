.. coding: utf-8
.. _forcefield_organization:

Force Field Organization
========================

A molecular force field consists of three main parts:

- the potential function, describing the mathematical form of the interaction potential between all possible atom of the
  system
- parameter set: definitions of atom types and the numerical parameters to be used when calculating various terms of the
  interaction potential between these kinds of atoms
- residue topologies: non-volatile information on the building blocks of the simulated system, i.e. everything which
  does not change during a simple molecular dynamics simulation run, e.g. atom names and types, bonds, other
  interactions. The building blocks are molecules or "natural" subunits of larger molecules (monomers, amino acids,
  nucleotides etc.)

