.. coding: utf-8
.. _charmm2gmx_in:

CHARMM2GMX Input File
=====================

The single input to the :ref:`CHARMM to GROMACS conversion program <convert>` is the input file, typically named `charmm2gmx.in`. An excerpt of an example file follows:

.. code-block:: none
    :caption: charmm2gmx.in
    :name: charmm2gmx-in

    toppardir: toppar_c36_jul20
    outputdir: charmm36m-waf-2021.ff
    bibtexfile: citations.bib

    [ solvent ]
    +str: toppar_water_ions.str
    cite[TIP3P water]: jorgensenComparisonSimplePotential1983
    cite[Ions by Roux et. al.]: beglovFiniteRepresentationInfinite1994
    cite[Zinc ion]: stoteZincBindingProteins1995
    copyresidues: TIP3 -> HOH
    renameatoms[HOH]: OH2 -> OW H1 -> HW1 H2 -> HW2

    [ ethers ]
    prm: par_all35_ethers.prm
    cite: vorobyovAdditiveClassicalDrude2007
    cite: leeMolecularDynamicsStudies2008

    rtf: top_all35_ethers.rtf
    cite: vorobyovAdditiveClassicalDrude2007
    cite: leeMolecularDynamicsStudies2008

The file is divided into sections, each section corresponding to a molecule type (loosely following the CHARMM nomenclature: prot, na, ethers etc.). Section headers are marked by square brackets (e.g. "[ section ]"). Other lines are either empty or have the format `<command>[<command modifier>]: <command arguments>`. The following commands are recognized:

`rtf`:
    the command argument is a single file name relative to `toppardir`. Residue topologies are read from this file.
`prm`:
    the command argument is a single file name relative to `toppardir`. Interaction parameters are read from this file.
`str`:
    the command argument is a single file name relative to `toppardir`. It is a stream file, `charmm2gmx` will look for `READ RTF CARD...` and `READ PARAM CARD...` lines. Note that the reader is rudimentary, it does not understand more advanced CHARMM constructs (e.g. if, parameter substitutions etc.).

Each of the above commands can be modified by either '+' or '-' prefixed to the command (e.g. '+rtf', '-prm', ...), to
specify how to handle redefined topologies or parameters. If a residue topology or a parameter has been already read
from a previous file and it is defined differently in another file with neither '+' nor '-' prefix, it is considered an
error, aborting the conversion process. If '+' is specified, entries read from that file overwrite previously read
entries. '-' works the other way: already existing entries take precedence over new ones. In either case, a warning
message is printed.

Some files (e.g. `toppar_water_ions.str`) have parameters defined twice for different reasons. Without any prefix, trying to read these files will abort the conversion. Prefixing with '+' or '-' is a crude way to handle this situation.

Apart from the three main commands specifying the file names, the following ones are also understood. They work either on the data read from the previously specified files, or on all data read up to the point:

+--------------------------+--------------+-----------------------------------+----------------------------------------+
| Command                  | Works on     | argument scheme                   | Description                            |
+==========================+==============+===================================+========================================+
| patch                    | all data     | <base> + <pres> = <patchedname>   | Apply the patch residue to a base      |
|                          |              |                                   | residue topology and save the thus     |
|                          |              |                                   | created residue topology under the     |
|                          |              |                                   | given name                             |
+--------------------------+--------------+-----------------------------------+----------------------------------------+
| nter[baseresidue]        | current file | <pres> ...                        | Convert the named patch residues to    |
|                          |              |                                   | N-termini (for the `*.n.tdb` files).   |
|                          |              |                                   | The named base residue will be used    |
|                          |              |                                   | for topology analysis and atom         |
|                          |              |                                   | addition rule generation               |
+--------------------------+--------------+-----------------------------------+----------------------------------------+
| cter[baseresidue]        | current file | <pres> ...                        | Convert the named patch residues to    |
|                          |              |                                   | C-termini (for the `*.c.tdb` files).   |
|                          |              |                                   | The named base residue will be used    |
|                          |              |                                   | for topology analysis and atom         |
|                          |              |                                   | addition rule generation               |
+--------------------------+--------------+-----------------------------------+----------------------------------------+
| cite[description]        | current file | <citekey> or "formatted citation" | Append a citation to the current file. |
|                          |              |                                   | This information will be used when     |
|                          |              |                                   | generating the `forcefield.doc` file.  |
|                          |              |                                   | If a citation key is given (any text   |
|                          |              |                                   | without double quotes), the            |
|                          |              |                                   | corresponding BibTeX entry will be     |
|                          |              |                                   | looked up. If the command argument is  |
|                          |              |                                   | surrounded by double quotes, the text  |
|                          |              |                                   | within quotes will be used as is.      |
|                          |              |                                   |                                        |
+--------------------------+              |                                   | The `description` gives context to the |
| cite                     |              |                                   | citation (e.g. Neutral arginine ). If  |
|                          |              |                                   | not given, the citation belongs to the |
|                          |              |                                   | whole file.                            |
+--------------------------+--------------+-----------------------------------+----------------------------------------+
| renameresidues           | current file | <old> -> <new> ...                | Give a residue topology a new name.    |
|                          |              |                                   | Multiple arguments can be specified,   |
|                          |              |                                   | separated by spaces.                   |
+--------------------------+--------------+-----------------------------------+----------------------------------------+
| copyresidues             | current file | <old> -> <new> ...                | The same as `renameresidues`, but      |
|                          |              |                                   | keeps the original entry as well.      |
+--------------------------+--------------+-----------------------------------+----------------------------------------+
| discardresidues          | current file | <resname> ...                     | Space separated names of the residues  |
|                          |              |                                   | which won't be read from the file      |
+--------------------------+--------------+-----------------------------------+----------------------------------------+
| overrideresidues         | current file | <resname> ...                     | Space separated names of the residues  |
|                          |              |                                   | which will be overridden if already    |
|                          |              |                                   | present from a previous file           |
+--------------------------+--------------+-----------------------------------+----------------------------------------+
| renameatomtypes          | current file | <old> -> <new> ...                | Renames atom types read from the       |
|                          |              |                                   | current file. One should specify this  |
|                          |              |                                   | for the topology and the parameter     |
|                          |              |                                   | files too.                             |
+--------------------------+--------------+-----------------------------------+----------------------------------------+
| renameatoms[residuename] | all data     | <old> -> <new> ...                | Give new name to atoms in the          |
|                          |              |                                   | specified residue topology.            |
+--------------------------+--------------+-----------------------------------+----------------------------------------+

The source distribution of `charmm2gmx` contains an example `charmm2gmx.in` file, which gives nearly the same output
as the original CHARMM -> GROMACS port found on the
`website of Alexander D. MacKerell's group <http://mackerell.umaryland.edu/charmm_ff.shtml>`_.