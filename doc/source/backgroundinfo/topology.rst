.. coding: utf-8

Residue Topology
================

One of the two main parts of a force field is a set of *residue topologies*. Simply put, the term means all *stable*,
*non-changing* information on a molecule or a part of it, i.e. what does not change during a classical molecular
dynamics simulation. Atom names, types, partial charges, bonded interactions belong to this category. Topology does not
know about atom positions, velocities or forces.

In CHARMM, topology is also well-separated from more general, residue-independent interaction parameters. In the
topology, interactions such as bond length vibration, angle bending, torsion etc. are only *declared*, it is the MD
engine's job to assign the interaction function and its parameters based on the types of the participating atoms.

Residue Topologies in CHARMM
----------------------------

Most CHARMM residue topologies are stored in `\*.rtf` files. An example entry follows::

    RESI ALA          0.00
    GROUP
    ATOM N    NH1    -0.47  !     |
    ATOM HN   H       0.31  !  HN-N
    ATOM CA   CT1     0.07  !     |     HB1
    ATOM HA   HB1     0.09  !     |    /
    GROUP                   !  HA-CA--CB-HB2
    ATOM CB   CT3    -0.27  !     |    \
    ATOM HB1  HA3     0.09  !     |     HB3
    ATOM HB2  HA3     0.09  !   O=C
    ATOM HB3  HA3     0.09  !     |
    GROUP                   !
    ATOM C    C       0.51
    ATOM O    O      -0.51
    BOND CB CA  N  HN  N  CA
    BOND C  CA  C  +N  CA HA  CB HB1  CB HB2  CB HB3
    DOUBLE O  C
    IMPR N -C CA HN  C CA +N O
    CMAP -C  N  CA  C   N  CA  C  +N
    DONOR HN N
    ACCEPTOR O C
    IC -C   CA   *N   HN    1.3551 126.4900  180.0000 115.4200  0.9996
    IC -C   N    CA   C     1.3551 126.4900  180.0000 114.4400  1.5390
    IC N    CA   C    +N    1.4592 114.4400  180.0000 116.8400  1.3558
    IC +N   CA   *C   O     1.3558 116.8400  180.0000 122.5200  1.2297
    IC CA   C    +N   +CA   1.5390 116.8400  180.0000 126.7700  1.4613
    IC N    C    *CA  CB    1.4592 114.4400  123.2300 111.0900  1.5461
    IC N    C    *CA  HA    1.4592 114.4400 -120.4500 106.3900  1.0840
    IC C    CA   CB   HB1   1.5390 111.0900  177.2500 109.6000  1.1109
    IC HB1  CA   *CB  HB2   1.1109 109.6000  119.1300 111.0500  1.1119
    IC HB1  CA   *CB  HB3   1.1109 109.6000 -119.5800 111.6100  1.1114

The above example defines the alanine residue, containing 10 atoms in three groups. Each atom has a unique name in the
residue (e.g. the first one is "N"), an atom type ("NH1", i.e. peptide nitrogen) and a partial charge (-0.47 electrons).
After this, bonds are listed, e.g. the first `BOND` line declares three chemical bonds: between atoms "CB" and "CA", "N"
and "HN" and "N" and "CA". Improper dihedrals (usually used for ensuring planarity) and dihedral correlation map
interactions are also listed explicitly. Angle and dihedral terms are seldom written here: they can be auto-generated
from the bond information. CHARMM has additional information on atoms which can participate in hydrogen bonds (`DONOR`
and `ACCEPTOR` lines), as well as a set of internal coordinates for building a molecular geometry from scratch. These
are not available in GROMACS.

CHARMM also recognizes so-called *patch residues*. These can be thought of as a set of modifications to be applied on
other residue topologies. For example the following patch modifies amino acids to have NH3 on the N-terminus::

    PRES NTER         1.00 ! standard N-terminus
    GROUP                  ! use in generate statement
    ATOM N    NH3    -0.30 !
    ATOM HT1  HC      0.33 !         HT1
    ATOM HT2  HC      0.33 !     (+)/
    ATOM HT3  HC      0.33 ! --CA--N--HT2
    ATOM CA   CT1     0.21 !   |    \
    ATOM HA   HB1     0.10 !   HA    HT3
    DELETE ATOM HN
    BOND HT1 N HT2 N HT3 N
    DONOR HT1 N
    DONOR HT2 N
    DONOR HT3 N
    IC HT1  N    CA   C     0.0000  0.0000  180.0000  0.0000  0.0000
    IC HT2  CA   *N   HT1   0.0000  0.0000  120.0000  0.0000  0.0000
    IC HT3  CA   *N   HT2   0.0000  0.0000  120.0000  0.0000  0.0000

Other patches work on two residues, e.g. the following one links to cysteines with a disulfide bond::

    PRES DISU        -0.36 ! patch for disulfides. Patch must be 1-CYS and 2-CYS.
                           ! use in a patch statement
                           ! follow with AUTOgenerate ANGLes DIHEdrals command
    GROUP
    ATOM 1CB  CT2    -0.10 !
    ATOM 1SG  SM     -0.08 !           2SG--2CB--
    GROUP                  !          /
    ATOM 2SG  SM     -0.08 ! -1CB--1SG
    ATOM 2CB  CT2    -0.10 !
    DELETE ATOM 1HG1
    DELETE ATOM 2HG1
    BOND 1SG 2SG
    IC 1CA  1CB  1SG  2SG   0.0000  0.0000  180.0000  0.0000  0.0000
    IC 1CB  1SG  2SG  2CB   0.0000  0.0000   90.0000  0.0000  0.0000
    IC 1SG  2SG  2CB  2CA   0.0000  0.0000  180.0000  0.0000  0.0000

Other patches also exist, e.g. for deprotonating or protonating an amino acid side-chain, converting a ribonucleotide to
its deoxy counterpart etc.

Residue Topologies in GROMACS
-----------------------------

Residue topologies are stored in `*.rtp` files in the GROMACS force field directories. The entry for alanine has the
following form::

    [ ALA ]
      [ atoms ]
            N   NH1   -0.470  0
           HN     H    0.310  1
           CA   CT1    0.070  2
           HA   HB1    0.090  3
           CB   CT3   -0.270  4
          HB1   HA3    0.090  5
          HB2   HA3    0.090  6
          HB3   HA3    0.090  7
            C     C    0.510  8
            O     O   -0.510  9
      [ bonds ]
           CB    CA
            N    HN
            N    CA
            C    CA
            C    +N
           CA    HA
           CB   HB1
           CB   HB2
           CB   HB3
            O     C
      [ impropers ]
            N    -C    CA    HN
            C    CA    +N     O
      [ cmap ]
           -C     N    CA     C    +N

Conversion of molecular topologies from CHARMM to GROMACS format looks therefore a straightforward procedure.

This is not the case, however. For starters, GROMACS does not implement the concept of patching.

For capping the start and the end of a chain (protein or nucleic acid), rules to add groups such as -NH3 or -COOH have
been implemented in `*.n.tdb` (start) and `*.c.tdb` (end) files in the force field directory. The entry for the
N-terminal NH3 group is for example::

    [ NH3+ ]
    [ replace ]
     N	 N	 NH3	 14.0070 -0.30
     CA  CA  CT1	 12.011   0.21
     HA  HA  HB1	 1.008	  0.10
    [ Add ]
     3	 4	 H		 N		 CA		 C
         HC  1.008	 0.33	 -1
    [ delete ]
     HN

More information on the structure of termini databse files can be found in the `official GROMACS documentation \
<https://manual.gromacs.org/current/reference-manual/topologies/pdb2gmx-input-files.html#termini-database>`_.

Other residue modifications (protonation, deprotonation) are supported by ready-made modified residue topologies. For
example, `ASPP` is a patch in CHARMM for protonating the side-chain of aspartic acid (`ASP`). In GROMACS, `ASPP` is a
residue in its own right, the protonated aspartic acid.

When two residues are patched together, the patched version of each residue is created in GROMACS, and an entry is added
to `specbond.dat \
<https://manual.gromacs.org/current/reference-manual/topologies/pdb2gmx-input-files.html#special-bonds>`_. To take the
example of disulfide linked cysteines, the residue `CYS2` in GROMACS is the `CYS` residue with the `DISU` patch applied.
The corresponding entry in `specbond.dat` reads::

    CYS     SG      1       CYS     SG      1       0.2     CYS2    CYS2

Meaning that the `SG` (γ sulfur) atom in CYS can make one special bond with another `SG` of another CYS, if they are
within 0.2 nm ± 10% distance from each other. If they are, both residues are renamed to CYS2.

.. _topologyanalysis:

Advanced Topology Analysis
--------------------------

As said above, converting residue topologies from CHARMM to GROMACS is straightforward. Hydrogen addition rules and
termini entries are more difficult, as the CHARMM force field files do not directly include the required information.
By looking at the actual value of interaction parameters, however, the local geometry around specific atoms can be
assessed.

In the `charmm2gmx` code, topology-related utilities are found in the :doc:`../api/charmm2gmx.residuetopology`
subpackage. Below we list the main functionalities.

Auto-Generating Bonded Interactions
...................................

In a typical residue topology, initially only atom and bond information is present, as well as improper dihedrals and
correlation map interactions. The other bonded interactions, namely angle bending and torsion potentials can be
auto-generated from bond information. At the core of things is the *bonded tree*: for each atom in the residue, all
possible paths of a specified maximum length, through existing bonds can be generated.

In bonded interactions, atoms in the residue topology are referenced by their names. Some residues, e.g. amino acids
contain references to atoms in the previous or the next residue. These atom names have either a '-' or a '+' prefix,
thus e.g. a bond between C and +N can be listed in the residue topology, or an improper dihedral on the atoms N, -C, CA
and HN. When generating angle and dihedral entries, an infinite chain of the same residues is assumed by default, thus
angles like C, +N, +CA or  -CA, -C, N and dihedrals like -C, N, CA, C or -CA, -C, N, HN are also listed. By convention,
only those angles are generated where at least one of the atoms is in the current residue, and only those dihedrals
where at least one of the two central atoms are in the current residue.

Perception of Local Geometry
............................

When all interactions are declared in the topology, interaction parameters can be assigned by the atom types of the
participating atoms. Using this information, the equilibrium geometry in the neighbourhood of any given atom can be
approximated. For example, hydrogen addition rules in GROMACS are often specified in terms of planarity/tetrahedrality,
for which the analysis of the extrema of the dihedral terms comes handy. Although the dihedral potential term is
originally a correction for the 1-4 Lennard-Jones interaction, the positions of its minima (or maxima) typically reflect
the rotational symmetry of the bond. In a similar fashion, the equilibrium angle of the angle vibration potential terms
shows if the angle is near 120° or the 109.5° required for tetrahedral configuration. As said above, this approach is
not foolproof and is prone to come to false consequences. However, the errors induced by this (e.g. slightly misplaced
hydrogens) are easy to correct by a short energy minimization routine.
