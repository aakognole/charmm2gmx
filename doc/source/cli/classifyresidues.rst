.. coding=utf-8
.. _classifyresidues:

Listing and Classifying Residue Topologies
==========================================

This subcommand lists and classifies residue topologies of the force field. The classification is done first according
to the molecule type (basically the name of the `\*.rtp` file in which the residue topology entry was found), secondly
according to the chainability of the residue: stand-alone, mid-chain, starting or ending terminus. Note that the
latter classification is only experimental and might not be incorrect in some cases.

Invocation
----------

.. code-block:: bash

    charmm2gmx classifyresidues <ffdir>

Command-line options
--------------------

`<ffdir>` (required):
    The force field directory
