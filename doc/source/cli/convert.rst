.. coding=utf-8
.. _convert:

Converting a CHARMM FF to GROMACS Format
========================================

The original goal of the whole `charmm2gmx` project is to create a reproducible, fully automated means for converting CHARMM toppar files to the GROMACS FF directory format. The `convert` subcommand of the `charmm2gmx` main entry point does just this.

Invocation
----------

.. code-block:: bash

    charmm2gmx convert <inputfile>

Command-line options
--------------------

`<inputfile>` (required):
    the single input file for the conversion subprogram, listing the CHARMM topology and parameter files and instructions on how to handle them. A detailed description is found :ref:`here <charmm2gmx_in>`.



