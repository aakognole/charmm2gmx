.. coding=utf-8
.. _genhdb:

Generate Hydrogen Addition Rules
================================

The `genhdb` subcommand of `charmm2gmx` generates hydrogen addition database files (`*.hdb` in the force field
directory) for almost all defined residue topologies.

Note that the handful of `hydrogen addition rules supported by \
GROMACS <https://manual.gromacs.org/current/reference-manual/topologies/pdb2gmx-input-files.html#hydrogen-database>`_
are not enough to cover all possible cases. `genhdb` tries to be smart about it, and it tries to assign a hydrogen
addition rule in all cases, even when no rule matches exactly. In any case, a subsequent, short energy minimization
should be enough to resolve the errors introduced this way.

Invocation
----------

.. code-block:: bash

    charmm2gmx genhdb -t <moleculeclass> -n <resn> -c <resn> -m <resn> -s <resn> <ffdir>

Command-line options
--------------------

`-t` or `--moleculeclass` (optional, multiple):
    only generate hydrogen bond database for the named molecule class (`\*.rtp` file base name, e.g. `merged` for
    `merged.rtp`). If not given, all `\*.rtp` files in the force field directory are read and analyzed. Can be given
    multiple times.
`-n`, `--force-nter` (optional, multiple):
    the named residue is to be considered as a N-terminal (5'-terminal) residue, instead of autodetection
`-c`, `--force-cter` (optional, multiple):
    the named residue is to be considered as a C-terminal (3'-terminal) residue, instead of autodetection
`-m`, `--force-midchain` (optional, multiple):
    the named residue is to be considered as a mid-chain residue, instead of autodetection
`-s`, `--force-standalone` (optional, multiple):
    the named residue is to be considered as a non-chainable one, instead of autodetection
`<ffdir>` (required):
    the force field directory

