.. coding=utf-8
.. _compare:

Comparing Two GROMACS FF Directories
====================================

The `compare` subcommand compares two force fields in the GROMACS format and lists missing or differently defined
parameters and residue topologies.

The first given force field should be the "new" one, the second is the "reference". Parameters and residue topologies
missing from the reference force field but present in the new one are not considered errors, therefore not printed.

Invocation
----------

.. code-block:: bash

    charmm2gmx compare <ffdir1> <ffdir2>

Command-line options
--------------------

`<ffdir1>` (required):
    The first ("new") force field
`<ffdir2>` (required):
    The second ("reference") force field
