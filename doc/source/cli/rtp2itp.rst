.. coding=utf-8
.. _rtp2itp:

Generating Include Topology Files for Small Molecules
=====================================================

`charmm2gmx rtp2itp` converts residue topologies of stand-alone (not chainable like amino acids or nucleotides) residue
topologies into `[ moleculeclass ]` entries and puts them into `\*.itp` files for including in topologies. It can be
thought of as a replacement for `gmx pdb2gmx` in simple cases, but constructing topologies without requiring a
coordinate file.

Invocation
----------

.. code-block:: bash

    charmm2gmx rtp2itp [OPTIONS] <ffdir>

Command-line options
--------------------

`<ffdir>` (required):
    GROMACS force field directory
`-r <resn>`, `--resname <resn>` (required; can be given multiple times):
    the residue name to operate on. This will also be the name of the molecule type.
`-o <file>`, `--output <file>` (optional):
    the name of the file to write the generated molecule types into. If not given, each residue will be written into its
    dedicated file (`<resn>.itp`).
`-p <name>`, `--posresmacro <name>` (optional):
    the name of the `grompp` preprocessor macro for enabling position restraints on heavy (non-hydrogen) atoms. If not
    given, position restraints are not generated in the topology.
`-f <fconst>`, `--posresforce <fconst>` (optional):
    force constant to use for position restraints in all three directions. By default it is 1000.0 kJ/mol/nm^2.


