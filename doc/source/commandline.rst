.. coding=utf-8

Command Line Utilities
======================

The main program is invoked by `charmm2gmx`. The following subcommands are defined:

:doc:`convert <cli/convert>`:
    convert a set of CHARMM force field files (`top_*.rtf`, `par_*.prm` or `*.str`) into a GROMACS FF directory.
:doc:`classifyresidues <cli/classifyresidues>`:
    print a list of residue topologies defined in a GROMACS FF directory and classify them according to molecule type
    (lipid, protein, nucleic acid etc.) and chainability
:doc:`compare <cli/compare>`:
    compare two force fields in GROMACS format: check for missing/differently defined residue topologies and interaction parameters
:doc:`findmissingpars <cli/findmissingpars>`:
    load a GROMACS FF directory and try to assign interaction parameters for each residue topology entry. List missing interaction parameters.
:doc:`genhdb <cli/genhdb>`:
    generate hydrogen addition database entries (`*.hdb` files) for (almost) all residue topologies in an already existing GROMACS FF directory
:doc:`rtp2itp <cli/rtp2itp>`:
    convert some residue topology entries in a GROMACS FF to [ moleculetype ] entries in `*.itp` files

Command line options
--------------------

The main program can have the following command line options, to be given *before* the subcommand:

`--debug`:
    Run the program in debug mode. Produces lots and lots of output.
`--profile`:
    Run the program with profiling enabled. Profiling is done using
    `cProfile <https://docs.python.org/3/library/profile.html>`_, and the results are written to the file supplied as
    an argument of this switch
`--help`:
    Print a short summary on how to use the program. This switch can also be given to each subcommand.
`--version`:
    Print the version information and exit.

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :hidden:

    cli/convert
    cli/classifyresidues
    cli/compare
    cli/findmissingpars
    cli/genhdb
    cli/rtp2itp