.. charmm2gmx documentation master file, created by
   sphinx-quickstart on Fri May 14 17:45:50 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to charmm2gmx's documentation!
======================================

.. container:: twocol

   .. container:: rightside

      .. image:: _static/logo.svg
         :width: 100%
         :alt: charmm2gmx logo

   .. container:: leftside

      CHARMM2GMX is a Python package and a set of command-line tools for converting CHARMM toppar files to GROMACS force
      fields. Apart from its main task, several other utilities are implemented for validating and checking GROMACS force
      field directories, as well as manipulating residue topologies.

      This project has been used since July 2021 to create the `"official" GROMACS port of the CHARMM additive force field <http://mackerell.umaryland.edu/charmm_ff.shtml#gromacs>`_ by J. Lemkul. It is however not related or affiliated in any way to the similarly named scripts found at
      `Alexander D. MacKerell's above mentionend page <http://mackerell.umaryland.edu/charmm_ff.shtml#gromacs>`_. Those scripts convert CHARMM stream
      files produced by CGenFF to GROMACS topologies.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   quickstart
   commandline
   cite
   backgroundinfo
   api
