# coding=utf-8

"""Base class for water model"""
from typing import List, Optional


class WaterAtomType:
    """Atom type used in a water model

    :ivar str name: the name of the atom type
    :ivar float mass: mass of the atom (amu)
    :ivar int atnum: atomic number
    :ivar str comment: textual description
    :ivar float epsilon: epsilon L-J parameter (units as in GROMACS)
    :ivar float sigma: sigma L-J parameter (units as in GROMACS)
    :ivar float pcharge: partial charge (electron units)
    """
    name: str
    mass: float
    atnum: int
    comment: str
    epsilon: float
    sigma: float
    pcharge: float

    def __init__(self, name: str, mass: float, atnum: int, epsilon: float, sigma: float, pcharge: float, comment: Optional[str] = None):
        self.name = name
        self.mass = mass
        self.atnum = atnum
        self.comment = comment
        self.epsilon = epsilon
        self.sigma = sigma
        self.pcharge = pcharge


class WaterModel:
    """Representation of a water model

    Each water model must have its dedicated atom types!

    :ivar str name: short name
    :ivar str itpfile: `\*.itp` file to include, relative to the resource/watermodels directory
    :ivar str description: textual description
    :ivar atomtypes: atom types used in this model
    :type atomtypes: list of :class:`WaterAtomType` instances
    """
    name: str
    atomtypes: List[WaterAtomType]
    itpfile: str
    description: str

    def __init__(self, name: str, atomtypes: List[WaterAtomType], itpfile: str, description: str):
        self.name = name
        self.atomtypes = atomtypes
        self.itpfile = itpfile
        self.description = description


    def hasAtomType(self, name: str) -> bool:
        """Check if this water model has the named atom type for one of its atoms

        :param str name: name of the atom type
        """
        return any([a.name == name for a in self.atomtypes])

    def getAtomType(self, name: str) -> WaterAtomType:
        try:
            return [a for a in self.atomtypes if a.name == name][0]
        except ValueError:
            raise KeyError(name)


WATERMODELS = {
    'TIP3P': WaterModel('TIP3P', [
        WaterAtomType('OT', 16.0000, 8, 0.63639, 0.315057422683, -0.834, comment='Oxygen for CHARMM TIP3P water model'),
        WaterAtomType('HT', 1.0080, 1, 0.192464, 0.0400013524445, 0.417, comment='Hydrogen for CHARMM TIP3P water model'),
    ], 'tip3p.itp', 'CHARMM-modified TIP3P water model (recommended over original TIP3P)'),
    'TIP3P_ORIGINAL': WaterModel('TIP3P_ORIGINAL', [
        WaterAtomType('OWT3', 16.0000, 8, 0.63639, 0.315057422683, -0.834, comment='Oxygen for TIP3P water model'),
        WaterAtomType('HWT3', 1.0080, 1, 0.0, 0.0, 0.417, comment='Hydrogen for TIP3P water model')
    ], 'tip3p_original.itp', 'Original TIP3P water model'),
    'SPC': WaterModel('SPC', [
        WaterAtomType('OWSPC', 15.99940, 8, 6.50629e-01, 3.16557e-01, -0.82, comment='Oxygen for SPC water model'),
        WaterAtomType('HWSPC', 1.0080, 1, 0.0, 0.0, 0.41, comment='Hydrogen for SPC water model'),
    ], 'spc.itp', 'SPC water model'),
    'SPCE': WaterModel('SPCE', [
        WaterAtomType('OWSPCE', 15.99940, 8, 6.50629e-01, 3.16557e-01, -0.8476, comment='Oxygen for SPC/E water model'),
        WaterAtomType('HWSPCE', 1.0080, 1, 0.0, 0.0, 0.4238, comment='Hydrogen for SPC/E water model'),
    ], 'spce.itp', 'SPC/E water model'),
    'TIP5P': WaterModel('TIP5P', [
        WaterAtomType('OWT5', 16.0000, 8, 6.69440e-01, 3.12000e-01, 0.0, comment='Oxygen for TIP5P water model'),
        WaterAtomType('HWT5', 1.0080, 1, 0.0, 0.0, 0.241, comment='Hydrogen for TIP5P water model'),
        WaterAtomType('MWT5', 0.0000, 0, 0.0, 0.0, -0.241, comment='Virtual site for TIP5P water model'),
    ], 'tip5p.itp', 'TIP5P water model'),
    'TIP4P': WaterModel('TIP4P', [
        WaterAtomType('OWT4', 16.0000, 8, 6.48520e-01, 3.15365e-01, 0.0, comment='Oxygen for TIP4P water model'),
        WaterAtomType('HWT4', 1.0080, 1, 0.0, 0.0, 0.52, comment='Hydrogen for TIP4P water model'),
        WaterAtomType('MWT4', 0.0000, 0, 0.0, 0.0, -1.04, comment='Virtual site of TIP4P water model')
    ], 'tip4p.itp', 'TIP4P water model'),
    'TIP4PEW': WaterModel('TIP4PEW', [
        WaterAtomType('OWT4EW', 16.0000, 8, 0.6809460, 0.316435, 0.0, comment='Oxygen for TIP4P/Ew water model'),
        WaterAtomType('HWT4EW', 1.0080, 1, 0.0, 0.0, 0.52422, comment='Hydrogen for TIP4P/Ew water model'),
        WaterAtomType('MWT4EW', 0.0000, 0, 0.0, 0.0, -1.04844, comment='Virtual site of TIP4P/Ew water model')
    ], 'tip4pew.itp', 'TIP4P/Ew water model'),
}
