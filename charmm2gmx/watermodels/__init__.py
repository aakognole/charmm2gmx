# coding=utf-8
"""Water model classes"""

from .watermodel import WaterModel, WATERMODELS, WaterAtomType
