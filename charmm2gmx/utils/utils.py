from typing import Optional, Tuple, Sequence
import re
import pkg_resources


def bondedtree2list(dic):
    lis = []
    if not dic:
        return [()]
    for key in dic:
        for parts in bondedtree2list(dic[key]):
            lis.append((key,)+parts)
    return lis


def splitline(line: str, commentchar: str, contchar: Optional[str] = None) -> Tuple[str, str, str]:
    """Split a line read from a file into code and comment parts, also taking care of line continuation

    :param str line: the line as read from the file, with the remainder from the previous line prepended if needed
    :param str commentchar: comment character (e.g. #, ;, ! etc.)
    :param str contchar: continuation character: if a line ends with this, it is continued on the next line.
    :return: code part, comment part, remainder
    :rtype: 3 strings
    """
    try:
        code, comment = line.split(commentchar, 1)
    except ValueError:
        code = line
        comment = ''
    code = code.strip()
    comment = comment.strip()
    if code and (contchar is not None) and code.endswith(contchar) and (commentchar not in line):
        remainder = code[:-len(contchar)] + ' '
        code = ''
    else:
        remainder = ''
    return code, comment, remainder


def commonname(atomnames: Sequence[str]) -> Tuple[str, bool]:
    """Get the common part of atom names

    :param atomnames: the names of the atoms
    :type atomnames: sequence of strings
    :return: common part, isnumbered flag
    :rtype: str, bool
    """
    maxnamelength = max([len(atom) for atom in atomnames])
    maxcommonlength = max(
        [i for i in range(maxnamelength + 1) if all([atom[:i] == atomnames[0][:i] for atom in atomnames])])
    if maxcommonlength < 1:
        raise ValueError(f'Common name too short for atoms {atomnames}')
    cn = atomnames[0][:maxcommonlength]
    # Check if atom names are like HB1, HB2 (numbered) or H5', HH5'' (not numbered)
    isnumbered = (len(atomnames) == 1) or all([re.match(fr"^{cn}\d+$", an) for an in atomnames])
    return cn, isnumbered


def getversion() -> str:
    """Get the version of the charmm2gmx package"""
    return pkg_resources.get_distribution('charmm2gmx').version
