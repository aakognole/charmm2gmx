# coding=utf-8
"""
Interaction type
"""

import enum


class InteractionType(enum.Enum):
    """Enumerating possible interaction types between atoms or groups of atoms"""
    Bond = 'bond'
    Angle = 'angle'
    Dihedral = 'dihedral'
    Improper = 'improper'
    Cmap = 'cmap'
    LJ = 'LennardJones'
    LJ14 = 'LennardJones14'
    NBfix = 'NBFix'
    Constraint = 'Constraint'
