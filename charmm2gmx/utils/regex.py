# coding=utf-8
"""Various frequently used regular expression patterns"""


RE_ATOMID = r"[-+_A-Za-z0-9']+"  # atom type or atom name. Also residue name
RE_MACRO = r'[0-9a-zA-Z_]+'  # C++ preprocessor macro names
RE_INT = r'\d+'  # integer number
RE_FLOAT = r'[+-]?(?:\d+\.\d+|\d+\.|\.\d+|\d+)([eE][+-]?\d+)?'  # match any of 0, 0., 0.0, .0 with arbitrary sign and number of digits, as well as exponent