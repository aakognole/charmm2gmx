# coding=utf-8
"""Citation management"""
import re
from typing import Tuple, List, Dict, Final, Union


class CitationEntry:
    title: str = ''
    shorttitle: str = ''
    authors: List[str]
    year: int = 0
    volume: int = 0
    pages: Tuple[str, str] = ('', '')
    doi: str = ''
    abstract: str = ''
    copyright: str = ''
    journal: str = ''
    number: int = 0
    url: str = ''

    def __str__(self) -> str:
        """String representation"""
        return f'{" and ".join(self.authors)}: {self.title}. {self.journal} ({self.year}) {self.volume}.' \
               f' {self.number if self.number > 0 else ""} pp {self.pages[0]}-{self.pages[1]} (DOI: {self.doi})'

class PhdThesis(CitationEntry):
    school: str = ''
    def __str__(self) -> str:
        """String representation"""
        return f'{" and ".join(self.authors)}: {self.title}. PhD thesis. {self.school} ({self.year}) (DOI: {self.doi})'



class CitationList:
    bibtex_translation: Final[Dict[str, str]] = {
        r'{\^i}': 'î',
        r'{$\phi}': 'φ',
        r'{$\psi}': 'ψ',
        r'{$X$}': 'Χ',  # capital chi
        r'{$\alpha$}': 'α',
        r'{$\beta$}': 'β',
        r'\textcopyright{}': '©',
        r'\{': '{',
        r'\}': '}',
        r'\textendash': '–',
        r'\textemdash': '—',
        r'\&': '&',
        r'{$\gamma$}': 'γ',
        r'{$\cdot$}': '·',
        r'{$\eta$}': 'η',
        r'{$\epsilon$}': 'ε',
        r'{\%}': '%',
        r'\textrightarrow': '→',
        r'{$\Phi$}': 'Φ',
        r'{$\Psi$}': 'Ψ',
        r'\,': '\u00a0',  # non-breakable space
        r'{$\lambda$}': 'λ',
        r'{$\zeta$}': 'ζ',
        r'{$<$}': '<',
        r'{$>$}': '>',
        r'{$\sim$}': '∼',
        r'\AA{}': 'Å',
        r'{\"u}': 'ü',
        r'{\'n}': 'ń',
        r'{$\pm$}': '±',
        r'\AA,': 'Å,',
        r'\AA ': 'Å ',
        r'{$\upsilon$}': 'υ',
        r'{$\propto$}': '∝',
        r'{$\leq$}': '≤',
        r'{$\mu$}': 'μ',
        r'{\'o}': 'ó',
        r'{$\varphi$}': 'ϕ',
        r'{$\chi$}': 'χ',
        r'{$\leftrightarrow$}': '↔',
        r'~': '\u00a0',  # non-breakable space
        r"{${'}$}": "'",
        r"{{": "",
        r"}}": "",
    }

    articles: Dict[str, CitationEntry]

    def __init__(self, filename: str):
        self.readBibTeX(filename)

    def untexify(self, text: str) -> str:
        for texcode, textvalue in self.bibtex_translation.items():
            text = text.replace(texcode, textvalue)
        return text

    def readBibTeX(self, filename: str):
        """Not a true BibTeX parser, only understands @article entries"""
        self.articles = {}
        currentarticle = None
        with open(filename, 'rt') as f:
            for lineno, line in enumerate(f, start=1):
                line = line.replace(r'\%', 'PROTECTED_BACKSLASH_PERCENT')
#                try:
#                    code, comment = line.split('%', 1)
#                except ValueError:
#                    code = line
#                    comment = ''
                code = line
                comment = ''
                code = code.replace('PROTECTED_BACKSLASH_PERCENT', '\%').strip()
                comment = comment.replace('PROTECTED_BACKSLASH_PERCENT', '\%').strip()
                if not code:
                    continue
                elif code == '}':
                    # end of the last entry
                    pass
                elif m := re.match(r'@(?P<entrytype>article|incollection|phdthesis)\{(?P<citekey>[a-zA-Z0-9]+)\s*,?', code):
                    if m['entrytype'] in ['article', 'incollection']:
                        currentarticle = CitationEntry()
                    elif m['entrytype'] == 'phdthesis':
                        currentarticle = PhdThesis()
                    else:
                        raise NotImplementedError
                    self.articles[m['citekey']] = currentarticle
                elif m := re.match(r'(title|booktitle)\s*=\s*\{(?P<title>.*)\}\s*,?', code):
                    currentarticle.title = self.untexify(m['title'])
                elif m := re.match(r'shorttitle\s*=\s*\{(?P<title>.*)\}\s*,?', code):
                    currentarticle.shorttitle = self.untexify(m['title'])
                elif m := re.match(r'author\s*=\s*\{(?P<authors>(.+)(\s+and\s+.+)*)\}\s*,?', code):
                    currentarticle.authors = [a.strip() for a in m['authors'].split(' and ')]
                elif m := re.match(r'year\s*=\s*\{(?P<year>\d{4})\}\s*,?', code):
                    currentarticle.year = int(m['year'])
                elif m := re.match(r'volume\s*=\s*\{(?P<volume>\d+)\}\s*,?', code):
                    currentarticle.volume = int(m['volume'])
                elif m := re.match(r'pages\s*=\s*\{(?P<pages>.+--.+)\}\s*,?', code):
                    firstpage, lastpage = m['pages'].split('--')
                    currentarticle.pages = (firstpage, lastpage)
                elif m := re.match(r'pages\s*=\s*\{(?P<pages>.+-.+)\}\s*,?', code):
                    firstpage, lastpage = m['pages'].split('-')
                    currentarticle.pages = (firstpage, lastpage)
                elif m := re.match(r'pages\s*=\s*\{(?P<pages>.+)\}\s*,?', code):
                    currentarticle.pages = (m['pages'], '')
                elif m := re.match(r'doi\s*=\s*\{(?P<doi>(https?://doi.org/)?\d{2}\.\d{4}/.+)\}\s*,?', code):
                    currentarticle.doi = m['doi']
                elif m := re.match(r'journal\s*=\s*\{(?P<journal>.*)\}\s*,?', code):
                    currentarticle.journal = m['journal']
                elif m := re.match(r'number\s*=\s*\{(?P<number>\d+)\}\s*,?', code):
                    currentarticle.number = int(m['number'])
                elif m := re.match(r'abstract\s*=\s*\{(?P<abstract>.*)\}\s*,?', code):
                    currentarticle.abstract = self.untexify(m['abstract'])
                elif m := re.match(r'copyright\s*=\s*\{(?P<copyright>.*)\}\s*,?', code):
                    currentarticle.copyright = self.untexify(m['copyright'])
                elif m := re.match(r'url\s*=\s*\{(?P<url>https?://.+)\}\s*,?', code):
                    currentarticle.url = self.untexify(m['url'])
                elif m := re.match(r'issn|keywords|ids|month|language|file|publisher|annotation|langid|editor|isbn|urldate|note|tex\.ids|annote\s*=\s*.*', code):
                    continue
                elif m := re.match(r'school\s*=\s*\{(?P<school>.+)\}\s*,?', code):
                    currentarticle.school = m['school']
                else:
                    raise ValueError(f'Unknown line #{lineno}: "{code}"')

    def __getitem__(self, item: str) -> Union[CitationEntry, str]:
        if item[0] == item[-1] == '"':
            return item
        return self.articles[item]
