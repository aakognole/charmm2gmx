# coding: utf-8
"""An enum for specifying how to handle parameter redefinition cases"""
import enum


class ParameterClashPolicy(enum.Enum):
    """Policy for handling parameter redefinitions"""
    KEEP_OLDER = '-'
    KEEP_NEWER = '+'
    ERROR = ''
