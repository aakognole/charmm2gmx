# coding=utf-8
"""Get the version of the package"""

import pkg_resources
import datetime

VERSION = pkg_resources.get_distribution('charmm2gmx').version


def GMXFileHeader() -> str:
    """Return a string to be included at the top of a written GMX file"""
    return f"""; CHARMM-port for GROMACS
; created with charmm2gmx version {VERSION} on {datetime.datetime.now()}
; Code: https://gitlab.com/awacha/charmm2gmx
; Documentation: https://awacha.gitlab.com/charmm2gmx
; Citation: András F. Wacha and Justin A. Lemkul: "charmm2gmx: An Automated 
;      Method to Port the CHARMM Additive Force Field to GROMACS". Journal of
;      Chemical Information and Modeling 2023, 63(14), 4246-4252.
;      DOI: 10.1021/acs.jcim.3c00860
;
; Charmm2GMX written by András Wacha and Justin Lemkul, based on the original
; port by E. Prabhu Raman, Justin A. Lemkul, Robert Best and Alexander D. 
; MacKerell, Jr.
"""
