# coding=utf-8

"""Information to be put in a .r2b file"""

import collections

R2B = collections.namedtuple("R2B", ["pdb", "normal", "Nter", "Cter", "twoter"])

R2Bdata = [
    R2B('ALA', 'ALA', None, None, None),
    R2B('ARG', 'ARG', None, None, None),
    R2B('ARGN', 'ARGN', None, None, None),
    R2B('ASN', 'ASN', None, None, None),
    R2B('ASP', 'ASP', None, None, None),
    R2B('ASPH', 'ASPP', None, None, None),
    R2B('CYS', 'CYS', None, None, None),
    R2B('CYS2', 'CYS2', None, None, None),
    R2B('GLN', 'GLN', None, None, None),
    R2B('GLU', 'GLU', None, None, None),
    R2B('GLY', 'GLY', None, None, None),
    R2B('GLUH', 'GLUP', None, None, None),
    R2B('HISD', 'HSD', None, None, None),
    R2B('HIS1', 'HSD', None, None, None),
    R2B('HISE', 'HSE', None, None, None),
    R2B('HISH', 'HSP', None, None, None),
    R2B('ILE', 'ILE', None, None, None),
    R2B('LYSN', 'LSN', None, None, None),
    R2B('LYS', 'LYS', None, None, None),
    R2B('LEU', 'LEU', None, None, None),
    R2B('MET', 'MET', None, None, None),
    R2B('PHE', 'PHE', None, None, None),
    R2B('PRO', 'PRO', None, None, None),
    R2B('SER', 'SER', None, None, None),
    R2B('THR', 'THR', None, None, None),
    R2B('TRP', 'TRP', None, None, None),
    R2B('TYR', 'TYR', None, None, None),
    R2B('VAL', 'VAL', None, None, None),
    R2B('HEM', 'HEME', None, None, None),
#    R2B('DA', 'DA', 'DA5', 'DA3', None),   # including these lines introduce errors in pdb2gmx
#    R2B('DG', 'DG', 'DG5', 'DG3', None),
#    R2B('DC', 'DC', 'DC5', 'DC3', None),
#    R2B('DT', 'DT', 'DT5', 'DC3', None),
    R2B('A', 'ADE', None, None, None),
    R2B('U', 'URA', None, None, None),
    R2B('C', 'CYT', None, None, None),
    R2B('T', 'THY', None, None, None),
#    R2B('DU', 'DU', 'DU5', 'DU3', None),
    R2B('G', 'GUA', None, None, None),
]
