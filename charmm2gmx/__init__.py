# coding=utf-8

import pkg_resources

__version__ = pkg_resources.get_distribution('charmm2gmx').version

del pkg_resources