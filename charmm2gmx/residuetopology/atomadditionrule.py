# coding=utf-8
from typing import Tuple, Optional, List


class AtomAdditionRule:
    """Atom addition rule, as in the GROMACS .hdb, .c.tdb and .n.tdb files

    For more details, check the official GROMACS documentation:

    https://manual.gromacs.org/current/reference-manual/topologies/pdb2gmx-input-files.html

    :ivar count: number of atoms to add
    :type count: int
    :ivar rule: rule number (check the GROMACS documentation)
    :type rule: int
    :ivar hname: common part of the atom names
    :type hname: str
    :ivar controlatoms: names of the control atoms
    :type controlatoms: tuple of strings
    :ivar comment: some optional description
    :type comment: str or None
    :ivar inaccurate: True if the rule has been assigned but some caveats exist
    :type inaccurate: bool
    """
    count: int
    rule: int
    hname: str
    controlatoms: Tuple[str, ...]
    comment: Optional[str]
    inaccurate: bool = False

    def __init__(self, count: int, rule: int, hname: str, contolatoms: Tuple[str, ...], comment: Optional[str] = None,
                 inaccurate: bool = False):
        self.count = count
        self.rule = rule
        self.hname = hname
        self.controlatoms = contolatoms
        self.comment = comment
        self.inaccurate = inaccurate

    def __str__(self, comment: bool = False) -> str:
        """GROMACS representation"""
        return f"{self.count:<7d} {self.rule:<7d} {self.hname:<7s} " + " ".join(
            [f"{a:<7s}" for a in self.controlatoms]) + (
                   ('; ' + self.comment) if (self.comment is not None) and comment else '')

    def __repr__(self) -> str:
        return f"AtomAdditionRule({self.count}, {self.rule}, {self.hname}, {self.controlatoms})"

    def __eq__(self, other: "AtomAdditionRule") -> bool:
        """Check equality"""

        if (self.count != other.count) or (self.rule != other.rule) or (self.hname != other.hname) or (
                len(self.controlatoms) != len(other.controlatoms)) or (self.controlatoms[0] != other.controlatoms[0]):
            # check the equality of the 1st control atom here: this requirement is common among all rules.
            return False
        elif self.rule == 1:
            return sorted(self.controlatoms[1:]) == sorted(other.controlatoms[1:])
        elif self.rule == 2:
            return self.controlatoms[1:] == other.controlatoms[1:]
        elif self.rule == 3:
            return self.controlatoms[1:] == other.controlatoms[1:]
        elif self.rule == 4:
            return self.controlatoms[1:] == other.controlatoms[1:]
        elif self.rule == 5:
            return sorted(self.controlatoms[1:]) == sorted(other.controlatoms[1:])
        elif self.rule == 6:
            return sorted(self.controlatoms[1:]) == sorted(other.controlatoms[1:])
        elif self.rule in [7, 8, 9]:
            # water hydrogens for SPC, TIP4P and TIP5P waters
            return True  # the 1st check is sufficient
        else:
            raise ValueError(f'Unknown atom addition rule: {self.rule}')

    def __ge__(self, other: "AtomAdditionRule") -> bool:
        if self.hname > other.hname:
            return True
        elif self.hname < other.hname:
            return False
        # if we reach this line, the hnames match
        elif self.rule > other.rule:
            return True
        elif self.rule < other.rule:
            return False
        # hnames and rule numbers match
        elif self.count > other.count:
            return True
        elif self.count < other.count:
            return False
        # hnames, rule numbers and atom counts match
        else:
            return ((self.controlatoms[0],)+tuple(sorted(self.controlatoms[1:]))) >= ( (other.controlatoms[0],)+tuple(sorted(other.controlatoms[1:])))

    def __lt__(self, other: "AtomAdditionRule") -> bool:
        return not other >= self

    def newatomnames(self) -> List[str]:
        if self.count == 1:
            return [self.hname]
        else:
            return [f"{self.hname}{i}" for i in range(self.count)]