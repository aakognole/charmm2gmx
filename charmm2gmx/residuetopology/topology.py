# coding=utf-8
"""Residue topology class"""

import copy
import enum
import itertools
import logging
from typing import List, Dict, Any, Tuple, Optional, Set, Sequence

from .atom import Atom
from .atomadditionrule import AtomAdditionRule
from .interaction import Bond, Angle, Improper, Dihedral, Cmap, ICSpec, TopologyInteraction
from ..prminfo import PRMInfo
from ..utils import commonname, bondedtree2list

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class LocationInChain(enum.Enum):
    """Possible location of the residue in a chain (e.g. peptide or nucleic acid)"""
    NotChainable = None  # stand-alone, non-chainable residue
    Left = -1  # left terminus (N or 5')
    Right = 1  # right terminus (C or 3')
    Mid = 0  # mid-chain residue, not a terminus


class ValidationError(Exception):
    """Exception raised when the validation of a residue topology fails for some reason"""
    pass


class ResidueTopology:
    """Representation of a residue topology

    Residue topologies consist of their atoms and the bonded interactions between them. Typically, only bonds, improper
    dihedrals and cmap interactions are specified explicitly, angles and proper dihedrals are auto-generated using
    bond information. Auto-generating angles and (proper) dihedrals based on the bond information is supported by the
    methods `generateAngles()` and `generateDihedrals()`. These are not called automatically, the user
    is responsible to call them before using operations requiring angle or dihedral information.

    Some residues are meant to be chained, e.g. amino acids or nucleotides. In these residues, one or more are
    between "normal" atoms and "references". Reference atoms have a '-' or a '+' prefix before their names, identifying
    the atom with the same name in the previous or next residue, respectively. When generating angles and dihedrals,
    it is assumed that we have an infinite chain of the same residues. All possible angles where at least one atom
    belongs to the current residue are generated. For dihedrals, all dihedrals are generated where at least one of the
    central atoms is in the current residue.

    Some facilities, such as building the hydrogen addition database, require in-depth analysis of the topology, i.e.
    having angle and dihedral information ready.
    sometimes also crossing the residue boundary.

    If the force field parameters are known (numerical parameters for the interaction potentials such as bond length
    and force constant etc.), they can be assigned to bond, angle, dihedral, improper and cmap entries, based on the
    atom types of the participating atoms. Some facilities, e.g. the hydrogen addition database builder needs this, for
    example to decide if a dihedral prefers planarity or not.

    :ivar name: residue name
    :type name: str
    :ivar charge: total electrostatic charge of the residue
    :type charge: float
    :ivar atoms: list of atoms in the residue
    :type atoms: list of :class:`charmm2gmx.residuetopology.atom.Atom` instances
    :ivar bonds: list of bonds
    :type bonds: list of :class:`charmm2gmx.residuetopology.interaction.Bond` instances
    :ivar angles: list of angles
    :type angles: list of :class:`charmm2gmx.residuetopology.interaction.Angle` instances
    :ivar impropers: list of improper dihedrals
    :type impropers: list of :class:`charmm2gmx.residuetopology.interaction.Improper` instances
    :ivar dihedrals: list of proper dihedrals
    :type dihedrals: list of :class:`charmm2gmx.residuetopology.interaction.Dihedral` instances
    :ivar cmaps: list of cmap interactions
    :type cmaps: list of :class:`charmm2gmx.residuetopology.interaction.Cmap` instances
    :ivar ics: list of internal coordinate specifications
    :type ics: list of :class:`charmm2gmx.residuetopology.interaction.ICSpec` instances
    :ivar comment: textual description/explanation
    :type comment: str
    :ivar filename: name of the file from which this residue topology has been read
    :type filename: str
    :ivar moleculeclass: molecule class (e.g. lipid, prot, na etc.)
    :type moleculeclass: str
    :ivar hydrogenadditionrules: hydrogen addition rules for the .hdb files in the GROMACS representation
    :type hydrogenadditionrules: None if not yet calculated or a list of :class:`AtomAdditionRule` instances
    :ivar next: points to the next residue in the chain
    :type next: :class:`ResidueTopology` instance
    :ivar prev: points to the previous residue in the chain
    :type prev: :class:`ResidueTopology` instance
    :ivar locationinchain: the residues location in a chain: terminus, mid-chain or not chainable
    :type locationinchain: :class:`LocationInChain` enum member
    """
    name: str
    charge: float
    atoms: List[Atom]
    bonds: List[Bond]
    angles: List[Angle]
    impropers: List[Improper]
    dihedrals: List[Dihedral]
    cmaps: List[Cmap]
    ics: List[ICSpec]
    comment: str
    filename: str
    lineno: Optional[int] = None
    moleculeclass: str
    hydrogenadditionrules: Optional[List[AtomAdditionRule]]
    prev: Optional["ResidueTopology"]
    next: Optional["ResidueTopology"]
    locationinchain: Optional[LocationInChain] = None
    defaultleftpatch: Optional[str] = None
    defaultrightpatch: Optional[str] = None
    has_lonepair: bool = False

    def __init__(self, name: str, charge: float, moleculeclass: str, filename: str, lineno: Optional[int], comment: str):
        """Create a new residue topology instance

        :param name: short name of the residue
        :type name: str
        :param charge: total charge of the residue
        :type charge: float
        :param moleculeclass: molecule class (e.g. lipid, prot, na etc.)
        :type moleculeclass: str
        :param filename: name of the file from which this residue topology has been read
        :type filename: str
        :param lineno: line number where the definition of this topology starts in the file
        :type lineno: int or None (for derived residues, e.g. patched ones).
        :param comment: long description
        :type comment: str
        """
        self.name = name
        self.charge = charge
        self.atoms = []
        self.bonds = []
        self.angles = []
        self.impropers = []
        self.dihedrals = []
        self.cmaps = []
        self.ics = []
        self.comment = comment
        self.filename = filename
        self.moleculeclass = moleculeclass
        self.hydrogenadditionrules = None
        self.lineno = lineno
        # by default, link to self
        self.prev = self
        self.next = self
        self.locationinchain = None

    def __str__(self) -> str:
        """GROMACS rtp representation of the residue topology"""
        self.removeDuplicateInteractions()
        s = f'\n[ {self.name} ]\n'
        s += f'; {self.comment}\n'
        s += f'  [ atoms ]\n'
        for atom in self.atoms:
            s += f'    {atom.name:>5s} {atom.atomtype:>8s} {atom.pcharge:>7.4f} {atom.cgroup:>3d}\n'
        for label, atomcount, lis in [
            ('bonds', 2, self.bonds),
            ('angles', 3, self.angles),
            ('dihedrals', 4, self.dihedrals),
            ('impropers', 4, self.impropers),
            ('cmap', 5, self.cmaps),
        ]:
            if not lis:
                # don't write empty lists
                continue
            s += f'  [ {label} ]\n'
            for item in lis:
                assert len(item.atoms) == atomcount
                s += '    ' + ' '.join([f'{a:>5s}' for a in item.atoms]) + '\n'
        return s

    def __eq__(self, other: "ResidueTopology"):
        """Check if two residue topologies are equal.

        Two residues are considered equal if:
            - an 1-1 mapping between atoms of the two residues can be established
            - an 1-1 mapping between interactions of the two residues can be established

        Equality of atoms and interactions is decided in the respective classes:
        :class:`charmm2gmx.residuetopology.atom.Atom` and the respective subclasses of
        :class:`charmm2gmx.residuetopology.interaction.TopologyInteraction`.
        """
        for lis in ['atoms', 'bonds', 'angles', 'impropers', 'dihedrals', 'cmaps']:
            if len(getattr(self, lis)) != len(getattr(other, lis)):
                # quick check: the length must match.
                return False
            for item in getattr(self, lis):
                if not [item_ for item_ in getattr(other, lis) if item_ == item]:
                    return False
        return True

    def __ne__(self, other: "ResidueTopology"):
        return not (self == other)

    def renameAtomType(self, oldname: str, newname: str):
        """Rename an atom type in this residue

        :param oldname: old name of an atom type
        :type oldname: str
        :param newname: new name of the atom type
        :type newname: str
        """
        for atom in self.atoms:
            if atom.atomtype == oldname:
                atom.atomtype = newname

    def renameAtom(self, oldname: str, newname: str):
        """Rename an atom in this residue

        :param oldname: old atom name
        :type oldname: str
        :param newname: new atom name
        :type newname: str
        """
        for atom in self.atoms:
            if atom.name == oldname:
                atom.name = newname

        for lisname in ['bonds', 'angles', 'impropers', 'dihedrals', 'cmaps', 'ics']:
            lis = getattr(self, lisname)
            for item in lis:
                item.atoms = tuple([(newname if a == oldname else a) for a in item.atoms])

    def __copy__(self) -> "ResidueTopology":
        """Make a deep copy"""
        resi = ResidueTopology(self.name, self.charge, self.moleculeclass, self.filename, self.lineno, self.comment)
        for listname in ['atoms', 'bonds', 'angles', 'impropers', 'dihedrals', 'cmaps', 'ics']:
            setattr(resi, listname, [copy.copy(item) for item in getattr(self, listname)])
        resi.locationinchain = self.locationinchain
        return resi

    def diff(self, other: "ResidueTopology", reduced: bool = True, panewidth: int = 39,
             atoms_reorderable: bool = True) -> str:
        """Make a side-by-side comparison of two residues

        :param other: the other residue topology
        :type other: :class:`charmm2gmx.residuetopology.topology.ResidueTopology` instance
        :param reduced: if only the differences are to be shown
        :type reduced: bool
        :param panewidth: number of text columns for each residue. Should be at least 30
        :type panewidth: int
        :param atoms_reorderable: do not require the same atom order in the two residues
        :type atoms_reorderable: bool
        :return: the difference string
        :rtype: str
        """
        s = '=' * (2 * panewidth + 1) + '\n'
        s += f'{self.name:^{panewidth}s}|{other.name:^{panewidth}s}\n'
        s += f'{"-" * len(self.name):^{panewidth}s}|{"-" * len(other.name):^{panewidth}s}\n'
        #            s+=f'{"[ atoms ]":^{2*panewidth+1}s}\n'
        if atoms_reorderable:
            missing1 = '\n'.join([f'{str(atm):^{panewidth}s}!{"missing":^{panewidth}}' for atm in self.atoms if
                                  not [a for a in other.atoms if a == atm]])
            if missing1:
                s += missing1 + '\n'
            missing2 = '\n'.join([f'{"missing":^{panewidth}}!{str(atm):^{panewidth}s}' for atm in other.atoms if
                                  not [a for a in self.atoms if a == atm]])
            if missing2:
                s += missing2 + '\n'
        else:
            for atom1, atom2 in itertools.zip_longest(self.atoms, other.atoms, fillvalue=None):
                if (atom1 == atom2) and reduced:
                    continue
                s += f'{str(atom1):^{panewidth}s}{"=" if atom1 == atom2 else "!"}{str(atom2):^{panewidth}s}\n'
        for listname in ['bonds', 'dihedrals', 'impropers', 'cmaps']:
            if (not getattr(self, listname)) and (not getattr(other, listname)):
                # skip this clause: neither of the residues has any entries in this list
                continue
            if reduced:
                onlyinthis = [it for it in getattr(self, listname) if
                              not [it_ for it_ in getattr(other, listname) if it_ == it]]
                onlyinother = [it for it in getattr(other, listname) if
                               not [it_ for it_ in getattr(self, listname) if it_ == it]]
                if onlyinthis or onlyinother:
                    s += f'{"[ " + listname + " ]":^{2 * panewidth + 1}s}\n'
                for item in onlyinthis:
                    s1 = ' '.join([f'{at:>5s}' for at in sorted(item.atoms)])
                    s += f'{s1:^{panewidth}s}!{"missing":^{panewidth}s}\n'
                for item in onlyinother:
                    s1 = ' '.join([f'{at:>5s}' for at in sorted(item.atoms)])
                    s += f'{"missing":^{panewidth}s}!{s1:^{panewidth}s}\n'
            else:
                s += f'{"[ " + listname + " ]":^{2 * panewidth + 1}s}\n'
                for item1, item2 in itertools.zip_longest(
                        *[sorted(getattr(resi, listname), key=lambda it: sorted(it.atoms)) for resi in
                          [self, other]],
                        fillvalue=None):
                    s1 = ' '.join([f'{at:>5s}' for at in sorted(item1.atoms)]) if item1 is not None else '<nonexistent>'
                    s2 = ' '.join([f'{at:>5s}' for at in sorted(item2.atoms)]) if item2 is not None else '<nonexistent>'
                    s += f'{s1:^{panewidth}s}{"=" if item1 == item2 else "!"}{s2:^{panewidth}s}\n'
        return s

    def validate(self) -> None:
        """Validate the residue topology

        :raises ValidationError: if the validation fails
        """

        # first check if the charge groups have integer charges
        cgroups = {at.cgroup for at in self.atoms}
        if len(cgroups) < len(self.atoms):
            # skip the check if all atoms have their unique charge group.
            for cg in {at.cgroup for at in self.atoms}:
                charge = sum([at.pcharge for at in self.atoms if at.cgroup == cg])
                if abs(charge - round(charge)) > 0.0001:
                    raise ValidationError(f'Total charge in charge group {cg} is non-integer ({charge})')

        # check if all atoms are defined
        atomnames = [at.name for at in self.atoms]
        for listname in ['bonds', 'angles', 'impropers', 'dihedrals', 'cmaps']:
            lis = getattr(self, listname)
            for item in lis:
                missingatoms = [
                    at for at in item.atoms
                    if not ((at.name in atomnames) or ((at.name[0] in '+-') and (at.name[1:] in atomnames)))
                ]
                if missingatoms:
                    raise ValidationError(
                        f'The following atom(s) in residue ({self.name}) are defined in interactions but are not '
                        f'explicitly listed.')

    def assignAtomTypes(self, prminfo: PRMInfo):
        for atom in self.atoms:
            atom.parameters = [a for a in prminfo.atomtypes if a.name == atom.atomtype][0]

    def assignParameters(self, prminfo: PRMInfo, exception_on_missing: bool = True) -> List[
        Tuple[TopologyInteraction, Tuple[str, ...]]]:
        """Assign interaction parameters based on atom types

        :param prminfo: parameter info structure
        :type prminfo: :class:`PRMInfo` instance
        """
        missing = []
        self.assignAtomTypes(prminfo)
        for toplistname, parlistname in [
            ('bonds', 'bondtypes'), ('angles', 'angletypes'), ('impropers', 'impropertypes'),
            ('dihedrals', 'dihedraltypes'), ('cmaps', 'cmaptypes')
        ]:
            # logger.debug(f'Assigning parameters to {toplistname}')
            toplist = getattr(self, toplistname)
            parlist = getattr(prminfo, parlistname)
            for interaction in toplist:
                # logger.debug(f'Interaction between atoms {interaction.atoms}')
                assert isinstance(interaction, TopologyInteraction)
                atomtypes = tuple([
                    self.getAtom(at).atomtype for at in interaction.atoms])
                pars = [item for item in parlist if item.atomtypes_match(atomtypes)]
                if not pars:
                    missing.append((interaction, atomtypes))
                    if exception_on_missing:
                        raise ValueError(
                            f'No parameters can be found for interaction {interaction} in residue {self.name}, with atom types {atomtypes}')
                if len(pars) > 1:
                    if any([p.does_clash(pars[0]) for p in pars[1:]]):
                        raise ValueError(
                            f'Parameter clash for interaction {interaction}:\n' + '\n'.join([str(p) for p in pars]))
                interaction.parameters = pars

        return missing

    def generateAngles(self) -> None:
        """Auto-generate angle interactions from bond information.

        CHARMM / GROMACS typically auto-generates all angle interactions, therefore they are usually not in the
        residue topology file. This method generates all of them. If previous or next residues are referenced
        in the residue topology (e.g. for proteins or nucleic acids), angle interactions crossing residue boundaries
        are also generated, assuming that the previous and the next residue is the same as the current one.
        """
        self.angles = []
        for atom in self.atoms:
            iname = atom.name
            btree = self.constructBondedTree(atom.name, 2)
            for jname in btree:
                for kname in btree[jname]:
                    atoms = (iname, jname, kname)
                    if iname == kname:
                        # should not happen, but who knows...
                        continue
                    if any([a[0] not in '+-' for a in atoms]) and (
                            not [a for a in self.angles if a.atoms == (kname, jname, iname)]):
                        self.angles.append(Angle((iname, jname, kname), 'Autogenerated'))
        return

    def generateDihedrals(self) -> None:
        """Generate all dihedral interactions from already present angle interactions.

        Note that angle interactions are typically auto-generated by CHARMM/GROMACS, and are
        not explicitly listed in the residue topology file. You might want to call `generateAngles()`
        to generate all angle interactions from bond information.
        """
        self.dihedrals = []
        for atom in self.atoms:
            iname = atom.name
            btree = self.constructBondedTree(atom.name, 3)
            for jname in btree:
                for kname in btree[jname]:
                    for lname in btree[jname][kname]:
                        atoms = (iname, jname, kname, lname)
                        if iname == lname:
                            # can happen, e.g. with 3-membered rings
                            continue
                        elif (iname == kname) or (jname == lname):
                            # should not happen, but who knows...
                            continue
                        if (not ((jname[0] in '+-') and (kname in '+-'))) and (
                                not [d for d in self.dihedrals if d.atoms == atoms or d.atoms == atoms[::-1]]):
                            self.dihedrals.append(Dihedral((iname, jname, kname, lname), 'Autogenerated'))

    def atomsCoplanar(self, atom1: str, atom2: str, atom3: str, atom4: str, onlyimproper: bool) -> bool:
        """Check if four atoms are coplanar, i.e. they lie in the same plane

        Tricky, because geometry cannot be used, only topology and interaction parameters, namely dihedral angles.

        If there is an improper dihedral interaction defined on the atoms (any order, i.e. every possible permutation
        of the atom names is checked!), it decides coplanarity (yes if no out-of-plane minima, no if there are).

        If there aren't any improper dihedrals but there is a proper dihedral term on the atoms (only the parameter
        order and its reverse is checked!) which is planar (no out-of-plane minima exist, i.e. only multiplicities 1
        and 2 are present), the atoms are coplanar. If there are out-of-plane minima, the atoms are not coplanar.

        If the above checks could not decide on planarity, a ValueError is raised.
        """
        # check for planar improper dihedral terms
        impropers = [i for i in self.impropers if i.atoms in itertools.permutations([atom1, atom2, atom3, atom4])]
        if all([i.isPlanar() for i in impropers]):
            return True

        if onlyimproper:
            return False
        # otherwise check if we have a proper dihedral.
        dihedrals = [d for d in self.dihedrals
                     if (d.atoms == (atom1, atom2, atom3, atom4)) or (d.atoms == (atom4, atom3, atom2, atom1))]
        if len(dihedrals) > 2:
            raise ValueError(f'More than one proper dihedral on atoms {atom1}, {atom2}, {atom3}, {atom4}! '
                             f'Check your topology!')
        elif len(dihedrals) == 1:
            # there is exactly one proper dihedral, check if it is planar or not
            return dihedrals[0].isPlanar()
        else:
            assert (not dihedrals) and (not impropers)
            raise ValueError(f'Neither proper, nor improper dihedral terms found on atoms'
                             f' {atom1}, {atom2}, {atom3}, {atom4}!')

    def generateHDB(self, keep_going: bool=False) -> List[AtomAdditionRule]:
        """Analyze hydrogens and create hydrogen database entries

        Crystal structures typically do not have information on hydrogens, therefore GROMACS has facilities to auto-
        generate missing hydrogens.

        Below we list the hydrogen addition rules known by GROMACS. The atom to which the hydrogens are attached is
        always denoted with 'i'. Other control atoms are 'j', 'k', 'l'. New hydrogens are (n, n1, n2, ...)

        1. one planar hydrogen, e.g. rings or peptide bond
            - n, i, j, k are in the same plane
            - angles n-i-j and n-i-k are equal and > 90°
            - distance of n, i is 0.1 nm

            ::

                  n
                k-i-j

        2. one single hydrogen, e.g. hydroxyl
            - angle n-i-j = 109.5 and the dihedral n-i-j-k is trans
            - distance of n, i is 0.1 nm

            ::

                 n-i-j-k

        3. two planar hydrogens: e.g. ehtylene (-C=CH2) or amide (-C(=O)NH2)
            - n1-i-j and n2-i-j are both 120° and n1-i-j-k is cis, n2-i-j-k is trans
            - distance of n{1, 2} and d is 0.1 nm

            ::

                 n1
                    \
                     i - j - k
                    /
                 n2

        4. two or three tetrahedral hydrogens, e.g. -CH3
            - n1-i-j = n2-i-j = n3-i-j = 109.47°, n1-i-j-k is trans, n2-i-j-k is trans + 120°, n3-i-j-k is trans + 240°

            ::

                n1   (n3)
                  \ /
                   i-j-k
                  /
                n2

        5. one tetrahedral hydrogen, e.g. C3CH
            - n-i-j = n-i-k = n-i-l = 109.47°

            ::

                n     j
                   i
                k     l

        6. two tetrahedral hydrogens, e.g. C-CH2-C
            - n1, n2 are on the plane bisecting angle j-i-k
            - n1-i-n2 angle is 109.47°
            - n1-i-j angle is 109.47°
            - n1-i-k angle is 109.47°

            ::

                n1     n2
                    i
                j      k

        7. two water hydrogens
            - around atom i according to SPC water geometry

        (Rules 8 and 9 correspond to COO and COOH groups in the termini database)

        10. three water "hydrogens" (e.g. for TIP4P)

        11. four water "hydrogens2 (e.g. for TIP5P)


        :param keep_going: if True, failing to assign a hydrogen addition rule is not
            fatal, the other heavy atoms are tried further.
        :type keep_going: bool
        :return: the list of hydrogen addition rules for each hydrogen addition point
        :rtype: list of :class:`AtomAdditionRule` instances
        """
        hydrogenrules: List[AtomAdditionRule] = []
        # loop over heavy atoms
        allhydrogens = [a for a in self.atoms if a.parameters.atnum == 1]
        allheavyatoms = [a for a in self.atoms if a not in allhydrogens]
        for heavyatom in allheavyatoms:
            hydrogens = [h for h in allhydrogens if self.isBonded(h.name, heavyatom.name)]
            if not hydrogens:
                # no hydrogen attached to this atom
                continue
            bondedtree = self.constructBondedTree(heavyatom.name, 3)
            hnames = sorted([h.name for h in hydrogens])
            # handle waters differently
            if self.isWater():
                for rule in [self.try_additionrule7, self.try_additionrule10, self.try_additionrule11]:
                    result = rule(hnames, heavyatom.name)
                    if not result:
                        continue
                    else:
                        hydrogenrules.extend(result)
                        break
                else:
                    raise ValueError(f'Residue {self.name} is water but no hydrogen addition rule applies')
                break

#            if len(allheavyatoms) < 3:
#                # too small molecule, e.g. OH ion
#                logger.debug('Too small molecule.')
#                continue

            if (len(hydrogens) == 3) and (len(bondedtree) == 3) and (len(self.atoms) == 4) and (
                    heavyatom.parameters.atnum == 7):
                # Ammonium ion. No hydrogen addition rule in GROMACS can handle this.
                logger.debug('Ammonium ion. No hydrogen addition rule can handle this.')
                continue

            else:
                results = []
                for rule in [self.try_additionrule1, self.try_additionrule2, self.try_additionrule3,
                             self.try_additionrule4, self.try_additionrule5, self.try_additionrule6,
                             self.try_additionrule7,
                             # rules #8 and #9 are for COO- and COOH, respectively
                             self.try_additionrule10, self.try_additionrule11]:
                    result = rule(hnames, heavyatom.name)

                    if not result:
                        continue
                    elif not any([r.inaccurate for r in result]):
                        # a reliable result has been found, accept this
                        hydrogenrules.extend(result)
                        break
                    else:
                        # inaccurate result: accumulate and try further rules
                        results.extend(result)
                else:
                    if results:
                        # we have some inaccurate results
                        cn, isnumbered = commonname(hnames)
                        if isnumbered:
                            hydrogenrules.append(results[0])
                        else:
                            for hn in hnames:
                                hydrogenrules.append([r for r in results if r.hname == hn][0])
                    else:
                        if keep_going:
                            logger.warning(
                                f'Cannot find hydrogen addition rule for atom {heavyatom.name} in residue {self.name}, '
                                f'having hydrogen(s) {hnames} and other heavy neighbour(s) '
                                f'{[k for k in bondedtree if k not in hnames]}.')
                        else:
                            raise ValueError(
                                f'Cannot find hydrogen addition rule for atom {heavyatom.name} in residue {self.name}, '
                                f'having hydrogen(s) {hnames} and other heavy neighbour(s) '
                                f'{[k for k in bondedtree if k not in hnames]}.')

        self.hydrogenadditionrules = hydrogenrules
        return hydrogenrules

    def constructBondedTree(self, rootatom: str, maxdepth: int) -> Dict[str, Any]:
        """Construct a tree of bonded atoms

        The result is a nested dictionary as follows:
            - the root level dictionary has one single key: the root atom name
            - the single item in in this (1st level) dictionary is another dictionary, the keys of which are the names
                of the bonded neighbours of the root atom
            - each item of this (1st level) dictionary is another dictionary, the keys of which are the names of the
                bonded neighbours of the atom whose name is in the key. Note that these do not include the root atom!

        Thus, when an alanine is considered, having the following topology::

            -O             O
            ||       HA   ||
            -C - N - CA - C - +N
                HN   |
                     |
              HB1 - CB - HB3
                     |
                    HB2

        the bonded tree starting with N with max depth 3 is::

            HN
            CA -> CB -> HB1
                     -> HB2
                     -> HB3
               -> C  -> +N
                     -> O
               -> HA
            -C -> -CA -> -CB
                      -> -N
                      -> -HA
               -> -O

        :param rootatom: name of the root atom
        :type rootatom: str
        :param maxdepth: maximum search depth
        :type maxdepth: int
        :return: the bonded tree in the form of nested dicts
        :rtype: dict
        """
        def _sort_atomnames(atomnames: Sequence[str]) -> List[str]:
            return sorted([an for an in atomnames if an[0] not in '+-']) + sorted([an for an in atomnames if an[0] in '+-'])

        if maxdepth == 0:
            return {}
        elif maxdepth == 1:
            # return the neighbours of this atom
            return {neighbour: {} for neighbour in _sort_atomnames(self.bondedNeighbours(rootatom)) if neighbour != rootatom}
        else:
            return {
                neighbour: {key: value for key, value in self.constructBondedTree(neighbour, maxdepth - 1).items() if
                            key != rootatom} for neighbour in _sort_atomnames(self.bondedNeighbours(rootatom))}

    def isBonded(self, atom1: str, atom2: str, dontcheckchain: bool = False) -> bool:
        """Check if two atoms are bonded or not.

        For chainable residues (where bonds exist with the previous and next residue), bonds are handled semi-smartly,
        assuming that the previous and the next residue are the same as this. Thus e.g. for a peptide, a bond between
        C and +N is explicitly listed in the topology. Thus the bonds (-C, N), (-C, -O), (-C, -CA), (+N, +CA)... are
        automatically assumed to be present. Multiple "-" and "+" signs are also handled.

        :param atom1: the name of the first atom
        :type atom1: str
        :param atom2: the name of the second atom
        :type atom2: str
        :return: True if bonded, False if not
        :rtype: bool
        """
        if ((atom1[0] not in '+-') and (atom2[0] not in '+-')) or dontcheckchain:
            # both atoms are in the current residue OR no prev-next residue resolving is to be done.
            return bool([b for b in self.bonds if (b.atoms == (atom1, atom2)) or (b.atoms == (atom2, atom1))])
        elif atom1.startswith('-') and atom2.startswith('-'):
            # the previous residue is the same as the current one.
            return self.prev.isBonded(atom1[1:], atom2[1:])
        elif atom1.startswith('+') and atom2.startswith('+'):
            # the next residue is the same as the current one
            return self.next.isBonded(atom1[1:], atom2[1:])
        elif atom1.startswith('+') and not (atom2.startswith('-') or atom2.startswith('+')):
            # can be found as (+N,C) or (N, -C)
            return self.isBonded(atom1, atom2, True) or (
                    (self.next is not None) and self.next.isBonded(atom1[1:], '-' + atom2, True))
        elif atom1.startswith('-') and not (atom2.startswith('-') or atom2.startswith('-')):
            # can be found as (-C, N) or (C, +N)
            return self.isBonded(atom1, atom2, True) or (
                    (self.prev is not None) and self.prev.isBonded(atom1[1:], '+' + atom2, True))
        else:
            return self.isBonded(atom2, atom1)

    @staticmethod
    def _normalizeatomname(atomname: str):
        """Remove superfluous +/- prefixes from an atom name

        E.g. -C -> -C,  +-C -> C, ---++-+C -> -C

        :param str atomname: atom name
        :return: the atom name after removing excess +/- prefix characters
        :rtype: str
        """
        pluses = atomname.count('+') - atomname.count('-')
        if pluses > 0:
            return '+' * pluses + atomname.replace('-', '').replace('+', '')
        elif pluses < 0:
            return '-' * abs(pluses) + atomname.replace('-', '').replace('+', '')
        else:
            return atomname.replace('-', '').replace('+', '')

    def getAtom(self, atomname: str) -> Atom:
        """Get the equivalent atom in this residue

        In some residue topologies references to previous and next residues exist in the form of
        atom names starting with '-' or '+', respectively. For the sake of simplicity we assume
        that the previous residue is the same as this. This is a convenience function to resolve
        the atom name to true :class:`Atom` instances.

        :param atomname: the name of the atom
        :type atomname: str
        :return: the :class:`Atom` instance
        :rtype: :class:`Atom`
        :raises ValueError: if no atoms or more than one atoms are found
        """
        #        logger.debug(
        #            f'Looking for atom {atomname} in residue {self.name} (prev: {self.prev.name if self.prev is not None else "NONE"}; next: {self.next.name if self.next is not None else "NONE"})')
        if atomname[0] == '-':
            if self.prev is None:
                raise ValueError(
                    f'Looking for atom {atomname} in the previous residue while there isn\'t any previous residue. {self.name=}, {self.next.name=}, {self.next.prev.name=}, {self.next.next.name=}, {self.next.next.prev.name=}, {[b.atoms for b in self.bonds]}, {[a.name for a in self.atoms]}')
            return self.prev.getAtom(atomname[1:])
        elif atomname[0] == '+':
            if self.next is None:
                raise ValueError(f'Looking for atom {atomname} in the next residue while there isn\'t any next residue. {self.name=}, {self.prev.name=}, {self.prev.next.name=}, {self.prev.prev.name=}, {self.prev.prev.next.name=}, {[b.atoms for b in self.bonds]}, {[a.name for a in self.atoms]}')
            return self.next.getAtom(atomname[1:])
        else:
            atoms = [a for a in self.atoms if a.name == atomname]
            if len(atoms) == 1:
                return atoms[0]
            else:
                raise ValueError(
                    f'Exactly one atom expected with name {atomname}, found {len(atoms)}. Atoms in this residue: {[a.name for a in self.atoms]}. Residue name {self.name}.')

    def bondedNeighbours(self, atomname: str) -> Set[str]:
        """Get a list of bonded neighbours of an atom

        Residue boundary crossing for chainable residues is done correctly.

        Caches newly found bonded neighbours in the :class:`Atom` instance, so next time the lookup will be much faster.

        :param str atomname: name of the atom
        :return: list of the names of neighbouring bonded atoms
        :rtype: set of str
        """
        logger.debug(
            f'Looking for neighbours of atom {atomname} in residue {self.name} (next: {self.next.name if self.next is not None else "NONE"}, prev: {self.prev.name if self.prev is not None else "NONE"})')
        if atomname[0] == '-':
            # this atom is in the previous residue: get all neighbours of the same atom in the previous residue
            # and append a '-' prefix. Then also add any atoms which are bonded to this atom _in_this_residue_.
            #
            # Note that this works recursively: if there are two '-' prefixes, this procedure will be done twice!
            # In this case, typically no bonds will be found in this residue which references this atom.

            # from this residue:
            neighbours = {[a for a in b.atoms if a != atomname][0] for b in self.bonds if atomname in b.atoms}
            # from the previous residue if exists:
            if self.prev is not None:
                neighbours |= {self._normalizeatomname('-' + at) for at in self.prev.bondedNeighbours(atomname[1:])}
        elif atomname[0] == '+':
            # this atom is in the next residue: get all neighbours of the same atom in the next residue and
            # append a '+' prefix. Then also add any atoms which are bonded to this atom _in_this_residue_.
            #
            # Note that this also works recursively for more '+' prefixes, as above.
            neighbours = {[a for a in b.atoms if a != atomname][0] for b in self.bonds if atomname in b.atoms}
            # from the previous residue if exists:
            if self.next is not None:
                neighbours |= {self._normalizeatomname('+' + at) for at in self.next.bondedNeighbours(atomname[1:])}
        else:
            try:
                atom = [a for a in self.atoms if a.name == atomname][0]
            except IndexError:
                raise ValueError(f'Cannot find atom for name {atomname} in residue {self.name}')
            if atom.bondedneighbours is not None:
                # if the cached result of a previous lookup is there, use it
                return set(atom.bondedneighbours)
            # first get all neighbours in this residue
            neighbours = {[a for a in b.atoms if a != atomname][0] for b in self.bonds if atomname in b.atoms}
            # get all neighbours in the previous residue
            if self.prev is not None:
                neighbours |= {["-" + a for a in b.atoms if a != "+" + atomname][0] for b in self.prev.bonds if
                               '+' + atomname in b.atoms}
            # get all neighbours from the next residue
            if self.next is not None:
                neighbours |= {["+" + a for a in b.atoms if a != "-" + atomname][0] for b in self.next.bonds if
                               '-' + atomname in b.atoms}
            atom.bondedneighbours = set(neighbours)  # save the result for further use.
        return neighbours

    def getDihedral(self, atoms: Tuple[str, str, str, str]) -> Dihedral:
        """Return the dihedral interaction entity associated with the given atoms, taking residue chaining into
        account.

        :param atoms: atom names
        :type atoms: tuple of 4 strings
        :return: the dihedral entity
        :rtype: instance of :class:`Dihedral`
        """
        # remember: only those dihedrals are defined which have at least one central atom in the residue.
        if (atoms[1][0] not in '+-') and (atoms[2][0] not in '+-'):
            # Simplest case: both central atoms are in the current residue. This is the most common, so handling this
            # first also improves performance
            dihedrals = [d for d in self.dihedrals if (d.atoms == atoms) or (d.atoms == atoms[::-1])]
            if not dihedrals:
                raise ValueError(f'No dihedral defined for atoms {atoms}.')
            elif len(dihedrals) > 1:
                raise ValueError(f'Multiple ({len(dihedrals)}) dihedrals defined for atoms {atoms}: '
                                 f'check your residue topology!')
            else:
                return dihedrals[0]
        elif atoms[1][0] == '-' and atoms[2][0] == '-':
            # shift every atom right by one residue and try again
            return self.prev.getDihedral(tuple([self._normalizeatomname('+' + a) for a in atoms]))
        elif atoms[1][0] == '+' and atoms[2][0] == '+':
            # shift every atom left by one residue and try again
            return self.next.getDihedral(tuple([self._normalizeatomname('-' + a) for a in atoms]))
        elif ((atoms[1][0] == '+') and (atoms[2][0] == '-')) or ((atoms[1][0] == '-') and (atoms[2][0] == '+')):
            # this should not occur: neither of the central atoms are in this residue: one is from the previous, the
            # other is from the next
            raise ValueError('Central atoms are from the previous and the next residue.')
        else:
            # now one of the central atoms is in the current residue and the other is not: either in the previous or the
            # next. We first try to find the dihedral in the current residue. If not found, we shift the atoms so the
            # other central atom will be in this residue and will try again.
            if '+' in [atoms[1][0], atoms[2][0]]:
                # the other atom is in the next residue
                assert '-' not in [atoms[1][0], atoms[2][0]]
                shift = '-'  # if the dihedral is not found, we will shift everything left.
                otherresidue = self.next
            else:
                # the other atom is in the previous residue
                assert '-' in [atoms[1][0], atoms[2][0]]
                # we know that "+" is not there, it was handled in the "if" clause
                shift = '+'  # if the dihedral is not found, we will shift everything right.
                otherresidue = self.prev
            for s, residue in [('', self), (shift, otherresidue)]:
                if residue is None:
                    # can happen if self.next or self.prev is None
                    continue
                shiftedatoms = tuple([self._normalizeatomname(s + a) for a in atoms])
                dihedrals = [d for d in residue.dihedrals if
                             (d.atoms == shiftedatoms) or (d.atoms == shiftedatoms[::-1])]
                if not dihedrals:
                    # try with the other shift
                    continue
                elif len(dihedrals) > 1:
                    raise ValueError(f'Multiple ({len(dihedrals)}) dihedrals defined for atoms {atoms}: '
                                     f'check your residue topology!')
                else:
                    return dihedrals[0]
            else:
                raise ValueError(f'No dihedral defined for atoms {atoms}.')

    def getAngle(self, atoms: Tuple[str, str, str]) -> Angle:
        """Get the angle entity for the given atoms, accounting for residue chaining.

        :param atoms: atom names
        :type atoms: tuple of 3 strings
        :return: the angle instance
        :rtype: instance of :class:`Angle`
        """

        # note that the central atom is always in this residue.
        if atoms[1][0] not in '+-':
            # simplest case: this angle entity should exist
            angles = [a for a in self.angles if (a.atoms == atoms) or (a.atoms == atoms[::-1])]
            if not angles:
                raise ValueError(f'No angle is defined for atoms {atoms}')
            elif len(angles) > 1:
                raise ValueError(f'Multiple ({len(angles)}) angles defined for atoms {atoms}: check your topology!')
            else:
                return angles[0]
        elif atoms[1][0] == '-':
            return self.prev.getAngle(tuple([self._normalizeatomname('+' + a) for a in atoms]))
        else:
            assert atoms[1][0] == '+'
            return self.next.getAngle(tuple([self._normalizeatomname('-' + a) for a in atoms]))

    def getBond(self, atoms: Tuple[str, str]) -> Bond:
        """Get the bond entity for the given atoms, accounting for residue chaining.

        :param atoms: atom names
        :type atoms: tuple of 2 strings
        :return: the angle instance
        :rtype: instance of :class:`Bond`
        """
        if (atoms[0][0] not in '+-') and (atoms[1][0] not in '+-'):
            # both atoms are in this residue: easy
            bonds = [b for b in self.bonds if (b.atoms == atoms) or (b.atoms == atoms[::-1])]
            if not bonds:
                raise ValueError(f'No bond found between atoms {atoms}.')
            elif len(bonds) > 1:
                raise ValueError(f'Multiple ({len(bonds)}) bonds defined between atoms {atoms}: '
                                 f'check your topology!')
            else:
                return bonds[0]
        elif (atoms[0][0] == '+') and (atoms[1][0] == '+'):
            # both atoms are in the next residue (or more far downstream)
            return self.next.getBond((atoms[0][1:], atoms[1][1:]))
        elif (atoms[0][0] == '-') and (atoms[1][0] == '-'):
            # both atoms are in the previous residue (or more far upstream)
            return self.prev.getBond((atoms[0][1:], atoms[1][1:]))
        elif ((atoms[0][0] == '-') and (atoms[1][0] == '+')) or ((atoms[0][0] == '+') and (atoms[1][0] == '-')):
            raise ValueError('Bond requested with atoms from the previous and the next residue.')
        else:
            # now at least one atom is in the current residue.
            if '+' in [atoms[0][0], atoms[1][0]]:
                assert '-' not in [atoms[0][0], atoms[1][0]]  # the other atom is in this residue.
                shiftdirection = '-'
                otherresidue = self.next
            else:
                assert '-' in [atoms[0][0], atoms[1][0]]
                shiftdirection = '+'
                otherresidue = self.prev
            for shift, residue in [('', self), (shiftdirection, otherresidue)]:
                if residue is None:
                    # can happen if self.prev or self.next is None
                    continue
                shiftedatoms = (self._normalizeatomname(shift + atoms[0]), self._normalizeatomname(shift + atoms[1]))
                bonds = [b for b in residue.bonds if (b.atoms == shiftedatoms) or (b.atoms == shiftedatoms[::-1])]
                if not bonds:
                    # try with the other shift
                    continue
                elif len(bonds) > 1:
                    raise ValueError(f'Multiple ({len(bonds)}) bonds defined between atoms {atoms}: '
                                     f'check your topology!')
                else:
                    return bonds[0]
            else:
                raise ValueError(f'No bond found for atoms {atoms}')

    def getImproper(self, atoms: Tuple[str, str, str, str]) -> Improper:
        for ordered in [atoms, atoms[::-1]]:  # check forward and backward ordering
            # ensure that the first atom is in the current residue
            while ordered[0][0] == '-':
                ordered = tuple([self._normalizeatomname('+' + a) for a in ordered])
            while ordered[0][0] == '+':
                ordered = tuple([self._normalizeatomname('-' + a) for a in ordered])
            assert ordered[0][0] not in '+-'
            impropers = [i for i in self.impropers if i.atoms == ordered]
            if not impropers:
                continue  # check backwards order
            elif len(impropers) > 1:
                raise ValueError(f'Improper dihedral for atoms {atoms} defined more than once ({len(impropers)}×): '
                                 f'check your topology!')
            else:
                return impropers[0]
        else:
            raise ValueError(f'Improper dihedral for atoms {atoms} could not be found.')

    def getCmap(self, atoms: Tuple[str, str, str, str, str]) -> Cmap:
        if atoms[2][0] == '-':
            return self.prev.getCmap(tuple([self._normalizeatomname('+' + a) for a in atoms]))
        elif atoms[2][0] == '+':
            return self.next.getCmap(tuple([self._normalizeatomname('-' + a) for a in atoms]))
        assert atoms[2][0] not in '+-'
        cmaps = [c for c in self.cmaps if c.atoms == atoms]
        if not cmaps:
            raise ValueError(f'No cmap entry found for atoms {atoms}.')
        elif len(cmaps) > 1:
            raise ValueError(f'Cmap entry for atoms {atoms} defined more than once ({len(cmaps)}×): '
                             f'check your topology!')
        else:
            return cmaps[0]

    def isLeftTerminal(self) -> bool:
        """Check if this residue is a left terminus (N or 5' etc.)"""
        if self.locationinchain is None:
            self.classifyChainLocation()
        return self.locationinchain == LocationInChain.Left

    def isRightTerminal(self) -> bool:
        """Check if this residue is a right terminus (C or 3' etc.)"""
        if self.locationinchain is None:
            self.classifyChainLocation()
        return self.locationinchain == LocationInChain.Right

    def isChainable(self) -> bool:
        """Is this residue chainable (amino acid, nucleotide etc.)"""

        # chainable if we have a reference to an atom in the previous or the next residue
        if self.locationinchain is None:
            self.classifyChainLocation()
        return self.locationinchain != LocationInChain.NotChainable

    def classifyChainLocation(self, loc: Optional[LocationInChain] = None) -> LocationInChain:
        """Check if the residue can be chained and if yes, at which part of the chain can it occur.
        """
        if loc is not None:
            self.locationinchain = loc
            if loc in [LocationInChain.NotChainable, LocationInChain.Left, LocationInChain.Right]:
                self.prev = None
                self.next = None
            return self.locationinchain
        atomsfromthepreviousresidue = []
        atomsfromthenextresidue = []
        for interactionlist in [self.bonds, self.angles, self.impropers, self.dihedrals, self.cmaps]:
            for i in interactionlist:
                atomsfromthepreviousresidue.extend([a for a in i.atoms if a[0] == '-'])
                atomsfromthenextresidue.extend([a for a in i.atoms if a[0] == '+'])
        if (not atomsfromthenextresidue) and (not atomsfromthepreviousresidue):
            return self.classifyChainLocation(LocationInChain.NotChainable)
        elif atomsfromthenextresidue and atomsfromthepreviousresidue:
            return self.classifyChainLocation(LocationInChain.Mid)
        else:
            notfoundfromnext = [(not [a for a in self.atoms if a.name == anext[1:]]) for anext in
                                atomsfromthenextresidue]
            notfoundfromprev = [(not [a for a in self.atoms if a.name == aprev[1:]]) for aprev in
                                atomsfromthepreviousresidue]
            if notfoundfromnext and notfoundfromprev:
                # this is not typical
                raise ValueError('A residue cannot be at both termini.')
            elif any(notfoundfromprev):
                # some atoms referenced from the previous residue are not here, e.g. no "C" but "-C" is found: this is
                # a right terminus
                return self.classifyChainLocation(LocationInChain.Right)
            elif any(notfoundfromnext):
                return self.classifyChainLocation(LocationInChain.Left)
            else:
                return self.classifyChainLocation(LocationInChain.Mid)

    # Hydrogen addition rules. It is typically not checked if the new atom(s) are really hydrogens: these can be
    # reused for atom additions in the termini databases.
    # All of these functions return the AtomAdditionRule instance if successful, None if not successful.

    def try_additionrule1(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #1: one planar hydrogen, e.g. rings or peptide bonds

        From the GROMACS documentation:

        "One hydrogen atom (n) is generated, lying in the plane of atoms (i,j,k) on the plane bisecting angle (j-i-k) at
        a distance of 0.1 nm from atom i, such that the angles (n-i-j) and (n-i-k) are >90°"

        Here two cases are checked. In both cases it is required that the base atom (1st control atom) has two
        non-hydrogen (or already existing) neighbours, j and k

        1. peptide bonds: there is a planar, improper dihedral on n, i, j1, j2, where j1 and j2 are neighbours of i.
            Control atoms will be i, j1, j2.

        2. aromatic ring (e.g. BENZ or PHE): atom i has two neighbours: j1 and j2. If we can find two other atoms (
            named k1 and k2) which are neighbours of j1 and j2, respectively, and the following dihedrals are planar,
            then i, j1 and j2 are the control atoms:
            - k1-j1-i-n
            - k2-j2-i-n
            - k1-j1-i-j2
            - k2-j2-i-j1

        3. if neither of the above, accept the rule provisionally, i.e. flagged as "inaccurate". Because no other rules
            can handle situations with 1 new atom and 2 already existing neighbours / 1 hydrogen and 2 other neighbours.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """
        bondedtree = self.constructBondedTree(baseatom, 3)
        if (len(newatoms) != 1) or (len(bondedtree) != 3):
            return []
        newatom = newatoms[0]

        logger.debug('One hydrogen and 2 bonded neighbours: trying planar geometry (rule #1)')

        # Case #1: try to find an improper dihedral
        name_j1, name_j2 = [key for key in bondedtree if
                            key not in newatoms]  # the two other neighbours of the base atom
        improper = [i for i in self.impropers
                    if i.atoms[0] == baseatom
                    and i.atoms[1:] in itertools.permutations((newatom, name_j1, name_j2), 3)]
        if improper and all([i.isPlanar() for i in improper]):
            # only planar impropers exist
            return [AtomAdditionRule(1, 1, newatom, (baseatom, name_j1, name_j2), 'Planarity due to improper dihedral')]

        # case #2
        for name_k1, name_k2 in itertools.product(list(bondedtree[name_j1]), list(bondedtree[name_j2])):
            # check proper dihedrals
            if (name_k1 in newatoms) or (name_k2 in newatoms):
                # we are looking for heavy atoms, hydrogens cannot be control atoms!
                continue
            dihedrals = [self.getDihedral((name_k1, name_j1, baseatom, newatom)),
                         self.getDihedral((name_k2, name_j2, baseatom, newatom)),
                         self.getDihedral((name_k1, name_j1, baseatom, name_j2)),
                         self.getDihedral((name_k2, name_j2, baseatom, name_j1))]
            isplanar = [d.isPlanar() for d in dihedrals]
            logger.debug(
                f'Checking planarity of ring-like dihedrals with atoms i={baseatom}, j1={name_j1}, j2={name_j2}, '
                f'k1={name_k1}, k2={name_k2}: {isplanar} ')
            if all(isplanar):
                return [AtomAdditionRule(1, 1, newatom, (baseatom, name_j1, name_j2),
                                         'Planarity due to proper dihedrals (e.g. in a ring)')]
        else:
            # we can rule out rule #1: nothing ensures planarity. However, accept this provisionally
            return [AtomAdditionRule(1, 1, newatom, (baseatom, name_j1, name_j2),
                                     'Not planar, but there is no other rule to apply for this chemical environment.',
                                     True)]

    def try_additionrule2(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #2: one single hydrogen, e.g. hydroxyl

        From the GROMACS documentation:

        "One hydrogen atom (n) is generated at a distance of 0.1 nm from atom i, such that angle (n-i-j)=109.5° and
        dihedral (n-i-j-k)=trans"

        This means a hydrogen (new atom) at the end of a linear chain of atoms. We require here that the base atom has
        one atom to be added and only one other existing neighbour (denoted j). We try to find k, a neighbour of j which
        fulfills the (n-i-j-k)=trans requirement. If not found, we are also happy with simple planarity (dihedral
        potential terms are only corrections for VdW and Coulomb terms, so they can also seem to prefer the cis
        conformation, while in reality they are just countering an overzealous VdW attraction). If no planarity found,
        we take the first available neighbour of j, hoping that an energy minimization will resolve the inaccuracy.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """
        bondedtree = self.constructBondedTree(baseatom, 3)
        if (len(newatoms) != 1) or (len(bondedtree) != 2):
            return []
        newatom = newatoms[0]

        logger.debug('One hydrogen and one other bonded neighbour: trying single hydrogen rule (rule #2)')
        # the heavy atom has one hydrogen and one other heavy atom neighbour
        name_j = [key for key in bondedtree if key not in newatoms][0]

        # find the third control atom, so the dihedral H-A-B-C is trans.
        for strict in [True, False]:  # first try it with trans, then with cis also enabled.
            candidate_k = [
                key for key in bondedtree[name_j]
                if (key not in newatoms)  # hydrogens cannot e control atoms
                   and self.getDihedral((newatom, baseatom, name_j, key)).isTrans(strict)]
            if candidate_k:
                return [AtomAdditionRule(
                    1, 2, newatom, (baseatom, name_j, candidate_k[0]),
                    None if strict else 'n-i-j-k dihedral is probably not trans', not strict)]
        else:
            logger.warning('Cannot find third control atom for trans conformation. Using the first available')
            return [AtomAdditionRule(
                1, 2, newatom,
                (baseatom, name_j,
                 [k for k in bondedtree[name_j] if self.getAtom(k).parameters.atnum > 1][0]),
                'n-i-j-k dihedral is probably not trans (nor planar!)', True)]

    def try_additionrule3(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #3: two planar hydrogens, e.g. ethylene -C=CH2 or amide -C(=O)NH2

        From the GROMACS documentation:

        "Two hydrogens (n1,n2) are generated at a distnace of 0.1 nm from atom i, such that angle (n1-i-j)=(n2-i-j)=120°
        and dihedral (n1-i-j-k)=cis and (n2-i-j-k)=trans, such that names are according to IUPAC standards"

        We require that we have two new (n1,n2) and one already existing (j) neighbour of the base atom, and that some
        form of planarity of n1, n2, i, j is ensured:

        - if an improper dihedral (centered on any of the four atoms) ensures the planarity of n1,n2,i,j, a
          neighbour of j (denoted k) will be looked for which satisfies the cis and trans requirements. If no such
          k is found, simple planarity is sufficient. If nothing ensures the planarity of (n1/n2)-i-j-k for any k,
          the first k is used
        - a neighbour of j (denoted k) is searched for, for which the dihedrals (n1/n2)-i-j-k is cis or trans,
          respectively. If cis/trans cannot be ensured, planarity is required.

        In both of the above cases, angles n1-i-j and n2-i-j are checked to be near 120°.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """
        bondedtree = self.constructBondedTree(baseatom, 3)
        if (len(newatoms) != 2) or (len(bondedtree) != 3):
            return []

        nname, isnumbered = commonname(newatoms)
        # aim for hydrogen addition rule #3: two hydrogens at the end of a chain, in planar configuration
        logger.debug('Two hydrogens and one other bonded neighbour: trying end-of-chain planar hydrogens (rule #3)')
        name_j = [key for key in bondedtree if key not in newatoms][0]
        rulesfound = []

        not120 = False
        # check equilibrium angle value
        angles = [self.getAngle((n, baseatom, name_j)) for n in newatoms]
        if not all([a.is120(5.0) for a in angles]):
            # Not all tetrahedrals are 120°: never mind, just give a warning.
            logger.debug(f'120° requirements not satisfied: {[a.parameters[0].Theta0 for a in angles]}')
            not120 = True

        # check if we have a planarity constraint on n1, n2, i, j
        impropers = [
            i for i in self.impropers
            if (i.atoms[0] == baseatom) and (i.atoms[1:] in itertools.permutations(tuple(newatoms) + (name_j,), 3))]
        if impropers and all([i.isPlanar() for i in impropers]):
            # rule #3 satisfied. Find the 3rd control atom for which n1-i-j-k is cis, n2-i-j-k is trans
            for strict in [True, False]:
                # first try with strict
                for name_k1, name_k2 in itertools.product(bondedtree[name_j], bondedtree[name_j]):
                    if (isnumbered and (name_k1 != name_k2)) or ((not isnumbered) and (name_k1 == name_k2)) or (
                            name_k1 in newatoms) or (name_k2 in newatoms):
                        continue
                    dihedral1 = self.getDihedral((newatoms[0], baseatom, name_j, name_k1))
                    dihedral2 = self.getDihedral((newatoms[1], baseatom, name_j, name_k2))
                    if dihedral1.isCis(strict) and dihedral2.isTrans(strict):
                        if isnumbered:
                            return [AtomAdditionRule(
                                2, 3, nname, (baseatom, name_j, name_k1),
                                None if strict else 'n12-i-j-k cis/trans requirement not met, but n12-i-j-k is planar',
                                (not strict) or not120)]
                        else:
                            return [
                                AtomAdditionRule(1, 3, newatoms[0], (baseatom, name_j, name_k1),
                                                 None if strict else 'n12-i-j-k cis/trans requirement not met, but n12-i-j-k is planar',
                                                 (not strict) or not120),
                                AtomAdditionRule(1, 3, newatoms[1], (baseatom, name_j, name_k2),
                                                 None if strict else 'n12-i-j-k cis/trans requirement not met, but n12-i-j-k is planar',
                                                 (not strict) or not120),
                            ]
                else:
                    # no 3rd atom found for cis/trans or planarity, use the first one anyway
                    logger.warning('No cis/trans or planarity, using the first available k atom.')
                    if isnumbered:
                        return [AtomAdditionRule(
                            2, 3, nname, (baseatom, name_j, list(bondedtree[name_j])[0]),
                            'n12-i-j-k is probably not planar', True)]
                    else:
                        return [
                            AtomAdditionRule(1, 3, newatoms[0], (baseatom, name_j, list(bondedtree[name_j])[0]),
                                             'n12-i-j-k is probably not planar', True),
                            AtomAdditionRule(1, 3, newatoms[1], (baseatom, name_j, list(bondedtree[name_j])[1]),
                                             'n12-i-j-k is probably not planar', True),
                        ]

        # There is no improper dihedral on atoms n1, n2, i, j
        # Try to find a suitable "k" atom to ensure cis/trans requirements, or at least planarity
        for strict in [True, False]:
            for name_k1, name_k2 in itertools.product(bondedtree[name_j], bondedtree[name_j]):
                if (isnumbered and (name_k1 != name_k2)) or ((not isnumbered) and (name_k1 == name_k2)) or (
                        name_k1 in newatoms) or (name_k2 in newatoms):
                    continue
                dihedral1 = self.getDihedral((newatoms[0], baseatom, name_j, name_k1))
                dihedral2 = self.getDihedral((newatoms[1], baseatom, name_j, name_k2))
                if dihedral1.isCis(strict) and dihedral2.isTrans(strict):
                    # found a 3rd control atom for planar configuration -> rule #3
                    if isnumbered:
                        return [AtomAdditionRule(
                            2, 3, nname, (baseatom, name_j, name_k1),
                            None if strict else 'cis/trans requirements probably not met', (not strict) or (not120))]
                    else:
                        return [
                            AtomAdditionRule(
                                1, 3, newatoms[0], (baseatom, name_j, name_k1),
                                None if strict else 'cis/trans requirements probably not met', (not strict) or (not120)
                            ),
                            AtomAdditionRule(
                                1, 3, newatoms[1], (baseatom, name_j, name_k2),
                                None if strict else 'cis/trans requirements probably not met', (not strict) or (not120)
                            )

                        ]
        return []  # probably tetrahedral.

    def try_additionrule4(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #4: two or three tetrahedral hydrogens, e.g. -CH3

        From the GROMACS documentation:

        "Three (n1,n2,n3) or two (n1,n2) hydrogens are generated at a distance of 0.1 nm from atom i, such that angle
        (n1-i-j)=(n2-i-j)=(n3-i-j)=109.47°, dihedral (n1-i-j-k)=trans, (n2-i-j-k)=trans+120° and (n3-i-j-k)=trans+240."

        We require that we have two (n1,n2) or three (n1,n2,n3) new and exactly one already existing (j) neighbour of
        the base atom. After this, a k atom is looked for, which:

        - fulfills the requirement of 3-fold rotation symmetry as above, at least approximately
        - if not found, use the first available k

        In both of the above cases, angles n1-i-j and n2-i-j (and n3-i-j) are checked to be near 109°.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """
        bondedtree = self.constructBondedTree(baseatom, 3)
        if not (((len(newatoms) == 2) and (len(bondedtree) == 3)) or ((len(newatoms) == 3) and (len(bondedtree) == 4))):
            return []

        nname, isnumbered = commonname(newatoms)
        # aim for hydrogen addition rule #4: two or three hydrogens at the end of a chain, in tetrahedral configuration
        logger.debug(
            'Two or three hydrogens and one other bonded neighbour: '
            'trying end-of-chain tetrahedral hydrogens (rule #4)')
        name_j = [key for key in bondedtree if key not in newatoms][0]

        if not bondedtree[name_j]:
            # the whole molecule looks like this:
            #
            #       n1
            #       |
            #  n2 - i - j        ( no k)
            #       |
            #      n3
            #
            # we cannot assign a hydrogen addition rule here.
            logger.debug(f'Molecule {self.name} too small ({len(self.atoms)} atoms): '
                         f'cannot assign hydrogen addition rule.')
            return []

        nottetrahedral = False
        angles = [self.getAngle((n, baseatom, name_j)) for n in newatoms]
        if not all([a.istetrahedral(9.0) for a in angles]):
            logger.debug(f'Angle tetrahedrality not met: {[a.parameters[0].Theta0 for a in angles]}')
            nottetrahedral = True

        k_candidates = [k for k in bondedtree[name_j] if k not in newatoms]
        for conf in ['gauche', 'eclipsed']:
            for name_k1, name_k2, name_k3 in itertools.product(
                    k_candidates,
                    k_candidates,
                    k_candidates,
            ):
                if ((isnumbered and ((name_k1 != name_k2) or (name_k1 != name_k3) or (name_k2 != name_k3))) or
                        (not isnumbered and ((name_k1 == name_k2) or (name_k2 == name_k3) or (name_k1 == name_k3)))
                ):
                    # if the new atoms are numbered (e.g. H1, H2, H3), a single AtomAdditionRule will be made, with
                    # only one set of control atoms, thus name_k1 == name_k2 == name_k3 is required.
                    # If they are not numbered (e.g. H', H'', H'''), each new atom needs
                    # a dedicated AtomAdditionRule, where the 3rd atom is different, so pdb2gmx won't put 2 hydrogens at
                    # the same position. Thus the name_k1, name_k2 and name_k3 must all be different.
                    continue
                # analyze n-i-j-k dihedrals
                dihedrals = [self.getDihedral((n, baseatom, name_j, name_k))
                             for n, name_k in zip(newatoms, [name_k1, name_k2, name_k3])]

                isconf = [d.isGauche(transtoo=True) if conf == 'gauche' else d.isEclipsed(cistoo=True) for d in
                          dihedrals]
                logger.debug(f'Trying k={name_k1, name_k2, name_k3}. Dihedrals {conf}: {isconf}')
                if all(isconf):
                    # rule #4 applies here.
                    if conf == 'eclipsed':
                        logger.warning('Only found 3rd control atom with eclipsed conformation!')
                    if isnumbered:
                        assert name_k1 == name_k2 == name_k3
                        return [AtomAdditionRule(
                            len(newatoms), 4, nname, (baseatom, name_j, name_k1),
                            None if (conf == 'gauche') else
                            '3rd control atom found only with eclipsed conformation',
                            (conf == 'eclipsed') or nottetrahedral)]
                    else:
                        # one atom addition rule for each new atom.
                        return [AtomAdditionRule(
                            1, 4, n, (baseatom, name_j, name_k),
                            None if (conf == 'gauche') else '3rd control atom found only with eclipsed conformation',
                            (conf == 'eclipsed') or nottetrahedral)
                            for n, name_k in zip(newatoms, [name_k1, name_k2, name_k3])]

        # if we reach this position, neither gauche, nor eclipsed conformation was OK.
        logger.warning(
            'Could not find third control atom for three tetrahedral hydrogens honoring '
            'trans/cis conformation requirements: going with the first one.')
        if isnumbered:
            return [AtomAdditionRule(len(newatoms), 4, nname, (baseatom, name_j, list(bondedtree[name_j])[0]),
                                     'Approximate: trans/cis requirements might not be met.', True)]
        else:
            if len(newatoms) > len(bondedtree[name_j]):
                logger.error(
                    f'Not enough 3rd level base atoms for adding atoms {newatoms} onto atom {baseatom} in residue {self.name} using rule #4')
                return []
            return [AtomAdditionRule(
                1, 4, n, (baseatom, name_j, name_k), 'Approximate: trans/cis requirements might not be met', True)
                for n, name_k in zip(newatoms, bondedtree[name_j])
            ]

    def try_additionrule5(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #5: one tetrahedral hydrogen, e.g. C3CH

        From the GROMACS documentation:

        "One hydrogen atom (n′) is generated at a distance of 0.1 nm from atom i in tetrahedral conformation such that
        angle (n′-i-j)=(n′-i-k)=(n′-i-l)=109.47°"

        We require that we have one new (n) and three already existing (j, k, l) neighbours of the base atom (i). If
        the following angles are all 109.47°, the rule applies:

        - n-i-j
        - n-i-k
        - n-i-l
        - j-i-l
        - j-i-k
        - k-i-l

        If one or more not, we still apply the rule, but with a warning.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """
        # one tetrahedral hydrogen, i.e. hydrogen rule #5
        bondedtree = self.constructBondedTree(baseatom, 3)
        if (len(newatoms) != 1) or (len(bondedtree) != 4):
            return []

        newatom = newatoms[0]

        logger.debug('One hydrogen and three other bonded neighbours: one tetrahedral hydrogen '
                     '(hydrogen addition rule #5)')
        name_j, name_k, name_l = [k for k in bondedtree if k != newatom]
        angles = [
            self.getAngle((newatom, baseatom, name_j)),
            self.getAngle((newatom, baseatom, name_k)),
            self.getAngle((newatom, baseatom, name_l)),
            self.getAngle((name_j, baseatom, name_l)),
            self.getAngle((name_j, baseatom, name_k)),
            self.getAngle((name_k, baseatom, name_l)),
        ]
        if not all([a.istetrahedral(9.0) for a in angles]):
            logger.warning('Not all angles are tetrahedral '
                           f'{", ".join(["{:.3f}°".format(a.parameters[0].Theta0) for a in angles])}')
            return [AtomAdditionRule(
                1, 5, newatom, (baseatom, name_j, name_k, name_l),
                f'Not all angles were strictly tetrahedral: '
                f'{", ".join(["{:.3f}°".format(a.parameters[0].Theta0) for a in angles])}', True)]
        else:
            return [AtomAdditionRule(1, 5, newatom, (baseatom, name_j, name_k, name_l))]

    def try_additionrule6(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #6: two tetrahedral hydrogens, e.g. C-CH2-C

        From the GROMACS documentation:

        "Two hydrogen atoms (n1,n2) are generated at a distance of 0.1 nm from atom i in tetrahedral conformation on
        the plane bisecting angle j-i-k with angle (n1-i-n2)=(n1-i-j)=(n1-i-k)=109.47°."

        We require that we have one new (n) and three already existing (j, k, l) neighbours of the base atom (i). If
        the following angles are all 109.47°, the rule applies:

        - n1-i-j
        - n1-i-k
        - n2-i-j
        - n2-i-k
        - n1-i-n2
        - j-i-k

        If one or more not, we still apply the rule, but with a warning.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """
        # one tetrahedral hydrogen, i.e. hydrogen rule #5
        bondedtree = self.constructBondedTree(baseatom, 3)
        if (len(newatoms) != 2) or (len(bondedtree) != 4):
            return None

        nname, isnumbered = commonname(newatoms)
        # two tetrahedral hydrogens, e.g. on an aliphatic carbon, i.e. hydrogen rule #6
        logger.debug('Two hydrogens and two other bonded neighbours: two tetrahedral hydrogens '
                     '(hydrogen addition rule #6)')
        name_j, name_k = [k for k in bondedtree if k not in newatoms]
        angles = [
            self.getAngle((newatoms[0], baseatom, name_j)),
            self.getAngle((newatoms[0], baseatom, name_k)),
            self.getAngle((newatoms[1], baseatom, name_j)),
            self.getAngle((newatoms[1], baseatom, name_k)),
            self.getAngle((newatoms[0], baseatom, newatoms[1])),
            self.getAngle((name_j, baseatom, name_k)),
        ]
        nottetrahedral = not all([a.istetrahedral(9.0) for a in angles])
        if nottetrahedral:
            logger.warning('Not all angles are tetrahedral '
                           f'{", ".join(["{:.3f}°".format(a.parameters[0].Theta0) for a in angles])}')
        if isnumbered:
            return [AtomAdditionRule(
                2, 6, nname, (baseatom, name_j, name_k),
                ('Not all angles were strictly tetrahedral: ' +
                 f'{", ".join(["{:.3f}°".format(a.parameters[0].Theta0) for a in angles])}') if nottetrahedral else None,
                nottetrahedral)]
        else:
            return [AtomAdditionRule(
                1, 6, newatoms[0], (baseatom, name_j, name_k),
                ('Not all angles were strictly tetrahedral: ' +
                 f'{", ".join(["{:.3f}°".format(a.parameters[0].Theta0) for a in angles])}') if nottetrahedral else None,
                nottetrahedral),
                AtomAdditionRule(
                    1, 6, newatoms[1], (baseatom, name_k, name_j),
                    ('Not all angles were strictly tetrahedral: ' +
                     f'{", ".join(["{:.3f}°".format(a.parameters[0].Theta0) for a in angles])}') if nottetrahedral else None,
                    nottetrahedral),
            ]

    def try_additionrule7(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #7: two water hydrogens

        From the GROMACS documentation:

        "Two hydrogens are generated around atom i according to SPC water geometry. The symmetry axis will alternate
        between three coordinate axes in both directions."

        We require that the whole residue consists of three atoms, one of which (the base atom) is an oxygen and the
        other two (newatoms) are hydrogens.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """

        if ((len(self.atoms) == 3)
                and (len(newatoms) == 2)
                and (self.getAtom(baseatom).parameters.atnum == 8)
                and (self.getAtom(newatoms[0]).parameters.atnum == 1)
                and (self.getAtom(newatoms[1]).parameters.atnum == 1)):
            return [AtomAdditionRule(2, 7, commonname(newatoms)[0], (baseatom,))]
        else:
            return []

    def try_additionrule8(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #8: two carboxyl oxygens, -COO-

        From the GROMACS documentation:

        "Two oxygens (n1,n2) are generated according to rule 3, at a distance of 0.136 nm from atom i and an angle
        (n1-i-j)=(n2-i-j)=117°"

        We check that the base atom is a carbon, and the two new atoms are oxygens, and also that the two new atoms
        should not have any other neighbours. We don't check angle constraints.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """
        bondedtree = self.constructBondedTree(baseatom, 3)
        if ((len(newatoms) == 2) and
                (len(bondedtree) == 3) and
                (self.getAtom(baseatom).parameters.atnum == 6) and
                (self.getAtom(newatoms[0]).parameters.atnum == 8) and
                (self.getAtom(newatoms[1]).parameters.atnum == 8) and
                (not bondedtree[newatoms[0]]) and
                (not bondedtree[newatoms[1]])):
            name_j = [key for key in bondedtree if key not in newatoms][0]
            name_k = [key for key in bondedtree[name_j] if self.getAtom(key).parameters.atnum > 1][0]
            return [AtomAdditionRule(
                2, 8, commonname(newatoms)[0], (baseatom, name_j, name_k))]
        else:
            return []

    def try_additionrule9(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #9: carboxyl oxygens and hydrogen, -COOH

        From the GROMACS documentation:

        "Two oxygens (n1,n2) are generated according to rule 3, at distances of 0.123 nm and 0.125 nm from atom i for
        n1 and n2, respectively, and angles (n1-i-j)=121 and (n2-i-j)=115 degrees. One hydrogen (n′) is generated
        around n2 according to rule 2, where n-i-j and n-i-j-k should be read as n′-n2-i and n′-n2-i-j, respectively."

        We check that the base atom is a carbon, and the two new atoms are oxygens, and also that one of the two new
        atoms has a hydrogen, the other doesn't. We don't check angle constraints.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """
        bondedtree = self.constructBondedTree(baseatom, 3)
        if ((len(newatoms) == 2) and
                (len(bondedtree) == 3) and
                (self.getAtom(baseatom).parameters.atnum == 6) and
                (self.getAtom(newatoms[0]).parameters.atnum == 8) and
                (self.getAtom(newatoms[1]).parameters.atnum == 8) and
                (len(bondedtree[newatoms[0]]) + len(bondedtree[newatoms[1]]) == 1)):
            name_j = [key for key in bondedtree if key not in newatoms][0]
            name_k = [key for key in bondedtree[name_j] if self.getAtom(key).parameters.atnum > 1][0]
            return [AtomAdditionRule(
                2, 9, commonname(newatoms)[0], (baseatom, name_j, name_k))
            ]
        else:
            return []

    def try_additionrule10(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #10: three water "hydrogens"

        From the GROMACS documentation:

        "Two hydrogens are generated around atom i according to SPC water geometry. The symmetry axis will alternate
        between three coordinate axes in both directions. In addition, an extra particle is generated on the position
        of the oxygen with the first letter of the name replaced by ‘M’. This is for use with four-atom water models
        such as TIP4P."

        We require that the whole residue consists of four atoms, one of which (the base atom) is an oxygen and the
        other three (newatoms) are hydrogens or dummy atoms.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """

        if ((len(self.atoms) == 4)
                and (len(newatoms) == 3)
                and (self.getAtom(baseatom).parameters.atnum == 8)
                and (self.getAtom(newatoms[0]).parameters.atnum <= 1)
                and (self.getAtom(newatoms[1]).parameters.atnum <= 1)
                and (self.getAtom(newatoms[2]).parameters.atnum <= 1)
        ):
            return [AtomAdditionRule(3, 10, commonname(newatoms)[0], (baseatom,))]
        else:
            return []

    def try_additionrule11(self, newatoms: Sequence[str], baseatom: str) -> List[AtomAdditionRule]:
        """Check the applicability of atom addition rule #11: four water "hydrogens"

        From the GROMACS documentation:

        "Same as above, except that two additional particles are generated on the position of the oxygen, with names
        ‘LP1’ and ‘LP2.’ This is for use with five-atom water models such as TIP5P."

        We require that the whole residue consists of five atoms, one of which (the base atom) is an oxygen and the
        other four (newatoms) are hydrogens or dummies.

        :param str baseatom: the name of the base atom
        :param newatoms: new atom name(s)
        :type newatoms: list of str
        :return: list of atom addition rules (empty if not applicable)
        :rtype: list of :class:`AtomAdditionRule` instances
        """

        if ((len(self.atoms) == 5)
                and (len(newatoms) == 4)
                and (self.getAtom(baseatom).parameters.atnum == 8)
                and (self.getAtom(newatoms[0]).parameters.atnum <= 1)
                and (self.getAtom(newatoms[1]).parameters.atnum <= 1)
                and (self.getAtom(newatoms[2]).parameters.atnum <= 1)
                and (self.getAtom(newatoms[3]).parameters.atnum <= 1)
        ):
            return [AtomAdditionRule(4, 11, commonname(newatoms)[0], (baseatom,))]
        else:
            return []

    def isWater(self, numvsites: Optional[int] = None) -> bool:
        """Check if this residue is a water molecule"""
        if numvsites is None:
            return self.isWater(0) or self.isWater(1) or self.isWater(2)
        if len(self.atoms) != 3 + numvsites:
            return False
        try:
            oxygen = [a for a in self.atoms if a.parameters.atnum == 8][0]
        except IndexError:
            return False
        hydrogens = [a for a in self.atoms if a.parameters.atnum == 1]
        if len(hydrogens) != 2:
            return False
        vsites = [a for a in self.atoms if a.parameters.atnum == 0]
        if len(vsites) != numvsites:
            return False
        if not (self.isBonded(oxygen.name, hydrogens[0].name, dontcheckchain=True) and
                self.isBonded(oxygen.name, hydrogens[1].name, dontcheckchain=True)):
            return False
        return True

    def toMoleculeType(self, moltypename: Optional[str] = None, posresmacro: Optional[str] = None,
                       posresforce: float = 1000.0) -> str:
        """Make an [ moleculetype ] entry

        Before calling this, angles and dihedrals must be auto-generated and at least atom types assigned.

        :param moltypename: name of the molecule type (by default the residue name)
        :type moltypename: str or None
        :param posresmacro: macro name for position restraints. If not given, position restraints are not generated
        :type posresmacro: str or None
        :param posresforce: position restraint force constant (kJ/mol/nm^2)
        :type posresforce: float
        :return: the [ moleculetype ] entry which can be written in a .itp file
        :rtype: str
        """
        if self.isChainable():
            raise ValueError(f'Cannot make a [ moleculetype ] entry from a chainable residue {self.name}')
        if any([a.parameters is None for a in self.atoms]):
            raise ValueError(f'Atom types not assigned in {self.name}')
        if len(self.atoms) >= 3 and not self.angles:
            raise ValueError(f'More than 2 atoms and no angles in {self.name}: angles have not been autogenerated!')

        s = '[ moleculetype ]\n'
        if self.comment:
            s += f';{self.comment}\n'
        s += '; molname  nrexcl\n'
        s += f'{moltypename if moltypename is not None else self.name:<10s} 3\n\n'
        s += '[ atoms ]\n'
        s += '; id    at type     res nr  res name   at name  cg nr  charge    mass\n'
        atom2index = {}
        for i, at in enumerate(self.atoms, start=1):
            s += f'  {i:<5d} {at.atomtype:<11s} 1      {self.name:<10s} {at.name:<8s} {at.cgroup:<6d} ' \
                 f'{at.pcharge:>6.3f}    {at.parameters.mass:>8.5f}\n'
            atom2index[at.name] = i
        s = s[:-1] + f' ; qtot = {sum([a.pcharge for a in self.atoms]):.6f}\n'

        # generate 1-4 pair list
        pairs = []
        for i, at in enumerate(self.atoms, start=1):
            for path in bondedtree2list(self.constructBondedTree(at.name, 3)):
                if len(path) == 3:
                    j = atom2index[path[-1]]
                    if i < j:
                        pairs.append((i, j))

        def toindexed(lis: List[TopologyInteraction]) -> List[Tuple[int, ...]]:
            """Convert atom names to indexes in an interaction list"""
            return [tuple([atom2index[a] for a in item.atoms]) for item in lis]

        for lis, caption, functype in [
            (toindexed(self.bonds), 'bonds', 1),
            (pairs, 'pairs', 1),
            (toindexed(self.angles), 'angles', 5),
            (toindexed(self.dihedrals), 'dihedrals', 9),
            (toindexed(self.impropers), 'dihedrals', 2),
            (toindexed(self.cmaps), 'cmaps', 1),
        ]:
            if not lis:
                continue
            s += f'\n[ {caption} ]\n'
            s += '; ' + (' '.join([f'a{chr(ord("i") + i)}     ' for i in range(len(lis[0]))]))[2:] + 'funct\n'
            for item in lis:
                sorteditem = item if item < item[::-1] else item[::-1]
                s += ' '.join([f'{index:<7d}' for index in sorteditem]) + f'{functype:d}\n'

        if posresmacro is not None:
            s += f'\n#ifdef {posresmacro}'
            s += '\n[ position_restraints ]\n'
            s += ';  atom  type      fx      fy      fz\n'
            for atom in self.atoms:
                if atom.parameters.atnum > 1:
                    s += f'{atom2index[atom.name]:>7d}     1 {posresforce:>7.2f} {posresforce:>7.2f} {posresforce:>7.2f}\n'
            s += '#endif\n'
        return s

    def __add__(self, other: "ResidueTopology") -> "ResidueTopology":
        """Chain two residues: this <-> other"""
        # check if all interactions referencing neighbouring residues are satisfied
        for interactionlistname in ['bonds', 'angles', 'impropers', 'dihedrals', 'cmaps']:
            for thisresidue, otherresidue, left in [(self, other, True), (other, self, False)]:
                lis = getattr(thisresidue, interactionlistname)
                for interaction in lis:
                    refatoms = [a for a in interaction.atoms if a.startswith('+' if left else '-')]
                    for an in refatoms:
                        if not [a_ for a_ in otherresidue.atoms if a_.name == an[1:]]:
                            raise ValueError(f'Cannot link residues: atom {an} in a {interactionlistname[:-1]} interaction in {self.name} references an unexisting atom in {other.name}')
        r = copy.copy(self)
        r.next = copy.copy(other)
        r.next.prev = r
        if r.prev is r:
            r.prev = copy.copy(self)
            # do not set r.prev.next !
        return r

    def allParametersAssigned(self) -> bool:
        """Check if all interaction parameters are assigned"""
        if not all([a.parameters is not None for a in self.atoms]):
            return False
        for lis in [self.bonds, self.angles, self.dihedrals, self.impropers, self.cmaps]:
            if not all([(i.parameters is not None) and (len(i.parameters) >0) for i in lis]):
                return False
        return True

    def someParametersAssigned(self) -> bool:
        if not all([a.parameters is not None for a in self.atoms]):
            return False
        for lis in [self.atoms, self.bonds, self.angles, self.dihedrals, self.impropers, self.cmaps]:
            if not all([i.parameters is not None for i in lis]):
                return False
        return True

    def __repr__(self) -> str:
        return f'{self.name}'

    def removeDuplicateInteractions(self):
        """Normalize interaction lists such as each interaction happens only once"""
        for lis in [self.bonds, self.angles, self.dihedrals, self.impropers, self.cmaps]:
            for i, interaction in enumerate(lis[:]):
                if [interaction_ for interaction_ in lis[:i] if interaction_.has_same_atoms(interaction)]:
                    lis.remove(interaction)
