# coding=utf-8
"""Residue topology patch class"""

import copy
import logging
from typing import Union, List, Optional, Tuple

from .atom import Atom
from .atomadditionrule import AtomAdditionRule
from .interaction import Bond, Angle, Dihedral, Improper, Cmap
from .topology import ResidueTopology, LocationInChain
from ..prminfo import PRMInfo
from ..utils.utils import commonname

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ResidueTopologyPatch(ResidueTopology):
    """Residue topology patch

    A patch residue applies minor modifications (e.g. deprotonation, methylation, termination) on an
    existing residue. This is purely CHARMM-specific: GROMACS does not supports this mechanism. Therefore
    facilities for applying patches are implemented in this code to help create most common residues.

    The pdb2gmx tool of GROMACS is able to do some limited patching, using the termini database files (*.c.tdb and
    *.n.tdb). CHARMM patch residues which terminate the base residue should be (if possible) converted to entries in
    these. CHARMM patch residues support the following operations:
    1. adding new atoms to the base residue, using the internal coordinate database to place them in the structure
    2. deleting atoms from the base residue
    3. modifying the properties of an atom in the base residue
    4. adding or deleting bonds, angles, dihedrals, impropers, cmaps

    What CHARMM cannot do is to rename existing atoms: one has to delete the existing atom and add a new one with a
    different name.

    In contrast, GROMACS can:
    1. add new atoms to the base residue. A limited set of addition rules is available to get coordinates based on control atoms
    2. delet atoms from the base residue
    3. modify the properties of an atom (including its name!) in the base residue
    4. add (but cannot selectively delete) bonds, angles, dihedrals, impropers, cmaps etc.

    Notably, in GROMACS .tdb entries the same atom can be specified for addition and replacement. The best example is
    the COO- entry:

    [ COO- ]
    ; standard C-terminus
    [ replace ]
     C      C   CC  12.011   0.34
     O      OT1 OC  15.9994 -0.67
     OXT    OT2 OC  15.9994 -0.67
    [ add ]
    2   8   OT    C      CA     CB
      OC        15.999400  -0.6700  -1
    [ impropers ]
    C      CA     OT2    OT1

    `gmx pdb2gmx` executes [ delete ], [ replace ] and [ add ] instructions in the sequence as they appear in the entry
    (multiple clauses can appear after each other, even interleaved!). So while executing the above patch, the following
    happens:
    1. The properties of atom C are changed (the name "C" is kept)
    2. Atom "O" is renamed to "OT1" and its properties are also changed (atom type O -> OC and the charge too)
    3. Atom "OXT" sometimes exist in structures downloaded from the Protein DataBank. If it is there, it is renamed to
        "OT2" and properties are changed. If "OXT" does not exist, this line is ignored.
    4. Two Oxygen atoms are added to atom "C", with name "OT1" and "OT2", using addition rule #8 with control atoms
        "CA" and "CB". If any of "OT1" or "OT2" exists, it is not added again.
    5. An improper dihedral interaction is added on C, keeping atoms CA, OT2 and OT1 in plane.

    The above procedure ensures that whatever the initial conformation is, OT1 and OT2 will exist at the end (either by
    renaming existing atoms or by adding them anew). If OT1 and OT2 is already there (patch is attempted a second time),
    it should not change the structure: no atoms to be added or removed.

    Another procedure would be, more conformant to CHARMM's logic, to first remove all possible atoms (OXT, O, OT1, OT2)
    and specify addition rules for OT1 and OT2. Compared to the above described one, this has the slight drawback that
    already existing atom coordinates are lost, and get regenerated using the addition rule, which may or may not be
    correct. This, however, should easily be solved by a short energy minimization, which is routinely done anyway.

    When a terminus entry is constructed from a patch residue, first a [ delete ] clause is written, removing all atoms
    from the topology (and the structure) which are not needed, then [ add ] clauses add back the required atoms.
    Analysing all possible termini and adding [ delete ] clauses for atoms, this can make the terminal patching
    procedure more robust, i.e. able to handle already terminated residues, even replacing the existing terminus with
    a different one.

    :ivar delete: list of atoms or interactions to be deleted by this patch
    :type delete: list of Atom, Bond, Angle, Dihedral, Improper or Cmap instances
    :ivar atomadditions: information on atoms to be added
    :type atomadditions: list of (atom name list, atom addition rule) pairs
    :ivar atomreplacements: information on atoms where parameters should be adjusted (renaming does not work, see above)
    :type atomreplacements: list of Atom instances
    :ivar atomdeletions: list of atoms to be deleted
    :type atomdeletions: list of Atom instances
    """
    delete: List[Union[Atom, Bond, Angle, Dihedral, Improper, Cmap]]
    atomadditions: Optional[List[Tuple[List[str], AtomAdditionRule]]]
    atomreplacements: Optional[List[Atom]]
    atomdeletions: Optional[List[Atom]]
    isleftterminus: Optional[bool] = None

    def __init__(self, name: str, charge: float, moleculeclass: str, filename: str, lineno: Optional[int],
                 comment: str):
        super().__init__(name, charge, moleculeclass, filename, lineno, comment)
        self.delete = []
        self.atomadditions = []
        self.atomreplacements = []
        self.atomdeletions = []
        self.isleftterminus = None

    def __copy__(self) -> "ResidueTopologyPatch":
        other = ResidueTopologyPatch(self.name, self.charge, self.moleculeclass, self.filename, self.lineno,
                                     self.comment)
        other.delete = [copy.copy(d) for d in self.delete] if other.delete is not None else None
        other.atomadditions = [(newnames[:], copy.copy(rule)) for newnames, rule in
                               self.atomadditions] if self.atomadditions is not None else None
        other.atomreplacements = [copy.copy(ar) for ar in
                                  self.atomreplacements] if self.atomreplacements is not None else None
        other.atomdeletions = [copy.copy(d) for d in self.atomdeletions] if self.atomdeletions is not None else None
        other.isleftterminus = self.isleftterminus
        return other

    def convertToResidue(self) -> ResidueTopology:
        """Convert some patches (e.g ACE and CT3) to actual residue topologies.
        Not all patches support this, currently ACE and CT3 (NME) are implemented.
        """
        if self.name == 'ACE':
            newresn = 'ACE'
            renaming = [
                ('+N', None), ('CA', '+CA'), ('N', '+N'), ('C', '+C'), ('HN', '+HN'),
                ('CAY', 'CH3'), ('CY', 'C'), ('OY', 'O'), ('HY1', 'HH31'), ('HY2', 'HH32'), ('HY3', 'HH33')]
        elif self.name == 'CT3':
            newresn = 'NME'
            renaming = [
                ('-C', None), ('C', '-C'), ('O', '-O'), ('NT', 'N'), ('HNT', 'HN'),
                ('CAT', 'CH3'), ('HT1', 'HH31'), ('HT2', 'HH32'), ('HT3', 'HH33'), ('CA', 'CH3')]
        elif self.name == 'ACP':
            newresn = 'ACP'
            renaming = [
                ('+N', None), ('CA', '+CA'), ('N', '+N'), ('C', '+C'), ('CD', '+CD'),
                ('CAY', 'CH3'), ('CY', 'C'), ('OY', 'O'), ('HY1', 'HH31'), ('HY2', 'HH32'), ('HY3', 'HH33'),
            ]
        else:
            raise NotImplementedError('This patch residue cannot be converted to a residue.')

        r = ResidueTopology(newresn, self.charge, self.moleculeclass, self.filename, self.lineno, self.comment)
        r.atoms = [copy.copy(at) for at in self.atoms]

        assert not self.delete

        # rename and delete atoms
        for oldname, newname in renaming:
            try:
                atm = [a for a in r.atoms if a.name == oldname][0]
            except IndexError:
                if oldname.startswith('+') or oldname.startswith('-'):
                    # this is expected: references to the previous or next residue are not explicitly listed as atoms
                    continue
                logger.warning(f'Atom with name {oldname} cannot be found in patch residue {self.name}.')
                continue
            if (newname is None) or (newname[0] == '+') or (newname[0] == '-'):
                r.atoms.remove(atm)
            else:
                atm.name = newname

        for listname in ['bonds', 'angles', 'dihedrals', 'impropers', 'cmaps']:
            # first copy the list from the patch residue as is
            setattr(r, listname, [copy.copy(item) for item in getattr(self, listname)])
            for oldname, newname in renaming:
                if newname is None:
                    # all references to this atom should be removed. E.g. in the ACE PRES, we have the following line:
                    # CMAP CY N CA C N CA C +N
                    # if we translate to a residue, +N references the amide nitrogen in the second neighbour. GROMACS
                    # cannot reference that (++N does not work :-)), but the residue, e.g. ALA has a similar CMAP line:
                    # CMAP -C  N  CA  C   N  CA  C  +N
                    # this will take care of it, if CY is renamed to C
                    setattr(r, listname,
                            [copy.copy(item) for item in getattr(r, listname) if oldname not in item.atoms])
                else:
                    for item in getattr(r, listname):
                        item.atoms = tuple([newname if at == oldname else at for at in item.atoms])
        return r

    def apply(self, residue: "ResidueTopology", newname: str) -> "ResidueTopology":
        """Apply this patch onto another residue

        :param residue: base residue to apply this patch on
        :type residue: :class:`charmm2gmx.residuetopology.topology.ResidueTopology` instance
        :param newname: the name of the new residue
        :type newname: str
        :return: the patched residue
        :rtype: :class:`charmm2gmx.residuetopology.topology.ResidueTopology` instance
        """
        # make a copy so we don't distort the base residue
        logger.info(f'Applying patch {self.name} onto base residue {residue.name}')

        r = copy.copy(residue)
        r.name = newname

        # delete everything which need to be deleted

        # some patch residues are used for patching two residues at once, e.g. DISU.
        # Atom identifiers start with '1' or '2' in these.
        # We do not (yet?) support this kind of patching, only single residue patching. Therefore:
        # 1. remove the starting '1'-s
        # 2. remove all atoms and interaction specifications featuring atom names starting with '2'.
        for delendum in self.delete:
            if isinstance(delendum, Atom):
                if delendum.name.startswith('1'):
                    # patching only the first residue from the two
                    delendum.name = delendum.name[1:]
                elif delendum.name.startswith('2'):
                    # we do not patch the 2nd residue
                    continue
                atomtobedeleted = [a for a in r.atoms if a.name == delendum.name]
                if len(atomtobedeleted) != 1:
                    raise ValueError(
                        f'Exactly one atom with name {delendum.name} is needed for deletion, '
                        f'got {len(atomtobedeleted)}.')
                r.atoms.remove(atomtobedeleted[0])
                for listname in ['bonds', 'angles', 'dihedrals', 'impropers', 'cmaps']:
                    tobedeleted = [item for item in getattr(r, listname) if delendum.name in item.atoms]
                    for item in tobedeleted:
                        getattr(r, listname).remove(item)
            else:
                for typ, listname in [(Bond, 'bonds'), (Angle, 'angles'), (Dihedral, 'dihedrals'),
                                      (Improper, 'impropers'), (Cmap, 'cmaps')]:
                    if not isinstance(delendum, typ):
                        continue
                    logger.debug(
                        f'Deleting interaction from list {listname}. Atoms: {delendum.atoms}. List contains: {[str(interaction.atoms) for interaction in getattr(r, listname)]}')
                    if any([a_[0] == '2' for a_ in delendum.atoms]):
                        # this interaction involves the second patchable residue, ignore this.
                        continue
                    # rename atom names starting with '1'
                    delendum.atoms = tuple([(a[1:] if (a[0] == '1') else a) for a in delendum.atoms])
                    # find those interactions which should be deleted. There should only be one.
                    tobedeleted = [item for item in getattr(r, listname)
                                   if item.has_same_atoms(delendum)]
                    if len(tobedeleted) != 1:
                        # ensure that we got one and exactly one deletable interaction
                        raise ValueError(
                            f'Exactly one item in the list "{listname}" is needed for deletion, '
                            f'got {len(tobedeleted)}.')
                    # delete the interaction
                    getattr(r, listname).remove(tobedeleted[0])

        # trick for chainable residues (nucleotides, amino acids): If a patch makes the residue a terminus, it typically
        # adds another ligand to the joining atom. E.g. the O3' atom of each nucleotide is bonded to P of the next one
        # (bond between O3' and +P). The patch 3TER makes the nucleotide a 3'-terminal by adding bonding a hydrogen
        # (H3T) to O3'. In this case, we need to delete the O3' - +P bond in this residue.

        linkingbonds = [b for b in r.bonds if any([a[0] in '+-' for a in b.atoms])]
        for linkingbond in linkingbonds:
            for linkeratom in [a for a in linkingbond.atoms if a[0] not in '+-']:
                # check if the patch adds a new neighbour to this atom. If it does, remove the residue linker bond
                if [b for b in self.bonds
                    if (linkeratom in b.atoms)
                       and not [b_ for b_ in r.bonds if (b_.atoms == b.atoms) or (b_.atoms == b.atoms[::-1])]]:
                    r.bonds.remove(linkingbond)
                    logger.warning(
                        f'Implicitely (no DELETE BOND was found in the rtf) removed bond {linkingbond} from residue {r.name} while patching with {self.name}')

        # add or modify atoms and interactions

        # assigning charge groups:
        # 1. "update" type atoms link base residue charge groups to patch charge groups
        # 2. charge groups in the patch residue consisting entirely of new atoms get a new
        #    cgroup number

        newatoms = []
        cgroupmap = {}
        # First only do the modifications/atom updates: this is easier.
        maxcgroup = max([a.cgroup for a in r.atoms]) + 1
        for atom in self.atoms:
            if atom.name[0] == '2':
                continue
            try:
                # find the corresponding atom in the base residue
                baseatom = [at for at in r.atoms
                            if at.name == (atom.name[1:] if (atom.name[0] == "1") else atom.name)][0]
            except IndexError:
                # this atom does not occur in the base residue
                newatoms.append(copy.copy(atom))
                continue
            # update the base atom
            baseatom.atomtype = atom.atomtype
            baseatom.pcharge = atom.pcharge
            baseatom.cgroup = maxcgroup + atom.cgroup

        for atom in newatoms:
            if atom.name[0] == '1':
                atom.name = atom.name[1:]
            elif atom.name[0] == '2':
                continue
            if all([a.cgroup == 0 for a in self.atoms]):
                # try to assign the cgroup of one of its neighbours, but defer this until the bonds are done
                atom.cgroup = None
            else:
                atom.cgroup = maxcgroup + atom.cgroup
            r.atoms.append(atom)

        # add interactions
        for listname in ['bonds', 'angles', 'dihedrals', 'impropers', 'cmaps']:
            for item in getattr(self, listname):
                item = copy.copy(item)
                if any([at[0] == '2' for at in item.atoms]):
                    continue  # this interaction item involves atoms from the 2nd patchable residue
                item.atoms = tuple([(at[1:] if at[0] == '1' else at) for at in item.atoms])
                if not [it for it in getattr(r, listname) if it.has_same_atoms(item)]:
                    getattr(r, listname).append(item)

        # fix charge groups
        for atom in r.atoms:
            if atom.cgroup is not None:
                continue
            # otherwise find its bonded neighbours
            neighbours = r.bondedNeighbours(atom.name)
            cgroups = {r.getAtom(n).cgroup for n in neighbours if r.getAtom(n).cgroup is not None}
            if not cgroups:
                raise ValueError(f'Cannot fix cgroup for atom {atom.name}, with bonded neighbours {neighbours}')
            elif len(cgroups) > 1:
                raise ValueError(
                    f'Cannot fix cgroup for atom {atom.name}, neighbours {neighbours} have cgroups {cgroups} between them.')
            else:
                atom.cgroup = cgroups.pop()
                logger.debug(f'Fixed cgroup for atom {atom.name} to {atom.cgroup}')

        # check if charge groups were correctly assigned
        for cgroup in {a.cgroup for a in r.atoms}:
            charge = sum([a.pcharge for a in r.atoms if a.cgroup == cgroup])
            if abs(charge - round(charge)) > 0.0001:
                logger.warning(f'Non-integer charge {charge} in charge group {cgroup} of the patched residue {r.name}.')

        # renumber charge groups: start from 1
        cgmap = {}
        cg = 1
        for atom in r.atoms:
            assert atom.cgroup is not None
            if atom.cgroup not in cgmap:
                cgmap[atom.cgroup] = cg
                cg += 1
            atom.cgroup = cgmap[atom.cgroup]

        # sort atoms for contiguous charge group numbers
        r.atoms = [a for cg, i, a in sorted([(a.cgroup, i, a) for i, a in enumerate(r.atoms)])]
        logger.info(f'Applied patch {self.name} onto residue {residue.name} -> {r.name}')
        return r

    def convertToTerminus(self, baseresidue: ResidueTopology, isleft: bool, prminfo: PRMInfo):
        """Convert this patch residue to a GROMACS terminus (entry in the .tdb file).

        :param baseresidue: an example residue topology, to which the patch is applicable
        :type baseresidue: ResidueTopology instance
        :param isleft: if this patch makes the base residue a left-terminal (N or 5'), or a right-terminal (C or 3')
        :type isleft: bool
        :param prminfo: interaction parameters, used for topology perception
        :type prminfo: PRMInfo instance
        """
        if not baseresidue.isChainable():
            raise ValueError('Base residue should be a chainable residue.')

        if (isleft and baseresidue.isLeftTerminal()) or ((not isleft) and baseresidue.isRightTerminal()):
            raise ValueError(f'Cannot patch base residue from the {"left" if isleft else "right"}: '
                             f'it is already terminated on this side.')

        logger.debug(f'Converting patch residue {self.name} to a {"left" if isleft else "right"} terminus, '
                     f'using {baseresidue.name} as a base')

        # first look for atoms to be removed
        atomstobedeleted: List[Atom] = [d for d in self.delete if isinstance(d, Atom)]

        # atoms to be replaced: only replace atoms which exist and are different (either atom type or partial charge)
        # in the base residue. Note that matching is done according to name, therefore we cannot use this for renaming.
        atomstobereplaced: List[Atom] = [
            a for a in self.atoms
            if [at for at in baseresidue.atoms
                if (at.name == a.name) and not ((at.atomtype == a.atomtype) and (at.pcharge == a.pcharge))]]

        # atoms to be added, i.e. those which do not exist in the base residue. Atom addition is tricky, we will need
        # to get the addition rule too!
        atomstobeadded: List[Atom] = [a for a in self.atoms if a.name not in [at.name for at in baseresidue.atoms]]

        # now create a patched version of the residue, so we can analyze its topology
        patchedresidue = copy.copy(baseresidue)
        # delete atoms which need to be deleted
        patchedresidue.atoms = [a for a in patchedresidue.atoms if a.name not in [at.name for at in atomstobedeleted]]
        # delete interactions which reference any of the deleted atoms. Additionally, remove all interactions
        # referencing atoms in the previous (or the next) residue if this is a left (or a right) terminal patch
        for interactionlistname in ['bonds', 'angles', 'dihedrals', 'impropers', 'cmaps']:
            lis = getattr(patchedresidue, interactionlistname)
            for atom in atomstobedeleted:
                lis = [i for i in lis if atom.name not in i.atoms]
            if isleft:
                # this is a left terminal patch, remove all references to the previous residue
                lis = [i for i in lis if all([a[0] != '-' for a in i.atoms])]
            else:
                # this is a right terminal patch, remove all references to the next residue
                lis = [i for i in lis if all([a[0] != '+' for a in i.atoms])]
            setattr(patchedresidue, interactionlistname, lis)

        # now add new atoms to the patched residue
        logger.debug(f'{atomstobeadded=}')
        for atom in atomstobeadded:
            logger.debug(f'Adding atom {atom.name} to the patched residue.')
            patchedresidue.atoms.append(copy.copy(atom))

        # modify existing atoms in the patched residue
        for atom in atomstobereplaced:
            at = [a for a in patchedresidue.atoms if a.name == atom.name][0]
            at.pcharge = atom.pcharge
            at.atomtype = atom.atomtype

        # add bonds
        for bond in self.bonds:
            if not [b for b in patchedresidue.bonds if b.atoms == bond.atoms or b.atoms == b.atoms[::-1]]:
                patchedresidue.bonds.append(copy.copy(bond))

        # add impropers
        for improper in self.impropers:
            if not [i for i in patchedresidue.impropers if
                    i.atoms == improper.atoms or i.atoms == improper.atoms[::-1]]:
                patchedresidue.impropers.append(copy.copy(improper))

        # add cmaps
        for cmap in self.cmaps:
            if not [c for c in patchedresidue.cmaps if c.atoms == cmap.atoms]:
                patchedresidue.cmaps.append(copy.copy(cmap))

        # check if we have angles or dihedrals: if we do, it is unsupported
        if self.angles or self.dihedrals:
            raise NotImplementedError('Adding dihedrals or angles in a patch residue is not yet supported.')

        # set up chain linking
        if isleft:
            patchedresidue.prev = None
            patchedresidue.next = copy.copy(baseresidue)
            patchedresidue.next.prev = patchedresidue
            patchedresidue.locationinchain = LocationInChain.Left
        else:
            patchedresidue.next = None
            patchedresidue.prev = copy.copy(baseresidue)
            patchedresidue.prev.next = patchedresidue
            patchedresidue.locationinchain = LocationInChain.Right

        logger.debug(f'Atoms in patched residue: {[a.name for a in patchedresidue.atoms]}')
        # patching the residue is done. Now prepare for topology analysis
        patchedresidue.generateAngles()
        patchedresidue.generateDihedrals()
        patchedresidue.assignParameters(prminfo)

        # now find atoms to be added. This will be done similarly as hydrogen addition rules. As some new atoms are
        # bound to new atoms themselves, we find the addition rules with a flood-fill technique: first only those atoms
        # which are only bound to already existing atoms. In the next turn we can use these, too as bases to bind new
        # atoms to etc. Note that this can still fail sometimes, like below. If building from atom1, atom21 and atom22
        # can be added without
        #
        #               atom21 - atom31
        #             /             |
        # ...  atom1                |
        #             \             |
        #               atom22 - atom32
        atomadditions = []
        # list of the names of atoms which need the addition rules to be determined
        atomnamestobeadded = [at.name for at in atomstobeadded]
        for i in range(len(atomstobeadded)):
            # we could do this with a while(atomnamestobeadded) loop, but if the algorithm gets stuck, we run into an
            # infinite loop. In the worst case, atom addition rules of only one atom can be found in every iteration,
            # therefore the maximum number of iterations is the number of atoms to be added. Usually, if we are lucky,
            # we finish earlier. If we exceed this number, the algorithm is certainly stuck.
            if not atomnamestobeadded:
                # if we are out of atoms to find addition rule to: we are lucky, bail out.
                break
            # these atoms we can build on:
            atomsalreadythere = [a.name for a in patchedresidue.atoms if a.name not in atomnamestobeadded]
            # now try each candidate base atom and see if we can build any of the new atoms onto them
            for baseatom in atomsalreadythere:
                # let's see if this base atom has new atoms to be bound to it.
                newatoms = sorted([aat for aat in atomnamestobeadded if patchedresidue.isBonded(baseatom, aat)])
                if not newatoms:
                    # no atoms to be added to this base atom
                    continue
                # try to find the common name
                try:
                    cn = commonname(newatoms)
                except ValueError:
                    raise RuntimeError(
                        f'Cannot convert patch residue {self.name} to a terminus entry: '
                        f'there is no rule for attaching atoms {newatoms} to atom {baseatom}.')
                # now try all known atom addition rules. Note that addition rules may be found "exactly" or only
                # approximately. We always try all addition rules, and choose the "exact" match, if available. If not,
                # choose one of the "approximate" matches.
                results = []  # for each successfully found addition rule, an entry is added to this
                additionrules = None  # the final, chosen match
                for rule in [patchedresidue.try_additionrule9, patchedresidue.try_additionrule8,
                             patchedresidue.try_additionrule1, patchedresidue.try_additionrule2,
                             patchedresidue.try_additionrule3, patchedresidue.try_additionrule4,
                             patchedresidue.try_additionrule5, patchedresidue.try_additionrule6,
                             patchedresidue.try_additionrule7, patchedresidue.try_additionrule10,
                             patchedresidue.try_additionrule11]:
                    # try the current addition rule
                    result = rule(newatoms, baseatom)
                    if not result:
                        # this rule is not applicable for this base atom and new atoms
                        continue
                    elif any([not r.hname for r in result]):
                        # this may occur if the common name is not found. This check might be superfluous, as we test
                        # this possibility eariler above.
                        raise ValueError(f'Cannot find common name for atoms: {newatoms} on base atom {baseatom}')
                    elif any([r.inaccurate for r in result]):
                        # some inaccurate results have been found
                        results.append(result)
                    else:
                        # this addition rule matches exactly: do not try anything else, break the for loop
                        additionrules = result
                        break
                else:
                    # we reach here if no exactly matching addition rule has been found.
                    if results:
                        # if there is at least one approximate one, choose the first one.
                        additionrules = results[0]
                    else:
                        # there are neither exact, nor approximate matches: give up, we cannot convert this patch
                        # residue to a terminus entry.
                        raise RuntimeError(
                            f'Cannot apply addition rule for new atoms {newatoms} '
                            f'attached to base atom {baseatom} in residue {patchedresidue.name}, with patch {self.name}.')
                # special case: COOH addition rule also handles a hydrogen, which is not attached to this base atom.
#                if (len(additionrules) == 1) and (additionrules[0].rule == 9):
#                    # COOH, remove the H as well
#                    for oxygen in newatoms[:]:
#                        hydrogens = [bn for bn in patchedresidue.getAtom(oxygen).bondedneighbours if bn != baseatom]
#                        if hydrogens:
#                            newatoms.append(hydrogens[0])
                assert sum([a.count for a in additionrules]) == len(newatoms)
                if len(additionrules) > 1:
                    for ar, n in zip(additionrules, newatoms):
                        atomadditions.append(([n], ar))
                else:
                    atomadditions.append((newatoms, additionrules[0]))
                atomnamestobeadded = [a for a in atomnamestobeadded if a not in newatoms]
                break
            else:
                # this happens when no new additions could be found but there are still some atom names to be added
                assert not atomnamestobeadded

        self.atomadditions = atomadditions
        self.atomreplacements = atomstobereplaced
        self.atomdeletions = atomstobedeleted
        for atom in self.atoms:
            atom.parameters = [at for at in prminfo.atomtypes if at.name == atom.atomtype][0]
        #        logger.debug(str(self))
        self.isleftterminus = isleft
        logger.debug(f'{self.name} converted to terminus successfully.')
        logger.debug(f'{self.moleculeclass=}, {self.isleftterminus=}')

    def __str__(self) -> str:
        """GROMACS .c.tdb or .n.ntd representation of this patch."""
        s = f"[ {self.name} ]\n"
        s += f"; {self.comment}\n"
        additional_replacements = []
        if self.atomdeletions:
            s += f'[ delete ]\n'
            for atom in self.atomdeletions:
                s += f' {atom.name}\n'
        if self.atomreplacements:
            s += f"[ replace ]\n"
            for at in self.atomreplacements:
                s += f'{at.name:<6s} {at.atomtype:<8s} {at.parameters.mass:10.6f} {at.pcharge:8.4f}\n'
        if self.atomadditions:
            s += f"[ add ]\n"
            for newatoms, ar in self.atomadditions:
                logger.debug(f"{repr(ar)}, {repr(type(ar))}")
                s += f'{ar.count:<3d} {ar.rule:<3d} {ar.hname:<6s}' + " ".join(
                    [f'{ca:<6s}' for ca in ar.controlatoms]) + '\n'
                atoms = [self.getAtom(na) for na in newatoms]
#                if not (all([a.pcharge == atoms[0].pcharge for a in atoms])
#                        and all([a.atomtype == atoms[0].atomtype for a in atoms])):
#                    # anoter replacement block will be needed

                s += f'  {atoms[0].atomtype:<8s} {atoms[0].parameters.mass:10.6f} {atoms[0].pcharge:8.4f}  -1\n'

                # Formerly, an ADDitional [ replace ] entry was ADDed for each ADDed atom (sorry for the puns ;-)), to
                # allow replacing similarly named atoms from different termini. However, due to a bug in pdb2gmx, which
                # won't be fixed in the near (or far) future, we cannot operate this way. On the other hand, because
                # in some cases added atoms of different types / partial charges are lumped together in a single atom
                # addition rule. E.g. CHARMM protonated C-terminus, PCTE:  OT1 (type OB, charge -0.55) and OT2 (type
                # OH1, charge -0.61) are constructed together with addition rule #9, with the type and chage of OT1.
                # Here we need another replace rule to correct this.
                for a in atoms[1:]:
                    if (a.pcharge != atoms[0].pcharge) or (a.parameters.mass != atoms[0].parameters.mass) or \
                            (a.atomtype != atoms[0].atomtype):
                        additional_replacements.append(a)
        if additional_replacements:
            s += f"[ replace ]\n"
            for at in additional_replacements:
                s += f'{at.name:<6s} {at.atomtype:<8s} {at.parameters.mass:10.6f} {at.pcharge:8.4f}\n'
        if self.impropers:
            s += f'[ impropers ]\n'
            for i in self.impropers:
                s += ' '.join([f'{a:<6s}' for a in i.atoms]) + '\n'
        return s
