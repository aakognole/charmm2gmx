# coding=utf-8
"""Atom class for representing atoms of a residue topology"""
from typing import Optional, Set

from ..prminfo.atomtype import AtomType


class Atom:
    """Representation of an atom in a residue

    :ivar str name: atom name
    :ivar str atomtype: atom type
    :ivar str pcharge: partial charge assigned to this atom
    :ivar int cgroup: charge group index
    :ivar str comment: long description
    :ivar bondedneighbours: list of the names of bonded neighbours: constructed under the hood from bond information:
        if present, it speeds up residue topology analysis
    :type bondedneighbours: None or a list of strings
    :ivar parameters: atom type parameters assigned from the force field
    :type parameters: :class:`AtomType` instance
    """
    name: str
    atomtype: str
    pcharge: float
    cgroup: int
    comment: str
    bondedneighbours: Optional[Set[str]] = None
    parameters: Optional[AtomType] = None

    def __init__(self, name: str, atomtype: str, pcharge: float, cgroup: int, comment: str):
        """Create a new Atom instance

        :param name: atom name
        :type name: str
        :param atomtype: atom type
        :type atomtype: str
        :param pcharge: partial charge
        :type pcharge: float
        :param cgroup: charge group index
        :type cgroup: int
        :param comment: long description
        :type comment: str
        """
        self.name = name
        self.atomtype = atomtype
        self.pcharge = float(pcharge)
        self.cgroup = int(cgroup)
        self.comment = comment
        self.bondedneighbours = None
        self.parameters = None

    def __copy__(self) -> "Atom":
        """Make an independent copy

        :return: the copy of this atom
        :rtype: Atom instance
        """
        return Atom(self.name, self.atomtype, self.pcharge, self.cgroup, self.comment)

    def __eq__(self, other: "Atom") -> bool:
        """Two atoms are considered equal if their names, atom types and partial charges are the same.

        Charge groups are not checked, nor comments.
        """
        if not isinstance(other, type(self)):
            return False
        return ((self.name == other.name) and (self.atomtype == other.atomtype) and (
                self.pcharge == other.pcharge))  # and (self.cgroup == other.cgroup))

    def isEquivalent(self, other: "Atom") -> bool:
        if (self.parameters is None) or (other.parameters is None):
            raise ValueError('Atom equivalency cannot be tested without assigning atom types')
        return self.parameters.isEquivalent(other.parameters)

    def __ne__(self, other: "Atom") -> bool:
        return not (self == other)

    def __str__(self) -> str:
        """String representation, suitable for directly writing into a GROMACS .rtp file."""
        return f'    {self.name:>5s} {self.atomtype:>8s} {self.pcharge:>7.4f} {self.cgroup:>3d}'

    def __repr__(self) -> str:
        return f'Atom({self.name}, {self.atomtype}, {self.pcharge:.4f}, {self.cgroup:d})'
