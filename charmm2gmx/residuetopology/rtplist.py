# coding=utf-8
import itertools
import logging
from typing import List, Iterator, Optional, Set, Tuple, Union

from .patch import ResidueTopologyPatch
from .topology import ResidueTopology
from ..utils.clashpolicy import ParameterClashPolicy

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class RTPList:
    """Residue topology list"""
    _residues: List[ResidueTopology]
    clashpolicy: ParameterClashPolicy = ParameterClashPolicy.ERROR
    version: Tuple[Optional[int], Optional[int]]

    def __init__(self):
        self._residues = []
        self.version = (None, None)

    def __getitem__(self, item: str):
        try:
            return [it for it in self._residues if it.name == item][0]
        except IndexError:
            raise ValueError(f'Item {item} not in RTPList.')

    def __iadd__(self, other: "RTPList"):
        for r in other:
            self.append(r)

    def __contains__(self, item: Union[str, "ResidueTopology"]) -> bool:
        if isinstance(item, str):
            return bool([r for r in self._residues if r.name == item])
        elif isinstance(item, ResidueTopology):
            return item in self._residues
        else:
            raise TypeError(item)

    def keys(self) -> List[str]:
        return [item.name for item in self._residues]

    def append(self, rtp: ResidueTopology):
        self._residues.append(rtp)

    def validate(self):
        self.sort()
        for dup in self.iter_duplicates():
            raise RuntimeError(f'Validation failed: duplicate residue name {dup[0].name}')

    def iter_duplicates(self) -> Iterator[List[ResidueTopology]]:
        self.sort()
        i = 0
        while i < len(self._residues):
            dup = [self._residues[i]]
            for j in range(i+1, len(self._residues)):
                if self._residues[j].name == dup[-1].name:
                    dup.append(self._residues[j])
                else:
                    i = j
                    break
            else:
                i = len(self._residues)
            if (len(dup) > 1) and (
                    (any([isinstance(d, ResidueTopologyPatch) for d in dup])
                     and not all([isinstance(d, ResidueTopologyPatch) for d in dup])) or
                    all([isinstance(d, ResidueTopology) for d in dup])):
                yield dup

    def __iter__(self) -> Iterator[ResidueTopology]:
        for r in self._residues:
            yield r

    def __len__(self) -> int:
        return len(self._residues)

    def residues(self, filename: Optional[str] = None) -> Iterator[ResidueTopology]:
        for r in self._residues:
            if isinstance(r, ResidueTopologyPatch):
                continue
            if (filename is None) or (r.filename == filename):
                yield r

    def patches(self, filename: Optional[str] = None) -> Iterator[ResidueTopologyPatch]:
        for p in self._residues:
            if not isinstance(p, ResidueTopologyPatch):
                continue
            if (filename is None) or (p.filename == filename):
                yield p

    def __bool__(self) -> bool:
        return bool(self._residues)

    def remove(self, rtp: ResidueTopology):
        if rtp in self._residues:
            assert isinstance(rtp, ResidueTopology)
            self._residues = [r for r in self._residues if not (r is rtp)]
            #assert rtp not in self._residues
        else:
            raise ValueError('Cannot remove nonexistent item.')

    def extend(self, other: "RTPList", overrideresidues: List[str]) -> "RTPList":
        """Extend this list with another"""
        self._residues.extend(other._residues)
        self.sort()
        return self

    def sort(self):
        self._residues = sorted(self._residues, key=lambda r: r.name)

    def filenames(self) -> List[str]:
        return list({r.filename for r in self})

    def moleculeclasses(self) -> Set[str]:
        return {r.moleculeclass for r in self}

    def getresidue(self, name: str) -> ResidueTopology:
        try:
            return [r for r in self._residues if (r.name == name) and not isinstance(r, ResidueTopologyPatch)][0]
        except IndexError:
            raise KeyError(name)

    def getpatch(self, name: str) -> ResidueTopologyPatch:
        try:
            return [r for r in self._residues if (r.name == name) and isinstance(r, ResidueTopologyPatch)][0]
        except IndexError:
            raise KeyError(name)

    def onlyMoleculeClass(self, molclass: str) -> "RTPList":
        obj = RTPList()
        obj.version = self.version
        obj._residues = [r for r in self._residues if r.moleculeclass == molclass]
        return obj

