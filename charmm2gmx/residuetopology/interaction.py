# coding=utf-8
"""Interaction entries in residue topologies"""

import logging
import math
from typing import Tuple, Final, Sequence, List, Optional

from ..prminfo import ComparableInteractionType, ImproperType, DihedralType, BondType, CmapType, AngleType
from ..utils.interactiontype import InteractionType

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class TopologyInteraction:
    """Representation of an interaction (bond, angle etc.) in a residue topology

    :ivar atoms: names of atoms participating in this interaction
    :type atoms: tuple of string
    :cvar natoms: number of atoms required
    :type natoms: int
    :ivar parameters: None or a list of interaction parameters
    :type parameters: None or a list of :class:`ComparableInteractionType` instances
    :cvar interactiontype: the type of the interaction
    :type interactiontype: member of the enum `InteractionType`
    """
    natoms: int = 0
    atoms: Tuple[str, ...]
    parameters: Optional[List[ComparableInteractionType]] = None
    interactiontype: InteractionType

    def __init__(self, atoms: Sequence[str], comment: str):
        """Create a new topology interaction

        :param atoms: atom names
        :type atoms: any sequence of strings
        :param comment: long description
        :type comment: str
        """
        if len(atoms) != self.natoms:
            raise ValueError(f'Exactly {self.natoms} atoms are required, got {len(atoms)}.')
        self.atoms = tuple(atoms)
        self.comment = comment
        self.parameters = None

    def __copy__(self) -> "TopologyInteraction":
        return type(self)(self.atoms, self.comment)

    def has_same_atoms(self, other: "TopologyInteraction") -> bool:
        """Check if two interactions are based on the same atoms. Note that reversed order also
        counts, e.g. for dihedrals: A, B, C, D is the same as D, C, B, A.
        """
        if not isinstance(other, type(self)):
            raise TypeError(f'Cannot compare type {type(other).__name__} to {type(self).__name__}')
        return (self.atoms == other.atoms) or (self.atoms == other.atoms[::-1])

    def __eq__(self, other: "TopologyInteraction") -> bool:
        if not isinstance(other, type(self)):
            return False
        return self.has_same_atoms(other)

    def __ne__(self, other: "TopologyInteraction"):
        return not (self == other)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({", ".join(self.atoms)})'

    def isEquivalent(self, other: "TopologyInteraction") -> bool:
        raise NotImplementedError


class Bond(TopologyInteraction):
    """Representation of a chemical bond between two atoms in the residue"""
    natoms: Final[int] = 2
    atoms: Tuple[str, str]
    order: int
    parameters: Optional[List[BondType]] = None
    interactiontype: Final[InteractionType] = InteractionType.Bond

    def __init__(self, atoms: Tuple[str, str], order: int, comment: str):
        super().__init__(atoms, comment)
        self.order = order

    def __copy__(self) -> "Bond":
        return Bond(self.atoms, self.order, self.comment)

    def isEquivalent(self, other: "TopologyInteraction") -> bool:
        if (self.parameters is None) or (other.parameters is None):
            raise ValueError('Cannot check bond equivalence if parameters not assigned.')
        return


class Angle(TopologyInteraction):
    """Bond angle vibration interaction"""
    natoms: Final[int] = 3
    atoms: Tuple[str, str, str]
    parameters: Optional[List[AngleType]] = None
    interactiontype: Final[InteractionType] = InteractionType.Angle

    def is120(self, tolerance: float = 5) -> bool:
        if self.parameters is None:
            raise ValueError('Parameters not yet assigned to this angle.')
        if len(self.parameters) > 1:
            raise ValueError('Multiple angle parameters not supported.')
        return abs(self.parameters[0].Theta0 - 120) < tolerance

    def istetrahedral(self, tolerance: float = 5.0) -> bool:
        if self.parameters is None:
            raise ValueError('Parameters not yet assigned to this angle.')
        if len(self.parameters) > 1:
            raise ValueError('Multiple angle parameters not supported.')
        return abs(self.parameters[0].Theta0 - 109.5) < tolerance

    def is180(self, tolerance: float=2.0) -> bool:
        if self.parameters is None:
            raise ValueError('Parameters not yet assigned to this angle.')
        if len(self.parameters) > 1:
            raise ValueError('Multiple angle parameters not supported.')
        return abs(self.parameters[0].Theta0 - 180.0) < tolerance


class Dihedral(TopologyInteraction):
    """Torsion angle potential"""
    natoms: Final[int] = 4
    parameters: Optional[List[DihedralType]] = None
    atoms: Tuple[str, str, str, str]
    interactiontype: Final[InteractionType] = InteractionType.Dihedral

    def _ensureMinima(self, positions: Sequence[float], notpositions: Optional[Sequence[float]],
                      globalminima: bool = False, difftol: float = 1e-10, postol=1e-6):
        """Check if the dihedral potential has minima at the given positions.

        The mathematical formula of the CHARMM dihedral potential is:

        $$ V_\text{dih} = \sum_{n} K_n ( 1 + \cos (n \phi - \phi_{0,n})) \text{,}$$
        where $n$ is the multiplicity (typically 1, 2, 3, 4, 6), $K_n$ is the force constant for the given multiplicity,
        $\phi$ is the dihedral angle and $\phi_{0,n}$ is the starting phase. For symmetry reasons, the starting phase
        can only be $0$ or $\pi$.

        Extrema can only occur at integer multiples of $\pi/n$ for each $n$. Because of the symmetry of the
        cosine function, if $\phi$ is an extremum, $2\pi - \phi$ is also a same kind of extremum.

        This method checks if the dihedral angle values in `positions` are indeed minima. `positions` are implicitly
        extended to their $2\pi-\phi$ pairs.

        If `notpositions` is None, it is required that no other minima exist, only those in the extended `positions`.

        If `notpositions` is a sequence, it is required that the potential has no minima at those positions.

        If `globalminima` is True, it is required that positiosn in `positions` are global minima.
        """

        # first some sanity checks:
        if self.parameters is None:
            raise ValueError('Parameters not yet assigned!')
        if any([p.functype != 9 for p in self.parameters]):
            raise NotImplementedError(
                'Only cosine dihedrals with different multiplicities (GROMACS func type == 9) are implemented')

        # possible minima:
        possibleminima = set()
        for mult, delta in {(p.n, p.delta) for p in self.parameters}:
            possibleminima |= {(math.pi / mult * i - delta * math.pi / 180. / mult) % 2 * math.pi for i in
                               range(2 * mult)}

        minima = {
            phi: sum([p.Kchi * (1 + math.cos(p.n * phi - p.delta * math.pi / 180.0)) for p in self.parameters])
            for phi in possibleminima
            # only if the 1st derivative is zero:
            if (abs(sum([p.n * p.Kchi * math.sin(p.n * phi - p.delta * math.pi / 180.0) for p in self.parameters]))
                < difftol)
               # and the 2nd derivative is positive:
               and (-sum(
                [p.n ** 2 * p.Kchi * math.cos(p.n * phi - p.delta * math.pi / 180.0) for p in self.parameters]) > 0)
        }

        logger.debug(
            f'Dihedral potential minima found at: '
            f'{", ".join(["pi * {:.4f}:  {:.4f}".format(k / math.pi, v) for k, v in minima.items()])}')
        # minima maps the positions of minima to the corresponding potential value.
        # Note that the minima are all in [0; 2*pi)

        if not minima:
            logger.debug('Empty dihedral')
            # can happen with zero dihedrals. Now every position is a minimum.
            if notpositions is None:
                # there _are_ other positions: every position is a minimum!
                return False
            if notpositions:
                # there are forbidden minima: every position is a minimum, so the forbidden minimums are there.
                return False
            # global minimum checking is not needed
            return True
        globalminvalue = min(minima.values())

        # ensure that `positions` is sane: map it to the [0; 2pi) interval, add 2pi-phi symmetry pairs and remove
        # duplicates
        positions = {p % (2 * math.pi) for p in
                     positions}  # map values to the [0; 2pi) interval, also remove duplicates
        positions |= {(2 * math.pi - p) % (2 * math.pi) for p in positions}
        #        logger.debug(str(positions))
        # now `positions` is a set, extended with the 2pi-phi symmetry pairs.
        logger.debug(f'Minima requested at: {", ".join(["pi * {:.4f}".format(k / math.pi) for k in positions])}')
        if notpositions is None:
            logger.debug('No other minima are tolerated.')
        else:
            logger.debug(f'Forbidden minima: {", ".join(["pi * {:.4f}".format(k / math.pi) for k in notpositions])}')
        if any([p for p in positions if not [p_ for p_ in minima if abs(p - p_) < postol]]):
            # one or more of the requested minima is not a minimum
            return False
        if (notpositions is None) and [p for p in minima if not [p_ for p_ in positions if abs(p - p_) < postol]]:
            # other minima exist apart from the requested
            return False
        if (notpositions is not None) and any(
                [p for p in minima if [p_ for p_ in notpositions if abs(p - p_) < postol]]):
            # one more forbidden minima found
            return False
        if globalminima and any([minima[p] > globalminvalue for p in minima]):
            # one or more of the requested minima is not a global minimum
            return False
        return True

    def isCis(self, strict: bool = False) -> bool:
        """Cheeck if this dihedral prefers the cis conformation

        Cis conformation corresponds to 0° dihedral angle

        :param bool strict: if True, ensure that no other minima exist
        :return: True if the dihedral allows the cis conformation
        :rtype: bool
        """
        return self._ensureMinima([0.0], None if strict else [], False)

    def isTrans(self, strict: bool = False) -> bool:
        """Check if this dihedral prefers the trans conformation

        Trans conformation corresponds to 180° dihedral angle

        :param bool strict: if True, ensure that no other minima exist
        :return: True if the dihedral allows the trans conformation
        :rtype: bool
        """
        return self._ensureMinima([math.pi], None if strict else [], False)

    def isGauche(self, strict: bool = False, transtoo: bool = False) -> bool:
        """Check if this dihedral prefers gauche conformation

        Gauche conformation corresponds to +/- 60° dihedral angle

        :param bool strict: if True, ensure that no other minima exist
        :param bool transtoo: if True, allow the trans conformation
        :return: True if the dihedral allows gauche conformations
        :rtype: bool
        """
        return self._ensureMinima([math.pi / 3, -math.pi / 3] + ([math.pi] if transtoo else []), None if strict else [],
                                  False)

    def isEclipsed(self, strict: bool = False, cistoo: bool = False) -> bool:
        """Check if this dihedral prefers eclipsed conformation

        Eclipsed conformation corresponds to +/- 120° dihedral angle

        :param bool strict: if True, ensure that no other minima exist
        :param bool cistoo: if True, allow the cis conformation
        :return: True if the dihedral allows eclipsed conformations
        :rtype: bool
        """
        return self._ensureMinima([2 * math.pi / 3, -2 * math.pi / 3] + ([0.0] if cistoo else []),
                                  None if strict else [], False)

    def isPlanar(self) -> bool:
        """Check if the interaction parameters force this dihedral to be planar: minima can only be at 0° or 180° but
        nowhere else.
        """
        return self._ensureMinima([0.0, math.pi], None, False)


class Improper(TopologyInteraction):
    """Improper dihedral involving four atoms, typically used to enforce planarity"""
    natoms: Final[int] = 4
    atoms: Tuple[str, str, str, str]
    parameters: Optional[List[ImproperType]] = None
    interactiontype: Final[InteractionType] = InteractionType.Improper

    def isPlanar(self) -> bool:
        """Check if the interaction parameters force this dihedral to be planar."""
        if self.parameters is None:
            raise ValueError('Parameters not yet assigned!')
        return all([p.isPlanar() for p in self.parameters])


class Cmap(TopologyInteraction):
    """Dihedral-dihedral correlation term"""
    natoms: Final[int] = 5
    parameters: Optional[List[CmapType]] = None
    atoms: Tuple[str, str, str, str, str]
    interactiontype: Final[InteractionType] = InteractionType.Cmap


class ICSpec:
    """Internal coordinate specification from the CHARMM force field, involving four atoms."""
    atoms: Tuple[str, str, str, str]
    b12: float
    theta123: float
    chi: float
    theta234: float
    b34: float
    comment: str

    def __init__(self, atoms: Tuple[str, str, str, str], b12: float, theta123: float, chi: float, theta234: float,
                 b34: float, comment: str):
        self.atoms = atoms
        self.b12 = float(b12)
        self.theta123 = float(theta123)
        self.chi = chi
        self.theta234 = theta234
        self.b34 = b34
        self.comment = comment

    def __copy__(self) -> "ICSpec":
        return ICSpec(self.atoms, self.b12, self.theta123, self.chi, self.theta234, self.b34, self.comment)
