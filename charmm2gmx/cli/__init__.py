# coding=utf-8
"""Command-line interface"""

import atexit
import cProfile
import logging
import sys
from typing import Optional

import click

from . import charmm2gmx, findmissingpars, genhdb, ffcompare, classifyresidues, rtp2itp


def exitHandler(profiler: cProfile.Profile, filename: str):
    profiler.disable()
    profiler.dump_stats(filename)


@click.group(name='charmm2gmx', context_settings={'help_option_names': ['-h', '--help']})
@click.option('--debug', help='Debug mode, with lots of output', is_flag=True, default=False)
@click.option('--profile', help='Profile using cProfile and write the output to the specified file',
              type=click.Path(dir_okay=False, writable=True), default=None)
@click.version_option()
def cli(debug: bool, profile: Optional[str] = None) -> None:
    """CHARMM to GROMACS force field converter and other utilities"""
    try:
        import colorlog

        handler = colorlog.StreamHandler(sys.stdout)
        handler.setFormatter(colorlog.ColoredFormatter(
            '%(log_color)s%(message)s'
        ))
        logging.root.addHandler(handler)
        del colorlog
        handler.setLevel(logging.DEBUG if debug else logging.INFO)
    except ImportError:
        logging.basicConfig(stream=sys.stdout)
        logging.root.setLevel(logging.DEBUG if debug else logging.INFO)
    if profile is not None:
        p = cProfile.Profile()
        atexit.register(exitHandler, p, profile)
        p.enable()


cli.add_command(charmm2gmx.convert)
cli.add_command(findmissingpars.findmissingpars)
cli.add_command(genhdb.genhdb)
cli.add_command(ffcompare.compare)
cli.add_command(classifyresidues.classifyresidues)
cli.add_command(rtp2itp.rtp2itp)
