#!/usr/bin/env python3
# coding=utf-8

import logging

import click

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from ..forcefield.gmxparse import GMXFFParser


@click.command()
@click.argument('ffdir1')
@click.argument('ffdir2')
def compare(ffdir1: str, ffdir2: str) -> int:
    """Compare two force fields"""

    logger.info('Loading 1st FF')
    ff1 = GMXFFParser(ffdir1, [])
    logger.info('Loading 2nd FF')
    ff2 = GMXFFParser(ffdir2, [])

    logger.info('Comparing the force fields')
    ff1.compare(ff2)
    return 0
