# coding=utf-8
"""Classify residues according to their molecule class and chainabilities
"""

import logging

import click

from ..forcefield.gmxparse import GMXFFParser

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@click.command()
@click.argument('ffdir')
def classifyresidues(ffdir: str) -> int:
    """Classify and list residues"""

    logger.info(f'Loading force field from directory {ffdir}')
    ff = GMXFFParser(ffdir, [])
    for molclass in sorted({r.moleculeclass for r in ff.rtp}):
        logger.info(f'Molecule class: {molclass}:')
        nonchain = []
        left = []
        mid = []
        right = []
        for residue in [r for r in ff.rtp if r.moleculeclass == molclass]:
            if not residue.isChainable():
                nonchain.append(residue)
            elif residue.isLeftTerminal():
                left.append(residue)
            elif residue.isRightTerminal():
                right.append(residue)
            else:
                mid.append(residue)
        logger.info('  Non-chainable residues: ' + ', '.join(sorted([r.name for r in nonchain])))
        logger.info('  Left termini: ' + ', '.join(sorted([r.name for r in left])))
        logger.info('  Right termini: ' + ', '.join(sorted([r.name for r in right])))
        logger.info('  Mid-chain: ' + ', '.join(sorted([r.name for r in mid])))

    return 0
