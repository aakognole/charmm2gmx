# coding=utf-8
"""Find missing interaction parameters for residue topologies"""
import gzip
import itertools
import inspect
import logging
import multiprocessing
import pickle
import time
import traceback
from typing import Optional, Tuple, Dict, List

import click

from ..forcefield.gmxparse import GMXFFParser
from ..prminfo import PRMInfo
from ..prminfo.interactiontypes import InteractionType, ImproperType
from ..residuetopology.interaction import TopologyInteraction
from ..residuetopology.topology import LocationInChain, ResidueTopology
from ..utils import GMXFileHeader

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

MISSINGPARAMETERSTYPE = Dict[str, Dict[Tuple[str, ...], Dict[str, List[Tuple[str, ...]]]]]


def findmissing(residue: ResidueTopology, prminfo: PRMInfo) -> List[
    Tuple[TopologyInteraction, Tuple[str, ...], ResidueTopology]]:
    residue.generateAngles()
    residue.generateDihedrals()
    if (residue.prev is not None) and (residue.prev is not residue):
        residue.prev.generateAngles()
        residue.prev.generateDihedrals()
    elif (residue.next is not None) and (residue.next is not residue):
        residue.next.generateAngles()
        residue.next.generateDihedrals()
    missing = []
    for interaction, atomtypes in residue.assignParameters(prminfo, exception_on_missing=False):
        missing.append((interaction, atomtypes, residue))
    return missing


@click.command()
@click.argument('ffdir')
@click.option('--cpo', 'output',
              help='Pickle file to output the results, for avoiding the time-consuming search next time.', default=None,
              type=click.Path(dir_okay=False, writable=True))
@click.option('--cpi', 'inputfile',
              help='Pickle file to read the results from, instead of doing the time-consuming search',
              default=None, type=click.Path(dir_okay=False))
@click.option('-n', '--force-nter', help='Force the named residue to be N-terminal (or 5\'-terminal)', default=None,
              type=click.STRING, multiple=True)
@click.option('-c', '--force-cter', help='Force the named residue to be C-terminal (or 3\'-terminal)', default=None,
              type=click.STRING, multiple=True)
@click.option('-m', '--force-midchain', help='Force the named residue to be mid-chain', default=None, type=click.STRING,
              multiple=True)
@click.option('-s', '--force-standalone', help='Force the named residue to be stand-alone', default=None,
              type=click.STRING, multiple=True)
@click.option('-d', '--dihedraltypes-file', help='Write zero dihedraltypes to this file', default=None,
              type=click.Path(dir_okay=False, writable=True))
@click.option('-r', '--residue', 'onlyresidues', help='Check only this residue (can be given multiple times)',
              default=None, type=click.STRING, multiple=True)
@click.option('--ncpus', help='Number of threads to use (default: all available)', default=multiprocessing.cpu_count(),
              type=click.IntRange(1, multiprocessing.cpu_count()))
def findmissingpars(ffdir: str, output: Optional[str], inputfile: Optional[str], force_nter: Tuple[str, ...] = (),
                    force_cter: Tuple[str, ...] = (), force_midchain: Tuple[str, ...] = (),
                    force_standalone: Tuple[str, ...] = (),
                    dihedraltypes_file: Optional[str] = None,
                    onlyresidues: Optional[Tuple[str, ...]] = None,
                    ncpus: int = multiprocessing.cpu_count()) -> int:
    """Find missing interaction parameters in a GROMACS FF"""
    logger.info('Findmissingpars')
    logger.info('Called with the following arguments:')
    for arg, value in inspect.getargvalues(inspect.currentframe()).locals.items():
        logger.info(f'  {arg}: {value}')
    missingparameters: List[Tuple[TopologyInteraction, Tuple[str, ...], ResidueTopology]] = []
    logger.info(f'Loading force field from directory {ffdir}')
    ff = GMXFFParser(ffdir, [])
    if (onlyresidues is not None) and len(onlyresidues):
        for r in list(ff.rtp.residues()):
            if r.name not in onlyresidues:
                ff.rtp.remove(r)
    logger.info(f'Force field loaded. Looking for missing parameters...')
    for lis, loc in [(force_nter, LocationInChain.Left), (force_cter, LocationInChain.Right),
                     (force_midchain, LocationInChain.Mid), (force_standalone, LocationInChain.NotChainable)]:
        for name in lis:
            for r in [r_ for r_ in ff.rtp if r_.name == name]:
                r.locationinchain = loc
    if inputfile is None:
        pool = multiprocessing.Pool(ncpus)
        tasks = []
        logger.info('Submitting tasks for background processing...')
        for molclass in sorted({r.moleculeclass for r in ff.rtp}):
            logger.info(f'Molecule class {molclass}')
            rtp = ff.rtp.onlyMoleculeClass(molclass)
            mid = [r for r in rtp if r.isChainable() and not (r.isRightTerminal() or r.isLeftTerminal())]
            for residue in rtp:
                if residue.isLeftTerminal() or residue.isRightTerminal():
                    logger.info(
                        f'{residue.name} is a terminus, applying all possible mid-chain residues below:')
                    for j, neighbour in enumerate(mid, start=1):
                        try:
                            r = residue + neighbour if residue.isLeftTerminal() else neighbour + residue
                        except ValueError as ve:
                            logger.warning(ve.args[0])
                            continue
                        if residue.isLeftTerminal():
                            r.prev = None
                        elif residue.isRightTerminal():
                            r = r.next
                            r.next = None
                        r.name = f'{residue.name} + ({neighbour.name})' \
                            if residue.isLeftTerminal() else f'({neighbour.name}) + {residue.name}'
                        tasks.append((r.name, pool.apply_async(findmissing, args=(r, ff.prminfo))))
                else:
                    tasks.append((residue.name, pool.apply_async(findmissing, args=(residue, ff.prminfo))))
        ntasks = len(tasks)
        logger.info(f'Waiting for {ntasks} to finish in the background.')
        i = 0
        t0 = time.monotonic()
        try:
            while len(tasks):
                ready = [(rname, t) for rname, t in tasks if t.ready()]
                for rname, task in ready:
                    i += 1
                    try:
                        missingparameters_ = task.get()
                    except Exception as exc:
                        logger.error(f'{rname} ({i}/{ntasks}): {traceback.format_exc()}')
                        raise
                    else:
                        if missingparameters_:
                            logger.warning(
                                f'{rname} ({i}/{ntasks}): {len(missingparameters_)} interactions found with missing parameters')
                            missingparameters.extend(missingparameters_)
                        else:
                            logger.info(f'{rname} ({i}/{ntasks}): OK')
                    tasks = [(rn, t) for rn, t in tasks if t is not task]
                time.sleep(0.5)
        except KeyboardInterrupt:
            pool.close()
            pool.terminate()
            pool.join()
            raise
        logger.info(f'All tasks finished in {time.monotonic() - t0:.2f} seconds')
        pool.close()
        pool.join()
        logger.info('Closed pool')
    else:
        logger.info(f'Loading checkpoint file {inputfile}')
        if inputfile.endswith('.gz'):
            with gzip.open(inputfile, 'rb') as f:
                missingparameters = pickle.load(f)
        else:
            with open(inputfile, 'rb') as f:
                missingparameters = pickle.load(f)

    if output is not None:
        logger.info(f'Writing checkpoint file {output}')
        if output.endswith('.gz'):
            with gzip.open(output, 'wb') as f:
                pickle.dump(missingparameters, f)
        else:
            with open(output, 'wb') as f:
                pickle.dump(missingparameters, f)

    if dihedraltypes_file is not None:
        logger.info(f'Will write missing dihedraltypes to file {dihedraltypes_file}.')
        fdihtype = open(dihedraltypes_file, 'wt')
        fdihtype.write(GMXFileHeader())
    else:
        logger.info('Not writing missing dihedraltypes file.')
        fdihtype = None
    try:
        for interactiontype in {mp[0].interactiontype for mp in missingparameters}:
            mps = [mp for mp in missingparameters if mp[0].interactiontype == interactiontype]
            logger.info(f'{interactiontype.name}s with missing parameters:')
            if (interactiontype == InteractionType.Dihedral) and (fdihtype is not None):
                fdihtype.write(
                    "\n\n; Dummy zero dihedral types for those which are missing in CHARMM\n"
                    
                    "; Note: in some residue topologies in CHARMM, especially in the CGenFF part "
                    "(e.g. C3, BUTY) some dihedrals have no\n"
                    "; corresponding interaction parameters based on the types of the participating atoms "
                    "(e.g. C1-C2-C3-C4 in BUTY).\n"
                    "; CHARMM simply ignores these dihedrals, giving you messages like \n"
                    '; "CHECKDH> dihedral :    1    3    4    7 will NOT be generated". Grompp however gives you an'
                    ' error.\n'
                    "; As a workaround, this file contains [ dihedraltypes ] with zero force constant for these "
                    "dihedral types.\n"
                    ";\n"
                    "[ dihedraltypes ]\n"
                    ";      i        j        k        l  func         phi0         kphi  mult\n")
            elif (interactiontype == InteractionType.Improper) and (fdihtype is not None):
                fdihtype.write(
                    "\n\n; When looking for improper dihedral parameters, GROMACS does an A-B-C-D -> D-C-B-A reversal "
                    "automatically. \n"
                    "; However, when the phase offset parameter of the improper dihedral is 0, the corresponding "
                    "potential term is an \n"
                    "; even function of the dihedral angle, thus the permutation A-B-C-D -> A-C-B-D gives the same "
                    "potential energy. \n"
                    "; The CHARMM engine seemingly does this automatically, e.g. in the residue SAH (in CGenFF), but "
                    "GROMACS does\n"
                    "; not support this. In order to satisfy grompp, we generate the corresponding dihedral types "
                    "here.\n"
                    "[ dihedraltypes ]\n"
                    ";      i        j        k        l  func         phi0         kphi  mult\n")
            while mps:
                atomtypes_reference = mps[0][1]
                mps_with_the_same_atomtypes = [mps[0]]
                for mp in mps[1:]:
                    interaction, atomtypes, residue = mp
                    assert isinstance(interaction, TopologyInteraction)
                    if tuple([residue.getAtom(a).atomtype for a in interaction.atoms]) in [atomtypes_reference,
                                                                                           atomtypes_reference[::-1]]:
                        mps_with_the_same_atomtypes.append(mp)

                resolved = False
                # check if dummy dihedral entries needed
                if (interactiontype == InteractionType.Dihedral) and (fdihtype is not None):
                    # check if this dihedral is partly or fully linear
                    # only need to check the 1st residue: all atom types are the same
                    interaction, atomtypes, residue = mps_with_the_same_atomtypes[0]
                    angle1 = residue.getAngle(interaction.atoms[1:])
                    angle2 = residue.getAngle(interaction.atoms[:-1])
                    if not angle1.parameters:
                        # some parameters are not assigned
                        resolved = False
                    elif not angle2.parameters:
                        # some parameters are not assigned
                        resolved = False
                    else:
                        if residue.getAngle(interaction.atoms[1:]).is180() or residue.getAngle(
                                interaction.atoms[:-1]).is180():
                            # a dummy dihedraltype with 0 force constant needs to be generated to satisfy grompp
                            fdihtype.write('; The following dihedraltype is for:\n')
                            for ia, at, res in mps_with_the_same_atomtypes:
                                fdihtype.write(f';     Residue {res.name}, atoms {", ".join(ia.atoms)}\n')
                            fdihtype.write(
                                f'{atomtypes[0]:>8s} {atomtypes[1]:>8s} {atomtypes[2]:>8s} {atomtypes[3]:>8s} '
                                f'{9:>5d} {0.0:>12.6f} {0.0:>12.6f} {1:>5d} ;\n')
                            logger.info(
                                f'  Resolved missing proper dihedral {" ".join(atomtypes)} with a dummy dihedraltype.')
                            resolved = True

                # check if dummy improper entries needed
                if (interactiontype == InteractionType.Improper) and (fdihtype is not None):
                    interaction, atomtypes, residue = mps_with_the_same_atomtypes[0]
                    if len(interaction.parameters) > 1:
                        raise RuntimeError(
                            f'Multiple improper dihedral entries defined for atoms {interaction.atoms} in residue '
                            f'{residue.name}: this is not supported!')
                    for lax in [False, True]:
                        if lax:
                            # If no compatible improper dihedral type could be found by ABCD -> ACBD permutation,
                            # attempt a more lax approach. If the improper dihedral is to enforce planarity, like::
                            #
                            #                  l
                            #                  i
                            #                j   k
                            #
                            # then in principle, any permutation of j, k and l can be used to enforce the planarity.
                            permutations = [
                                (atomtypes[0],) + perm for perm in itertools.permutations(atomtypes[1:])
                            ]
                        else:
                            # first the simple ABCD -> ACBD permutation is tried
                            permutations = [atomtypes, (atomtypes[0], atomtypes[2], atomtypes[1], atomtypes[3])]
                        dihs = [impr for impr in ff.prminfo.impropertypes
                                if any([impr.atomtypes_match(ats) for ats in permutations]) and impr.psi0 == 0.0]

                        specifics = [d for d in dihs if not d.isWildcard()]
                        if len(specifics) > 1:
                            # More than one candidate
                            raise RuntimeError(
                                f'More than one permutation candidates for missing improper dihedral'
                                f' entry having atom types {atomtypes}')
                        elif specifics:
                            # Exactly one candidate
                            assert len(specifics) == 1
                            candidate = specifics[0]
                        elif len(dihs) > 2:
                            # no specifics, only wildcards, but more than one
                            raise RuntimeError(
                                f'More than one permutation candidates (wildcards) for missing improper'
                                f'dihedral entry having atom types {atomtypes}')
                        elif dihs:
                            assert len(dihs) == 1
                            candidate = dihs[0]
                        elif not lax:
                            # the lax method will be tried next
                            continue
                        else:
                            # the lax method has been tried, with no success
                            break
                        # this line is only reached, if a candidate is found.
                        fdihtype.write('; The following dihedraltype is for:\n')
                        for ia, at, res in mps_with_the_same_atomtypes:
                            fdihtype.write(f';     Residue {res.name}, atoms {", ".join(ia.atoms)}\n')
                        assert isinstance(candidate, ImproperType)
                        fdihtype.write(
                            f'{atomtypes[0]:>8s} {atomtypes[1]:>8s} {atomtypes[2]:>8s} {atomtypes[3]:>8s} '
                            f'{candidate.functype:>5d} {candidate.psi0:>12.6f} {candidate.Kpsi:>12.6f}  ')
                        if lax:
                            fdihtype.write(f'; Permutation of atoms j, k, l in {", ".join(candidate.atomtypes)}\n')
                        else:
                            fdihtype.write(f'; ijkl -> ikjl permutation of {", ".join(candidate.atomtypes)}\n')
                        # resolved, do not report this
                        logger.info(
                            f'  Resolved missing improper dihedral {" ".join(atomtypes)} '
                            f'with {"ABCD->ACBD" if not lax else "lax"} permutation.')
                        resolved = True
                        break  # do not try with the lax approach if the strict one succeeded.

                if not resolved:
                    logger.warning('  Cannot resolve missing parameter with atom types: ' + " ".join(mps_with_the_same_atomtypes[0][1]))
                for mp in mps_with_the_same_atomtypes:
                    # list affected residues
                    if not resolved:
                        logger.warning(f'    Residue {mp[2].name}: atoms ' + ", ".join(mp[0].atoms))
                    mps.remove(mp)
    finally:
        if fdihtype is not None:
            fdihtype.close()

    return 0
