#!/usr/bin/env python3
# coding: utf-8
"""Convert a CHARMM force field to GROMACS format"""

import logging
import sys
import pickle
import gzip
from typing import Optional

import click

from ..forcefield.charmmparse import CHARMMFFParser

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


@click.command()
@click.argument('inputfile')
@click.option('--cpo', 'checkpointout',
              help='Pickle file to output the results, for avoiding the time-consuming parse.', default=None,
              type=click.Path(dir_okay=False, writable=True))
@click.option('--cpi', 'checkpointin',
              help='Pickle file to read the results from, instead of doing the time-consuming parse',
              default=None, type=click.Path(dir_okay=False))
def convert(inputfile: str, checkpointout: Optional[str] = None, checkpointin: Optional[str] = None) -> int:
    """Convert a toppar representation of a CHARMM force field into a GROMACS-readable formatna"""
    if checkpointin is None:
        try:
            charmm = CHARMMFFParser(inputfile)
        except:
            # error message should have been written to log
            raise
    else:
        if checkpointin.endswith('.gz'):
            with gzip.open(checkpointin, 'rb') as f:
                charmm = pickle.load(f)
        else:
            with open(checkpointin, 'rb') as f:
                charmm = pickle.load(f)
    if checkpointout is not None:
        if checkpointout.endswith('.gz'):
            with gzip.open(checkpointout, 'wb') as f:
                pickle.dump(charmm, f)
        else:
            with open(checkpointout, 'wb') as f:
                pickle.dump(charmm, f)
    logger.info('CHARMM FF data loaded, starting conversion.')
    outputdir = charmm.outputdir
    # write parameter files
    charmm.writeGMXFormat(outputdir, charmm.charmmversion, charmm.cgenffversion)
    logger.info('Statistics:\n===========')
    logger.info('Residues:\n---------')
    for molclass in sorted(charmm.rtp.moleculeclasses()):
        logger.info(f'  [[ {molclass} ]]')
        standalone = [r for r in charmm.rtp.residues() if (not r.isChainable()) and (r.moleculeclass == molclass)]
        leftter = [r for r in charmm.rtp.residues() if r.isLeftTerminal() and (r.moleculeclass == molclass)]
        rightter = [r for r in charmm.rtp.residues() if r.isRightTerminal() and (r.moleculeclass == molclass)]
        mid = [r for r in charmm.rtp.residues() if
               r.isChainable() and (not r.isLeftTerminal()) and (not r.isRightTerminal()) and (
                           r.moleculeclass == molclass)]
        patch = [r for r in charmm.rtp.patches() if r.moleculeclass == molclass and not([r_ for r_ in charmm.rtp.residues() if (r.name == r_.name) and (r_.moleculeclass == molclass)])]
        for label, lis in [
            ('stand-alone', standalone),
            ('left terminal', leftter),
            ('right terminal', rightter),
            ('mid-chain', mid),
            ('patch', patch),
        ]:
            logger.info(f'    {len(lis)} {label} residues: ' + ' '.join(sorted([r.name for r in lis])))
    return 0
