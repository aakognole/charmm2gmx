# coding=utf-8
"""Convert residue topology entries to itp files with a single [ moleculeclass ] entry inside"""
import logging
from typing import Tuple, Optional

import click

from ..forcefield.gmxparse import GMXFFParser

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@click.command('rtp2itp')
@click.argument('ffdir', type=click.Path(exists=True, file_okay=False), required=True)
@click.option('--resname', '-r', multiple=True, help='Residue name', type=click.STRING, required=True)
@click.option('--output', '-o', 'filename', default=None,
              help='Output file name. If not given, write <residuename>.itp files.',
              type=click.Path(dir_okay=False, writable=True))
@click.option('--posresmacro', '-p', 'posresmacro', default='POSRES',
              help='Preprocessor macro name for position restraints', type=click.STRING)
@click.option('--posresforce', '-f', 'posresforce', default=1000.0, help='Force constant for position restraints',
              type=click.FLOAT)
def rtp2itp(ffdir: str, resname: Tuple[str],  filename: Optional[str], posresmacro: str, posresforce: float):
    """Convert a residue topology to a [ moleculetype ] in an .itp file.

    Multiple residue names can be given by repeating the '-r' option.
    """
    ff = GMXFFParser(ffdir, [])
    f = None
    try:
        if filename is not None:
            logger.debug(f'Opening file {filename}')
            f = open(filename, 'wt')
        for resn in resname:
            logger.info(f'Converting residue {resn} to a [ moleculetype ] entry')
            try:
                r = ff.rtp[resn]
            except KeyError:
                logger.error(f'Residue {resn} does not exist.')
                continue
            try:
                r.generateAngles()
            except ValueError:
                logger.error(f'Cannot generate angles for residue {resn}.')
                continue
            try:
                r.generateDihedrals()
            except ValueError:
                logger.error(f'Cannot generate dihedrals for residue {resn}.')
                continue
            try:
                r.assignAtomTypes(ff.prminfo)
            except ValueError as ve:
                logger.error(f'Cannot assign atom types in residue {resn}: {str(ve)}.')
                continue
            try:
                r.assignParameters(ff.prminfo)
            except ValueError as ve:
                logger.error(f'Cannot assign interaction parameters in residue {resn}: {str(ve)}.')
                continue
            if filename is None:
                f = open(resn.lower() + '.itp', 'wt')
            f.write(r.toMoleculeType(None, posresmacro, posresforce))
            if filename is None:
                f.close()
                logger.info(f'File {f.name} written.')
                f = None
    finally:
        if f is not None:
            f.close()
            logger.info(f'File {f.name} written.')
            f = None
