# coding: utf-8
"""Genhdb subcommand"""
import gzip
import logging
import multiprocessing
import os
import pickle
import time
from typing import Optional, Tuple
import enum

import click

from ..forcefield.gmxparse import GMXFFParser
from ..prminfo import PRMInfo
from ..residuetopology.topology import LocationInChain, ResidueTopology
from ..utils import GMXFileHeader

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class FailType(enum.Enum):
    AngleGeneration = 'angle generation'
    DihedralGeneration = 'dihedral generation'
    AtomTypeAssignment = 'atom type assignment'
    ParameterAssignment = 'interaction parameter assignment'
    HDBGeneration = 'hydrogen addition rule assignment'
    NoFail = 'OK'


def assignHDBEntries(baseresiduename: str, residue: ResidueTopology, prminfo: PRMInfo, partial: bool=False):
    try:
        residue.generateAngles()
    except Exception as exc:
        return baseresiduename, residue.moleculeclass, [], residue, FailType.AngleGeneration, str(exc)
    try:
        residue.generateDihedrals()
    except Exception as exc:
        return baseresiduename, residue.moleculeclass, [], residue, FailType.DihedralGeneration, str(exc)
    try:
        residue.assignAtomTypes(prminfo)
    except Exception as exc:
        return baseresiduename, residue.moleculeclass, [], residue, FailType.AtomTypeAssignment, str(exc)
    try:
        residue.assignParameters(prminfo)
    except Exception as exc:
        return baseresiduename, residue.moleculeclass, [], residue, FailType.ParameterAssignment, str(exc)
    try:
        hdb = residue.generateHDB(keep_going=partial)
    except Exception as exc:
        return baseresiduename, residue.moleculeclass, [], residue, FailType.HDBGeneration, str(exc)
    return baseresiduename, residue.moleculeclass, hdb, residue, None, ''


@click.command()
@click.option('--cpo', 'output',
              help='Pickle file to output the results, for avoiding the time-consuming search next time.', default=None,
              type=click.Path(dir_okay=False, writable=True))
@click.option('--cpi', 'inputfile',
              help='Pickle file to read the results from, instead of doing the time-consuming search',
              default=None, type=click.Path(dir_okay=False))
@click.option('-t', '--molclass', 'onlymolclass', help='Molecule class (if not given, all is treated)', default=(),
              multiple=True,
              type=click.STRING)
@click.option('-n', '--force-nter', help='Force the named residue to be N-terminal (or 5\'-terminal)', default=(),
              type=click.STRING, multiple=True)
@click.option('-c', '--force-cter', help='Force the named residue to be C-terminal (or 3\'-terminal)', default=(),
              type=click.STRING, multiple=True)
@click.option('-m', '--force-midchain', help='Force the named residue to be mid-chain', default=(), type=click.STRING,
              multiple=True)
@click.option('-s', '--force-standalone', help='Force the named residue to be stand-alone', default=(),
              type=click.STRING, multiple=True)
@click.option('--ncpus', help='Number of threads to use (default: all available)', default=multiprocessing.cpu_count(),
              type=click.IntRange(1, multiprocessing.cpu_count()))
@click.option('--ignore', help="Ignore the named residue (can be specified multiple times)", default=(),
              type=click.STRING, multiple=True)
@click.option('--partial/--complete',
              help="If specified, partial assignments (i.e. when no addition rule can be assigned to some hydrogens)"
                   " are also accepted.", default=False)
@click.argument('ffdir')
def genhdb(ffdir: str, output: Optional[str], inputfile: Optional[str], onlymolclass: Optional[Tuple[str, ...]] = (),
           force_nter: Tuple[str, ...] = (),
           force_cter: Tuple[str, ...] = (), force_midchain: Tuple[str, ...] = (),
           force_standalone: Tuple[str, ...] = (), ncpus: int = multiprocessing.cpu_count(),
           ignore: Tuple[str, ...] = (), partial: bool=False) -> int:
    """Generate hydrogen addition database (.hdb) files from residue topologies and interaction parameters."""
    logger.info(f'Loading force field from directory {ffdir}')
    ff = GMXFFParser(ffdir, [])
    logger.info(f'Force field loaded.')
    logger.debug(f'{partial=}')
    # assign chain location tags to residues
    for lis, loc in [
        (force_nter, LocationInChain.Left),
        (force_cter, LocationInChain.Right),
        (force_midchain, LocationInChain.Mid),
        (force_standalone, LocationInChain.NotChainable)
    ]:
        for name in lis:
            for r in [r_ for r_ in ff.rtp if r_.name == name]:
                r.locationinchain = loc
    # assigning hydrogen addition rules takes much time. Results can be cached. If no cache file is given,
    # we have to do it.
    if inputfile is None:
        # do the work in parallel. Enqueue several tasks, then wait for their completion
        pool = multiprocessing.Pool(ncpus)
        tasks = []
        logger.info('Enqueueing tasks for parallel processing in the background.')
        for molclass in sorted({r.moleculeclass for r in ff.rtp}):
            if onlymolclass and (molclass not in onlymolclass):
                continue
            logger.info(f'Molecule class {molclass}')
            rtp = ff.rtp.onlyMoleculeClass(molclass)
            # r.isChainable() will force a check of the chain-location tags if they have not been assigned yet.
            mid = [r for r in rtp if r.isChainable() and not (r.isRightTerminal() or r.isLeftTerminal())]
            for residue in rtp:
                if residue.name in ignore:
                    continue
                if residue.isLeftTerminal() or residue.isRightTerminal():
                    # by default, each mid-chain residue is linked with a copy of itself from both sides, so references
                    # to atoms in neighbouring residues (e.g. +N or -C in amino acids) can be resolved. Chain-end
                    # residues (e.g. NME or ACE) cannot be linked with themselves, so all other mid-chain residues are
                    # checked.
                    logger.info(
                        f'{residue.name} is a chain-end residue, trying to link with all possible mid-chain residues')
                    for j, neighbour in enumerate(mid, start=1):
                        try:
                            r = residue + neighbour if residue.isLeftTerminal() else neighbour + residue
                        except ValueError as ve:
                            # happens if linking fails, e.g. unresolvable reference atoms
                            logger.warning(ve.args[0])
                            continue
                        # fix up neighbour links
                        if residue.isLeftTerminal():
                            r.prev = None
                        elif residue.isRightTerminal():
                            r = r.next
                            r.next = None
                        r.name = f'{residue.name} + ({neighbour.name})' \
                            if residue.isLeftTerminal() else f'({neighbour.name}) + {residue.name}'
                        tasks.append(
                            (residue.name, r.name,
                             pool.apply_async(assignHDBEntries, args=(residue.name, r, ff.prminfo, partial))))
                else:
                    # mid-chain or stand-alone residues can be submitted as is.
                    tasks.append((residue.name, residue.name,
                                  pool.apply_async(
                                      assignHDBEntries, args=(residue.name, residue, ff.prminfo, partial))))
        # all tasks have been submitted, wait for them to complete.
        ntasks = len(tasks)
        logger.info(f'Waiting for {ntasks} to finish in the background.')
        results = []
        i = 0
        t0 = time.monotonic()
        failed_residues = set()
        try:
            while len(tasks):
                ready = [(brname, rname, t) for brname, rname, t in tasks if t.ready()]
                for basername, rname, task in ready:
                    i += 1
                    baseresiduename, molclass, hdb, residue, failtype, errormessage = task.get()
                    # result is:
                    #   - base residue name: the name of the target residue. For mid-chain or standalone residues, this
                    #        is the residue name. For chain-end residues, this is the original residue name
                    #   - molclass: molecule class for easier sorting later on
                    #   - hdb: list of AtomAdditionRule instances, preferably one for each heavy atom with hydrogens
                    #        attached.
                    #   - residue: the residue instance used. For linked chain-end residues, the name is updated, e.g.
                    #        "ACE + (ALA)" or "(ALA) + NME" to be able to distinguish between various linking trials
                    #   - failtype: None if everything is OK, else a member of the FailType enum
                    #   - errormessage: a string, empty if success.
                    results.append((baseresiduename, molclass, hdb, residue, failtype, errormessage))
                    if failtype is not None:
                        logger.error(f'{rname} ({i}/{ntasks}): {errormessage}')
                    else:
                        logger.info(f'{rname} ({i}/{ntasks}): OK')
                    tasks = [(brn, rn, t) for (brn, rn, t) in tasks if t is not task]
                time.sleep(0.5)
        except KeyboardInterrupt:
            pool.close()
            pool.terminate()
            pool.join()
            raise
        logger.info(f'All tasks finished in {time.monotonic() - t0:.2f} seconds')
        pool.close()
        pool.join()
        logger.info('Closed pool')
    else:
        # If the input file is defined, load the output from that.
        if inputfile.endswith('.gz'):
            with gzip.open(inputfile, 'rb') as f:
                failed_residues, results = pickle.load(f)
        else:
            with open(inputfile, 'rb') as f:
                failed_residues, results = pickle.load(f)


    if output is not None:
        if output.endswith('.gz'):
            with gzip.open(output, 'wb') as f:
                pickle.dump((failed_residues, results), f)
        else:
            with open(output, 'wb') as f:
                pickle.dump((failed_residues, results), f)

    # group the results by molecule class, a separate .hdb file will be written for each
    molclasses = {mc for br, mc, hdb, r, ft, em in results}
    for mc in molclasses:
        results_mc = [(br, hdb, residue, ft, em) for br, mc_, hdb, residue, ft, em in results if mc_ == mc]
        with open(os.path.join(ffdir, mc + '.hdb'), 'wt') as f:
            f.write('; Hydrogen addition database generated by charmm2gmx\n')
            f.write(GMXFileHeader())

            # group by base residue name: results for the same chain-end residue with different neighbours are treated
            # together.
            for baseresiduename in sorted({br for br, hdb, residue, ft, em in results_mc}):
                # get all results for the current molecule class and base residue
                results_mc_br_all = [r for r in results_mc if (r[0] == baseresiduename)]
                # the next is a narrowed list of the successful ones
                results_mc_br_ok = [(hdb, residue) for br, hdb, residue, ft, em in results_mc_br_all if ft is None]
                if not results_mc_br_ok:
                    logger.error(f'Could not assign hydrogen addition rules to residue {baseresiduename}')
                    failed_residues.add(baseresiduename)
                    continue
                elif len({len(hdb) for hdb, residue in results_mc_br_ok}) > 1:
                    logger.error(
                        f'Different number of assigned hydrogen addition rules for residue {baseresiduename}:\n' + '\n'.join(
                            [f'{residue.name}: {len(hdb)}' for hdb, residue in results_mc_br_ok]))
                    continue
                else:
                    for hdb, residue in results_mc_br_ok[1:]:
                        for hdbrule1, hdbrule2 in zip(sorted(hdb), sorted(results_mc_br_ok[0][0])):
                            if hdbrule1 != hdbrule2:
                                raise ValueError(
                                    f'Hydrogen addition rules for residue {residue.name} and {results_mc_br_ok[0][1].name} do not match:\n'
                                    f'Rule for {residue.name}:\n' + str(hdbrule1) + f'\nRule for {results_mc_br_ok[0][1].name}:\n' + str(hdbrule2)
                                )
                hdb, residue = results_mc_br_ok[0]
                if not hdb:
                    # this happens when there are no hydrogens
                    logger.warning(
                        f'No hydrogen addition rules in residue {baseresiduename}, '
                        f'probably because there are no hydrogens at all')
                    continue
                f.write(f'{baseresiduename:<10s} {len(hdb)}\n')
                for hdbentry in hdb:
                    f.write(str(hdbentry) + '\n')
                if residue.name != baseresiduename:
                    logger.info(
                        f'Found HDB rules for residue {baseresiduename} using linked configuration {residue.name}')
                failed_residues.discard(baseresiduename)
    if failed_residues:
        logger.error(f'Residues failed:')
        failtypes = {ft for br, mc, hdb, residue, ft, em in results if ft is not None}
        for failtype in sorted(failtypes, key = lambda ft:ft.value):
            assert isinstance(failtype, FailType)
            logger.error(f'  Failed at {failtype.value}:')
            resnames = sorted({br for br, mc, hdb, residue, ft, em in results if (ft == failtype) and (br in failed_residues)})
            logger.error('    '+', '.join(resnames))
    return 0
