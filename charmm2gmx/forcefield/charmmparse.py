# coding=utf-8
"""Parser for CHARMM force field files"""
import copy
import itertools
import logging
import os
import re
import time
from typing import TextIO, List, Tuple, Optional

from .arnrule import AtomRenameRule
from .forcefield import ForceField
from .inputfileinfo import InputFileInfo, InputFileType
from ..prminfo import PRMInfo, AtomType, BondType, AngleType, DihedralType, ImproperType, CmapType, NonbondedType, \
    NbfixType
from ..residuetopology import ResidueTopology, Atom, Bond, Angle, Dihedral, Improper, Cmap, ICSpec, \
    ResidueTopologyPatch, RTPList
from ..utils import splitline
from ..utils.citations import CitationList
from ..utils.clashpolicy import ParameterClashPolicy
from ..utils.regex import RE_FLOAT, RE_ATOMID

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class CHARMMFFParser(ForceField):
    """CHARMM force field file parser
    """
    toppardir: str
    outputdir: str
    bibtexfile: str
    charmmversion: Optional[str] = None
    cgenffversion: Optional[str] = None

    def __init__(self, configfile: str):
        super().__init__()
        self.parseConfigFile(configfile)
        self.parse()

    def parseConfigFile(self, filename: str):
        """Parse a config file.

        :param filename: the config file name
        :type filename: str
        """
        with open(filename, 'rt') as f:
            sectionname = None
            for lineno, line in enumerate(f, start=1):
                try:
                    stripped, comment, remainder = splitline(line, ';')
                    if not stripped:
                        continue
                    elif stripped[0] == '[' and stripped[-1] == ']':
                        sectionname = stripped[1:-1].strip()
                    else:
                        try:
                            left, right = [x.strip() for x in stripped.split(':', 1)]
                        except ValueError:
                            logger.critical(f'Error on line {lineno} in file {filename}: {line.strip()}')
                            raise
                        if sectionname is None and left == 'toppardir':
                            self.toppardir = right
                        elif sectionname is None and left == 'outputdir':
                            self.outputdir = right
                        elif sectionname is None and left == 'bibtexfile':
                            self.bibtexfile = right
                        elif sectionname is None and left == 'charmmversion':
                            self.charmmversion = right
                        elif sectionname is None and left == 'cgenffversion':
                            self.cgenffversion = right
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'(?P<prefix>[+-]?)'
                                    fr'(?P<filetype>str|prm|rtf)\s*:\s*'
                                    fr'(?P<filename>[-\d\w._/\\:$]+)', stripped)):
                            self.inputfiles.append(
                                InputFileInfo(
                                    os.path.join(self.toppardir, m["filename"]),
                                    InputFileType(m["filetype"]),
                                    ParameterClashPolicy(m['prefix']), sectionname))
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'patch\s*:\s*'
                                    fr'(?P<resn1>{RE_ATOMID})\s*\+\s*(?P<resn2>{RE_ATOMID})\s*=\s*(?P<resn3>{RE_ATOMID})',
                                    stripped)):
                            self.inputfiles[-1].patching.append((m['resn1'], m['resn2'], m['resn3']))
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'discardresidues\s*:\s*'
                                    fr'(?P<resnames>({RE_ATOMID}\s*)+)',
                                    stripped)):
                            self.inputfiles[-1].discardresidues.extend(m['resnames'].split())
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'overrideresidues\s*:\s*'
                                    fr'(?P<resnames>({RE_ATOMID}\s*)+)',
                                    stripped)):
                            self.inputfiles[-1].overrideresidues.extend(m['resnames'].split())
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'renameresidues\s*:\s*'
                                    fr'(?P<resnames>({RE_ATOMID}\s*->\s*{RE_ATOMID}\s*)+)',
                                    stripped)):
                            renamelist = [
                                (m1['origname'], m1['newname'])
                                for m1 in re.finditer(
                                    fr'(?P<origname>{RE_ATOMID})\s*->\s*(?P<newname>{RE_ATOMID})', m['resnames'])]
                            self.inputfiles[-1].renameresidues.extend(renamelist)
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'copyresidues\s*:\s*'
                                    fr'(?P<resnames>({RE_ATOMID}\s*->\s*{RE_ATOMID}\s*)+)',
                                    stripped)):
                            renamelist = [
                                (m1['origname'], m1['newname'])
                                for m1 in re.finditer(
                                    fr'(?P<origname>{RE_ATOMID})\s*->\s*(?P<newname>{RE_ATOMID})', m['resnames'])]
                            self.inputfiles[-1].copyresidues.extend(renamelist)
                        elif sectionname is not None and (
                                m := re.match(fr'renameatomtype\s*:\s*'
                                              fr'(?P<atomtypes>({RE_ATOMID}\s*->\s*{RE_ATOMID}\s*)+)', stripped)):
                            renamelist = [
                                (m1['origname'], m1['newname'])
                                for m1 in re.finditer(
                                    fr'(?P<origname>{RE_ATOMID})\s*->\s*(?P<newname>{RE_ATOMID})', m['atomtypes'])]
                            self.inputfiles[-1].atomtyperenamelist.extend(renamelist)
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'cite\s*(\[(?P<description>.*)\])?\s*:\s*(?P<citekey>\w+)\s*',
                                    stripped)):
                            self.inputfiles[-1].citekeys.append((m['citekey'], m['description']))
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'cite\s*(\[(?P<description>.*)\])?\s*:\s*(?P<citekey>".*")\s*',
                                    stripped)):
                            self.inputfiles[-1].citekeys.append((m['citekey'], m['description']))
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'(?P<tertype>cter|nter)\s*\[(?P<baseresidue>{RE_ATOMID})\]\s*:'
                                    fr'\s*(?P<patchnames>(\s*{RE_ATOMID})+)',
                                    stripped)):
                            for pres in m['patchnames'].split():
                                self.inputfiles[-1].terminals.append((m['tertype'], m['baseresidue'], pres))
                        elif sectionname is not None and (
                                m := re.match(fr'renameatoms\[(?P<resn>{RE_ATOMID})\]\s*:'
                                              fr'(?P<atomnames>(\s*{RE_ATOMID}\s*->\s*{RE_ATOMID})+)', stripped)):
                            renamelist = [
                                (m['resn'], m1['origname'], m1['newname'])
                                for m1 in re.finditer(
                                    fr'(?P<origname>{RE_ATOMID})\s*->\s*(?P<newname>{RE_ATOMID})', m['atomnames'])]
                            self.inputfiles[-1].atomrenamelist.extend(renamelist)
                        elif sectionname is not None and (
                                m := re.match(fr'(?P<end>[cn])ter_delete\s*:\s*'
                                              fr'(?P<atomnames>(\s*{RE_ATOMID}\s*)+)', stripped)):
                            if m['end'] == 'c':
                                self.inputfiles[-1].cter_delete.extend(m['atomnames'].split())
                            else:
                                self.inputfiles[-1].nter_delete.extend(m['atomnames'].split())
                        elif sectionname is not None and (
                                m := re.match(
                                    fr'arn\s*:\s*(?P<resname>{RE_ATOMID}|\*)\s+'
                                    fr'(?P<gromacsname>{RE_ATOMID})\s+(?P<charmmname>{RE_ATOMID})', stripped)):
                            self.arn.append(
                                AtomRenameRule(sectionname, m['resname'], m['gromacsname'], m['charmmname']))
                        else:
                            raise ValueError(f'Unknown line in input file: {line}')
                except ValueError as ve:
                    logger.critical(f'Error on line {lineno} in input file {filename} ("{line.rstrip()}": {str(ve)}')
                    raise

    @staticmethod
    def readRTF(f: TextIO, fileinfo: InputFileInfo) -> Tuple[RTPList, List[AtomType]]:
        """Parse a CHARMM residue topology file

        :param f: an open file handle
        :type f: file
        :param fileinfo: information on the input file
        :type f: InputFileInfo
        """
        atomtypes = []
        rtf = RTPList()
        firstpatch = None
        lastpatch = None
        autogenerate = None
        currentchargegroup = None
        linkatoms = []
        minorver = None
        majorver = None
        currentresidue = None
        remainder = ''  # remainder of the previous line, if continued.
        for lineno, line in enumerate(f, start=1):
            stripped, comment, remainder = splitline(remainder + line, '!', '-')
            upper = stripped.upper()
            if not upper:
                continue
            elif upper.startswith('*'):
                continue
            elif upper == 'END':
                break
            elif (m := re.match(
                    fr'MASS\s+(?P<typenum>[+-]?\d+)\s+(?P<typename>{RE_ATOMID})'
                    fr'\s+(?P<mass>{RE_FLOAT})(\s+(?P<symbol>[A-Z]+))?',
                    upper, re.ASCII)) is not None:
                gd = m.groupdict()
                atomtypes.append(AtomType(gd['typename'], int(gd['typenum']), float(gd['mass']), None, comment, f.name))
            elif (m := re.match(fr'(RESI|PRES)\w*\s+(?P<resn>{RE_ATOMID})\s+(?P<charge>[+-]?{RE_FLOAT})', upper,
                                re.ASCII)) is not None:

                gd = m.groupdict()
                if clashes := [r for r in rtf if r.name == gd['resn']]:
                    assert all([c.filename == f.name for c in clashes])
                    logger.warning(f'Found a second definition of residue {gd["resn"]} in file {f.name}.')
                    # a residue is redefined in this file
                    for c in clashes:
                        rtf.remove(c)
                if upper.startswith('RESI'):
                    # this is an ordinary residue topology entry
                    currentresidue = ResidueTopology(
                        gd['resn'], float(gd['charge']), fileinfo.moleculeclass,
                        f.name, lineno, comment)
                    currentresidue.defaultleftpatch = firstpatch
                    currentresidue.defaultrightpatch = lastpatch
                else:
                    assert upper.startswith('PRES')
                    # this is a patch entry
                    currentresidue = ResidueTopologyPatch(
                        gd['resn'], float(gd['charge']), fileinfo.moleculeclass,
                        f.name, lineno, comment
                    )
                if currentresidue.name in fileinfo.discardresidues:
                    # read the residue, just do not store
                    pass
                elif [r for r in rtf if r.name == gd['resn']] and (
                        (fileinfo.clashpolicy == ParameterClashPolicy.KEEP_NEWER) or
                        (gd['resn'] in fileinfo.overrideresidues)):
                    # remove the previous entry and store the new one
                    rtf.remove(rtf[gd['resn']])
                    rtf.append(currentresidue)
                elif [r for r in rtf if r.name == gd['resn']] and (
                        fileinfo.clashpolicy == ParameterClashPolicy.KEEP_OLDER):
                    # this residue is already present, and the older one needs to be kept, read the new version but do
                    # not store
                    pass
                else:
                    rtf.append(currentresidue)
                currentchargegroup = 0
            elif (m := re.match(
                    fr"ATOM\S*\s+(?P<name>[+-]?{RE_ATOMID})\s+(?P<atomtype>{RE_ATOMID})\s+(?P<pcharge>{RE_FLOAT})",
                    upper, re.ASCII)) is not None:
                gd = m.groupdict()
                currentresidue.atoms.append(Atom(
                    name=gd['name'],
                    atomtype=gd['atomtype'],
                    pcharge=float(gd['pcharge']),
                    cgroup=currentchargegroup,
                    comment=comment
                ))
            elif (m := re.match(
                    fr'PATC(H(I(NG?)?)?)?(\s+FIRST?\s+(?P<firstpatch>{RE_ATOMID}))?(\s+LAST\s+(?P<lastpatch>{RE_ATOMID}))?',
                    upper, re.ASCII)) is not None:
                gd = m.groupdict()
                currentresidue.defaultleftpatch = gd['firstpatch'] if gd['firstpatch'] is not None else firstpatch
                currentresidue.defaultrightpatch = gd['lastpatch'] if gd['lastpatch'] is not None else lastpatch
            elif re.match(
                    fr"(BOND|DOUB|TRIP)\w*((\s+[+-]?{RE_ATOMID}){{2}})+", upper) is not None:
                atomnames = upper.split()[1:]
                assert not len(atomnames) % 2
                for i in range(len(atomnames) // 2):
                    currentresidue.bonds.append(
                        Bond(
                            atoms=tuple(atomnames[2 * i:2 * i + 2]),
                            order=1 if upper.startswith('BOND') else (2 if upper.startswith('DOUB') else 3),
                            comment=comment
                        )
                    )
            elif re.match(fr"ANGL\w*((\s+[+-]?{RE_ATOMID}){{3}})+", upper) is not None:
                atomnames = upper.split()[1:]
                assert not len(atomnames) % 3
                for i in range(len(atomnames) // 3):
                    currentresidue.angles.append(
                        Angle(atoms=atomnames[3 * i:3 * i + 3], comment=comment))
            elif re.match(fr"IMPR\w*((\s+[+-]?{RE_ATOMID}){{4}})+", upper) is not None:
                atomnames = upper.split()[1:]
                assert not len(atomnames) % 4
                for i in range(len(atomnames) // 4):
                    currentresidue.impropers.append(
                        Improper(
                            atoms=atomnames[4 * i:4 * i + 4],
                            comment=comment))
            elif re.match(fr"DIHE\w*((\s+[+-]?{RE_ATOMID}){{4}})+", upper) is not None:
                atomnames = upper.split()[1:]
                assert not len(atomnames) % 4
                for i in range(len(atomnames) // 4):
                    currentresidue.dihedrals.append(
                        Dihedral(
                            atomnames[4 * i:4 * i + 4],
                            comment=comment))
            elif (m := re.match(
                    fr"(IC|BILD)\s+(?P<a1>[+-]?{RE_ATOMID}|\?\?)\s+"
                    fr"(?P<a2>[+-]?{RE_ATOMID}|\?\?)\s+(?P<a3>\*?[+-]?{RE_ATOMID}|\?\?)"
                    fr"\s+(?P<a4>[+-]?{RE_ATOMID}|\?\?)\s+(?P<b12>{RE_FLOAT})\s+(?P<theta123>{RE_FLOAT})"
                    fr"\s+(?P<chi>{RE_FLOAT})"
                    fr"\s+(?P<theta234>{RE_FLOAT})\s+(?P<b34>{RE_FLOAT})",
                    upper, re.ASCII)) is not None:
                gd = m.groupdict()
                currentresidue.ics.append(ICSpec(
                    atoms=(gd['a1'], gd['a2'], gd['a3'], gd['a4']),
                    b12=float(gd['b12']) * 0.1,
                    theta123=float(gd['theta123']),
                    chi=float(gd['chi']),
                    theta234=float(gd['theta234']),
                    b34=float(gd['b34']) * 0.1,
                    comment=comment))
            elif re.match(fr'DECL\w*\s+([+-]{RE_ATOMID})+', upper) is not None:
                linkatoms.extend(upper.split()[1:])
            elif upper.startswith('DEFA'):
                split = upper.split()[1:]
                firstpatch = [split[i + 1] for i in range(len(split)) if split[i].startswith('FIRS')][0]
                lastpatch = [split[i + 1] for i in range(len(split)) if split[i].startswith('LAST')][0]
            elif upper.startswith('AUTO'):
                autogenerate = upper.split()[1:]
            elif upper.startswith('GROU'):
                currentchargegroup += 1
            elif re.match(fr'(DONO|ACCE)\w*(\s+{RE_ATOMID}){{1,2}}', upper) is not None:
                pass  # do not collect DONOR/ACCEPTOR data: GROMACS does not need it.
            elif upper.startswith('DELE'):
                if not isinstance(currentresidue, ResidueTopologyPatch):
                    raise ValueError('DELETE line but not a patch residue.')
                if re.match(fr'DELE\w*\s+ATOM(\s+{RE_ATOMID})+', upper) is not None:
                    for atomname in upper.split()[2:]:
                        currentresidue.delete.append(Atom(atomname, '', 0.0, 0, comment))
                elif re.match(fr'DELE\w*\s+ACCE\w*(\s+{RE_ATOMID})+', upper) is not None:
                    # do not collect donor and acceptor data
                    pass
                elif re.match(fr'DELE\w*\s+DONO\w*(\s+{RE_ATOMID})+', upper) is not None:
                    # do not collect donor and acceptor data
                    pass
                elif re.match(fr"DELE\w*\s+IMPR\w*(\s+{RE_ATOMID}){{4}}", upper) is not None:
                    currentresidue.delete.append(Improper(atoms=upper.split()[2:], comment=comment))
                elif re.match(fr"DELE\w*\s+ANGL\w*((\s+{RE_ATOMID}){{3}})+", upper) is not None:
                    atomnames = upper.split()[2:]
                    assert not (len(atomnames) % 3)
                    for i in range(len(atomnames) // 3):
                        currentresidue.delete.append(Angle(atomnames[i * 3:i * 3 + 3], comment))
                elif re.match(fr"DELE\w*\s+BOND\w*((\s+{RE_ATOMID}){{2}})+", upper) is not None:
                    atomnames = upper.split()[2:]
                    assert not (len(atomnames) % 2)
                    for i in range(len(atomnames) // 2):
                        currentresidue.delete.append(Bond(atomnames[i * 2:i * 2 + 2], 1, comment))
                else:
                    raise ValueError(f'Invalid DELETE line: {line}')
            elif upper.startswith('LONEPAIR'):
                # this is not supported in GROMACS, do not store this
                try:
                    rtf.remove(currentresidue)
                    logger.warning(f'Residue {currentresidue.name} contains LONEPAIRs, skipping')
                except ValueError:
                    pass
                currentresidue.has_lonepair = True
            elif re.match(fr"CMAP\w*(\s+[+-]?{RE_ATOMID}){{8}}", upper) is not None:
                atoms = upper.split()[1:]

                currentresidue.cmaps.append(
                    Cmap(atoms[:4] + atoms[7:8], comment=comment))
            elif (not rtf) and (not atomtypes) and (not firstpatch) and (not lastpatch) and (not linkatoms) and (
                    not autogenerate) and (minorver is None) and (majorver is None):
                # try to interpret this as the version line
                try:
                    minorver, majorver = stripped.split()
                except ValueError:
                    majorver = stripped.strip()
                    minorver = '0'
                majorver = int(majorver)
                minorver = int(minorver)
            else:
                raise ValueError(f'Invalid line in RTF file: {line}')
        rtf.version = (majorver, minorver)
        return rtf, atomtypes

    @staticmethod
    def readPRM(f: TextIO, fileinfo: InputFileInfo):
        """Read CHARMM parameter file"""
        section = None
        prminfo = PRMInfo()
        remainder = ''  # for line continuation when a line ends with a dash ('-')
        lastcmaptype = None
        for line in f:
            if line.startswith('*'):
                # this is the title
                continue
            stripped, comment, remainder = splitline(remainder + line, '!', '-')
            upper = stripped.upper()  # CHARMM auto-uppercases the code
            if not upper:
                continue
            elif upper.startswith('ATOM'):
                section = 'ATOMS'
            elif upper.startswith('BOND'):
                section = 'BONDS'
            elif upper.startswith('ANGL'):
                section = 'ANGLES'
            elif upper.startswith('DIHE'):
                section = 'DIHEDRALS'
            elif upper.startswith('IMPR'):
                section = 'IMPROPERS'
            elif upper.startswith('NONB'):
                section = 'NONBONDED'
            elif upper.startswith('CMAP'):
                section = 'CMAPS'
            elif upper.startswith('NBFIX'):
                section = 'NBFIX'
            elif upper.startswith('HBOND'):
                section = 'HBOND'
            elif upper == 'END':
                break
            elif (section == 'ATOMS') and (
                    (m := re.match(fr'MASS\s+(?P<typenum>[+-]?\d+)\s+(?P<typename>{RE_ATOMID})\s+(?P<mass>{RE_FLOAT})',
                                   upper, re.ASCII)) is not None):
                gd = m.groupdict()
                prminfo.atomtypes.append(
                    AtomType(gd['typename'], int(gd['typenum']), float(gd['mass']), None,
                             comment=comment, filename=f.name))
            elif (section == 'BONDS') and (
                    (m := re.match(fr'(?P<atomtypes>({RE_ATOMID}\s+){{2}})(?P<Kb>{RE_FLOAT})\s+(?P<b0>{RE_FLOAT})',
                                   upper, re.ASCII)) is not None):
                gd = m.groupdict()
                # convert values to GROMACS format
                prminfo.bondtypes.append(
                    BondType(
                        tuple(gd['atomtypes'].split()),
                        1,
                        float(gd['Kb']) * 2 * 4.184 * 100,
                        float(gd['b0']) * 0.1,
                        comment, f.name))
            elif (section == 'ANGLES') and (
                    (m := re.match(
                        fr'(?P<atomtypes>({RE_ATOMID}\s+){{3}})(?P<Ktheta>{RE_FLOAT})\s+'
                        fr'(?P<Theta0>{RE_FLOAT})(\s+(?P<Kub>{RE_FLOAT})\s+(?P<S0>{RE_FLOAT}))?',
                        upper, re.ASCII)) is not None):
                gd = m.groupdict()
                prminfo.angletypes.append(
                    AngleType(
                        tuple(gd['atomtypes'].split()),
                        5,
                        float(gd['Ktheta']) * 2 * 4.184,
                        float(gd['Theta0']),
                        0.0 if gd['Kub'] is None else float(gd['Kub']) * 2 * 4.184 * 10 * 10,
                        0.0 if gd['S0'] is None else float(gd['S0']) * 0.1,
                        comment, f.name
                    )
                )
            elif (section == 'DIHEDRALS') and (
                    (m := re.match(
                        fr'(?P<atomtypes>({RE_ATOMID}\s+){{4}})(?P<Kchi>{RE_FLOAT})\s+(?P<n>\d+)\s+'
                        fr'(?P<delta>{RE_FLOAT})', upper, re.ASCII)) is not None):
                gd = m.groupdict()
                prminfo.dihedraltypes.append(DihedralType(
                    tuple(gd['atomtypes'].split()),
                    9,
                    float(gd['Kchi']) * 4.184,
                    int(gd['n']),
                    float(gd['delta']),
                    comment, f.name))
            elif (section == 'DIHEDRALS') and (
                    (m := re.match(
                        fr'(?P<atomtypes>({RE_ATOMID}\s+){{4}})(?P<Kchi>{RE_FLOAT})\s+'
                        fr'(?P<delta>{RE_FLOAT})', upper, re.ASCII)) is not None):
                # dihedral parameter variant where the multiplicity is omitted: default to 1
                gd = m.groupdict()
                prminfo.dihedraltypes.append(DihedralType(
                    tuple(gd['atomtypes'].split()),
                    9,
                    float(gd['Kchi']) * 4.184,
                    1,
                    float(gd['delta']),
                    comment, f.name))
            elif (section == 'IMPROPERS') and (
                    (m := re.match(
                        fr'(?P<atomtypes>({RE_ATOMID}\s+){{4}})'
                        fr'(?P<Kpsi>{RE_FLOAT})\s+(?P<ignored>{RE_FLOAT})\s+(?P<psi0>{RE_FLOAT})',
                        upper, re.ASCII)) is not None):
                gd = m.groupdict()
                prminfo.impropertypes.append(ImproperType(
                    tuple(gd['atomtypes'].split()),
                    2,
                    float(gd['Kpsi']) * 2 * 4.184,
                    float(gd['psi0']),
                    comment, f.name
                ))
            elif (section == 'IMPROPERS') and (
                    (m := re.match(
                        fr'(?P<atomtypes>({RE_ATOMID}\s+){{4}})'
                        fr'(?P<Kpsi>{RE_FLOAT})\s+(?P<psi0>{RE_FLOAT})',
                        upper, re.ASCII)) is not None):
                # variant of improper dihedral parameters where the multiplicity is omitted
                gd = m.groupdict()
                prminfo.impropertypes.append(ImproperType(
                    tuple(gd['atomtypes'].split()),
                    2,
                    float(gd['Kpsi']) * 2 * 4.184,
                    float(gd['psi0']),
                    comment, f.name
                ))
            elif ((section == 'CMAPS') and
                  bool(prminfo.cmaptypes) and
                  (lastcmaptype is not None) and
                  (len(lastcmaptype.data) < lastcmaptype.count ** 2)):
                # the last cmap entry is incomplete.
                if m := re.match(fr'({RE_FLOAT}\s*)+', upper, re.ASCII) is None:
                    raise ValueError(f'Invalid CMAP data line: {line}')
                lastcmaptype.data += tuple([float(x) * 4.184 for x in upper.split()])
            elif section == 'CMAPS':
                # We are in the CMAPS section and either:
                #    - this is the first CMAP parameter entry to read, or
                #    - the most recently read CMAP parameter entry is complete.
                # In both cases: we start a new CMAP entry
                if (m := re.match(fr'(?P<atomtypes>({RE_ATOMID}\s+){{8}})(?P<count>\d+)', upper, re.ASCII)) is None:
                    raise ValueError(f'Invalid CMAP spec line: {line}')
                gd = m.groupdict()
                atomtypes = tuple(gd['atomtypes'].split())
                assert len(atomtypes) == 8  # the regex should ensure this
                if atomtypes[1:4] != atomtypes[4:7]:
                    # GROMACS identifies the CMAP entry with 5 atoms, because only neighbouring dihedrals are supported.
                    # In the CHARMM FF all four atoms are given, but they are always "1, 2, 3, 4, 2, 3, 4, 5".
                    # Ensure that this is the case.
                    raise ValueError(f'Atom types #2-4 ({atomtypes[1:4]}) are not the same as #5-7 ({atomtypes[4:7]}): '
                                     'this CMAP type is not supported by GROMACS!')
                lastcmaptype = CmapType(atomtypes[:4] + atomtypes[7:8], 1,
                                        int(gd['count']),
                                        (),  # data is empty: we will read this soon
                                        comment, f.name)

                prminfo.cmaptypes.append(lastcmaptype)
            elif (section == 'NONBONDED') and (
                    (m := re.match(
                        fr'(?P<atomtype>{RE_ATOMID})\s+{RE_FLOAT}\s+(?P<epsilon>{RE_FLOAT})\s+(?P<Rmindiv2>{RE_FLOAT})'
                        fr'(\s+{RE_FLOAT}\s+(?P<epsilon_14>{RE_FLOAT})\s+(?P<Rmindiv2_14>{RE_FLOAT}))?',
                        upper, re.ASCII)) is not None):
                gd = m.groupdict()
                prminfo.nonbondedtypes.append(NonbondedType(
                    gd['atomtype'],
                    abs(float(gd['epsilon']) * 4.184),
                    float(gd['Rmindiv2']) * 2 / (10 * 2 ** (1 / 6)),
                    None if gd['epsilon_14'] is None else abs(float(gd['epsilon_14'])) * 4.184,
                    None if gd['Rmindiv2_14'] is None else float(gd['Rmindiv2_14']) * 2 / (10 * 2 ** (1 / 6)),
                    comment, f.name
                ))
            elif (section == 'NBFIX') and (
                    (m := re.match(
                        fr'(?P<atomtypes>({RE_ATOMID}\s+){{2}})'
                        fr'(?P<epsilon>{RE_FLOAT})\s+(?P<Rmin>{RE_FLOAT})',
                        upper, re.ASCII)) is not None):
                gd = m.groupdict()
                prminfo.nbfixtypes.append(
                    NbfixType(
                        tuple(gd['atomtypes'].split()), 1,
                        abs(float(gd['epsilon']) * 4.184),
                        float(gd['Rmin']) / 2 ** (1 / 6) * 0.1,
                        comment, f.name
                    )
                )
            elif section == 'HBOND':
                raise ValueError(f'Unknown line in HBOND section: {line}')
            else:
                raise ValueError(f'Invalid line in section {section}: {line}')
        return prminfo

    def readCHARMMstream(self, fileinfo: InputFileInfo):
        """

        :param fileinfo:
        :type fileinfo:
        :return:
        :rtype:
        """
        with open(fileinfo.path, 'rt') as f:
            rtp = RTPList()
            prminfo = PRMInfo()
            remainder = ''
            for line in f:
                if line.startswith('*'):
                    # this is the header
                    remainder = ''
                    continue
                stripped, comment, remainder = splitline(remainder + line, '!', '-')
                if not stripped:
                    continue
                upper = stripped.upper()
                split = upper.split()
                if (split[0] == 'READ') and (split[1] == 'RTF') and ('CARD' in split) and ('NAME' not in split):
                    newrtp, atomtypes = self.readRTF(f, fileinfo)
                    rtp.clashpolicy = fileinfo.clashpolicy
                    rtp.extend(newrtp, fileinfo.overrideresidues)
                    prminfo_ = PRMInfo()
                    prminfo_.atomtypes.extend(atomtypes)
                    prminfo.clashpolicy = fileinfo.clashpolicy
                    prminfo += prminfo_
                elif (split[0] == 'READ') and (split[1].startswith('PARA')) and ('CARD' in split) and (
                        'NAME' not in split):
                    prminfo_ = self.readPRM(f, fileinfo)
                    prminfo.clashpolicy = fileinfo.clashpolicy
                    prminfo += prminfo_
                elif split[0] == 'RETURN':
                    return rtp, prminfo
                else:
                    logger.debug(f'Warning: skipping line {line.rstrip()}')
        return rtp, prminfo

    @staticmethod
    def renameAtomTypes(prminfo: PRMInfo, rtp: RTPList, renamelist: List[Tuple[str, str]]):
        for oldname, newname in renamelist:
            prminfo.renameAtomType(oldname, newname)
            for r in rtp:
                # logger.debug(f'Renaming atom type in {r.name}')
                r.renameAtomType(oldname, newname)
        return prminfo, rtp

    def parse(self):
        self.citations = CitationList(self.bibtexfile)

        # clear the residue topology and parameter lists
        self.rtp = RTPList()
        self.prminfo = PRMInfo()

        moleculeclasses = []
        # load the files in order
        for fileinfo in self.inputfiles:
            if fileinfo.moleculeclass not in moleculeclasses:
                moleculeclasses.append(fileinfo.moleculeclass)
            # load the file: either an RTF, a PRM or a STR file.
            if fileinfo.filetype == InputFileType.CHARMM_RTF:
                logger.info(f'Reading residue topology file {fileinfo.path}')
                with open(fileinfo.path, 'rt') as f:
                    rtf_, atomtypes = self.readRTF(f, fileinfo)
                    prminfo_ = PRMInfo()
                    prminfo_.atomtypes.extend(atomtypes)
            elif fileinfo.filetype == InputFileType.CHARMM_PRM:
                logger.info(f'Reading parameter file {fileinfo.path}')
                with open(fileinfo.path, 'rt') as f:
                    prminfo_ = self.readPRM(f, fileinfo)
                    rtf_ = RTPList()
            elif fileinfo.filetype == InputFileType.CHARMM_STR:
                logger.info(f'Reading stream file {fileinfo.path}')
                rtf_, prminfo_ = self.readCHARMMstream(fileinfo)
            else:
                assert False

            # we have read a file. Do some post-processing

            # Post-processing #1: convert some patches to actual residues, e.g. ACE or NME
            for pres in rtf_.patches():
                try:
                    conv = pres.convertToResidue()
                    if conv.name == pres.name:
                        rtf_.remove(pres)
                    rtf_.append(conv)
                except NotImplementedError:
                    pass

            # Post-processing #2: rename some atom types
            prminfo_, rtf_ = self.renameAtomTypes(prminfo_, rtf_, fileinfo.atomtyperenamelist)

            # Post-processing #3: rename some residues
            for oldname, newname in fileinfo.renameresidues:
                for r in rtf_:
                    if r.name == oldname:
                        r.name = newname
                        logger.info(f'Renamed residue {oldname} -> {r.name}')

            # Post-processing #3b: rename some residues while keeping the original
            for oldname, newname in fileinfo.copyresidues:
                for r in rtf_:
                    if r.name == oldname:
                        r = copy.copy(r)
                        r.name = newname
                        rtf_.append(r)
                        logger.info(f'Copied residue {oldname} -> {r.name}')

            # Post-processing #3c: rename atoms in some residues
            for resn, oldname, newname in fileinfo.atomrenamelist:
                rtf_[resn].renameAtom(oldname, newname)

            # Extend the RTP list
            t0 = time.monotonic()
            self.rtp.extend(rtf_, fileinfo.overrideresidues)
            logger.debug(f'Time for extending RTP list: {time.monotonic() - t0:.2f} seconds')

            # extend the parameter list
            t0 = time.monotonic()
            self.prminfo.clashpolicy = fileinfo.clashpolicy
            self.prminfo += prminfo_
            logger.debug(f'Time for extending prminfo: {time.monotonic() - t0:.2f} seconds')

        logger.info('Loading of files done. Post-processing...')
        # after everything has been loaded, generate 1-4 LJ interactions, i.e. [ pairtypes ] in GROMACS terminology.
        self.prminfo.generatePairTypes()

        # search for duplicate residues
        logger.info('Searching for duplicate residue / patch entries')
        duplicates_found = 0
        duplicates_resolved = 0
        duplicates = list(self.rtp.iter_duplicates())
        for dup in duplicates:
            remaining = None
            duplicates_found += 1
            name = [r.name for r in dup]
            # check if one has priority over another
            fileinfos_all = [(r, [f for f in self.inputfiles if f.path == r.filename][0]) for r in dup]
            # discard if needed
            fileinfos = [(r, fi) for r, fi in fileinfos_all if r.name not in fi.discardresidues]
            if len(fileinfos) < len(fileinfos_all):
                discarded = [(r, fi) for r, fi in fileinfos_all if r.name in fi.discardresidues]
                for r, fi in discarded:
                    logger.info(f'Discarded definition of {r.name} from file {fi.path}.')
            if len(fileinfos) == 1:
                # only one remained, this duplicate is successfully resolved
                logger.info(f'Duplicates of residue {name} successfully resolved with discarding.')
                remaining = fileinfos[0][0]
            else:
                # check if one is explicitly overridden
                override = [(r, fi) for r, fi in fileinfos if r.name in fi.overrideresidues]
                if len(override) == 1:
                    # this duplicate is successfully resolved
                    logger.info(
                        f'Only one of the duplicates of residue {name} is overriding (from file {override[0][1].path}): good.')
                    remaining = override[0][0]
            if remaining is not None:
                assert remaining in dup
                for r in dup:
                    if r is not remaining:
                        logger.debug(
                            f'Removing duplicate residue {r.name}, defined in file {r.filename} on line {r.lineno}.')
                        self.rtp.remove(r)
                        logger.debug(
                            f'{[(r_.name, type(r_), r_.filename, r_.lineno) for r_ in self.rtp._residues if r_.name == r.name]}')

                duplicates_resolved += 1
                logger.info(
                    f'Duplicate for residue {remaining.name} successfully resolved, kept definition from file {remaining.filename} on line {remaining.lineno}')
            else:
                logger.error(
                    f'Residue {name} defined multiple times:\n' + '\n'.join(
                        [f'   On line {r.lineno} in file {r.filename}' for r in dup]))

        if duplicates_found > duplicates_resolved:
            raise RuntimeError('Duplicate residues found!')

        remaining_duplicates = list(self.rtp.iter_duplicates())
        if remaining_duplicates:
            print(remaining_duplicates)
            assert False

        # Post-processing #3: apply patches. This may include residues loaded from other files previously.
        logger.info('Applying patches')
        for fileinfo in self.inputfiles:
            for resn1, resn2, patchedname in fileinfo.patching:
                # logger.debug(f'Trying to apply patches: {resn1}, {resn2}, {patchedname}')
                try:
                    base = self.rtp.getresidue(resn1)
                except IndexError:
                    logger.critical(f'Cannot patch: residue {resn1} cannot be found')
                    raise
                try:
                    patch = self.rtp.getpatch(resn2)
                except IndexError:
                    logger.critical(f'Cannot patch: residue {resn2} cannot be found')
                    raise
                self.rtp.append(patch.apply(base, patchedname))

        logger.info('Creating TDB entries from patches')
        for moleculeclass in sorted({f.moleculeclass for f in self.inputfiles}):
            lefttermini = []
            righttermini = []
            for fileinfo in self.inputfiles:
                if fileinfo.moleculeclass != moleculeclass:
                    continue
                # Post-processing #4: create termini entries
                for terminustype, baseresidue, patch in fileinfo.terminals:
                    pres = self.rtp.getpatch(patch)
                    base = self.rtp.getresidue(baseresidue)
                    pres.convertToTerminus(base, terminustype.lower() == 'nter', self.prminfo)
                    # append the patch residue to the end of the list: this way we ensure that they will appear in the .tdb
                    # file in the same order as the nter and cter entries appear in the .in file.
                    self.rtp.remove(pres)
                    self.rtp.append(pres)
                    if terminustype.lower() == 'nter':
                        lefttermini.append(pres)
                    else:
                        righttermini.append(pres)

            # create additional atom deletions to allow applying the terminus to an already patched one
            for lis, label in [(lefttermini, 'left'), (righttermini, 'right')]:
                # first, get atom names specified in the charmm2gmx.in file under cter_delete and nter_delete lines
                deletableatomnames = set(
                    itertools.chain(
                        *[(f.cter_delete if (label == 'right') else f.nter_delete)
                          for f in self.inputfiles
                          if f.moleculeclass == moleculeclass
                          ]
                    )
                )
                # then find all added atoms in each terminus
                for entry in lis:
                    for atomlist, additionrule in entry.atomadditions:
                        deletableatomnames.update(set(atomlist))
                logger.info(
                    f'Additional [ delete ] clauses for {label} termini for molecule class "{moleculeclass}": ' +
                    ", ".join(deletableatomnames)
                )
                for entry in lis:
                    # find atoms which are added by this patch
                    addedatomsnames = set()
                    for atomnamelist, rules in entry.atomadditions:
                        addedatomsnames.update(atomnamelist)
                    for da in deletableatomnames:
                        if da in addedatomsnames:
                            # do not delete this atom: it will be added anyway. PDB2GMX runs into an infinite loop.
                            continue
                        if not [a.name for a in entry.atomdeletions if a.name == da]:
                            # if no deletion rule exists for this atom, add it
                            entry.atomdeletions.append(Atom(da, '', 0.0, -1, 'Dummy atom, for deletion'))
