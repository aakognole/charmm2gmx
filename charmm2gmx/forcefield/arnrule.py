# conding: utf-8

"""GROMACS .arn file representation"""


class AtomRenameRule:
    """Class to represent lines in the atom renaming database files (*.arn) of GROMACS

    :ivar moleculeclass: molecule class (na, prot, carb, cgenff etc.)
    :type moleculeclass: str
    :ivar residuename: the name of the residue (* to apply for all residues)
    :type residuename: str
    :ivar gromacsname: name in GROMACS-terminology (e.g. H)
    :type gromacsname: str
    :ivar ffname: name force-field terminology (e.g. HN in CHARMM)
    :type ffname: str
    """
    residuename: str
    gromacsname: str
    ffname: str
    moleculeclass: str

    def __init__(self, moleculeclass: str, residuename: str, gromacsname: str, ffname: str):
        self.moleculeclass = moleculeclass
        self.residuename = residuename
        self.gromacsname = gromacsname
        self.ffname = ffname

    def __str__(self):
        """Write out in GROMACS-representation"""
        return f'{self.residuename:<10s} {self.gromacsname:<10s} {self.ffname:<10s}\n'
