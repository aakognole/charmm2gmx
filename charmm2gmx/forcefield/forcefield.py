# coding=utf-8
"""Abstract base class for a force field topology/parameter set"""
import itertools
import logging
import os
import shutil
from typing import List, Set, Optional

import pkg_resources

from .arnrule import AtomRenameRule
from .inputfileinfo import InputFileInfo
from ..prminfo import PRMInfo
from ..residuetopology.rtplist import RTPList
from ..utils import GMXFileHeader, VERSION
from ..utils.citations import CitationList
from ..utils.r2b import R2Bdata
from ..watermodels import WATERMODELS

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ForceField:
    """Representation of a force field (interaction parameters and residue topologies)

    :ivar rtp: list of residue topologies
    :type rtp: :class:`RTPList`
    :ivar prminfo: interaction parameters
    :type prminfo: :class:`PRMInfo`
    :ivar citations: citation list
    :type citations: :class:`CitationList` or None
    :ivar inputfiles: list of input files
    :type inputfiles: list of :class:`InputFileInfo`
    """
    rtp: RTPList
    prminfo: PRMInfo
    citations: Optional[CitationList] = None
    inputfiles: List[InputFileInfo]
    arn: List[AtomRenameRule]

    def __init__(self) -> None:
        """Create a new instance"""
        self.inputfiles = []
        self.rtp = RTPList()
        self.prminfo = PRMInfo()
        self.citations = None
        self.arn = []

    def moleculetypes(self) -> Set[str]:
        """Return a set of molecule type strings"""
        return {fi.moleculeclass for fi in self.inputfiles}

    def compare(self, other: "ForceField", no_fuss_on_missing_from_other: bool = True) -> bool:
        """Compare two force fields

        :param other: the other force field
        :type other: :class:`ForceField`
        """
        # compare force field parameters
        self.prminfo.compare(other.prminfo, no_fuss_on_missing_from_other)

        # validate unicity of the 1st FF
        for label, ff in [('1st', self), ('2nd', other)]:
            multiple = {rname: count for rname in {r.name for r in ff.rtp} if
                        (count := len([r for r in ff.rtp if r.name == rname])) > 1}
            if multiple:
                raise ValueError(
                    f'The following residues are defined multiply in the {label} FF: ' +
                    ', '.join([f'{rname} ({count}×)' for rname, count in multiple.items()]))
        # find missing residues
        rtpmissingfromother = [r for r in self.rtp if not [r_ for r_ in other.rtp if r_.name == r.name]]
        rtpmissingfromthis = [r for r in other.rtp if not [r_ for r_ in self.rtp if r_.name == r.name]]
        # find residues defined differently
        rtpdifferent = []
        for r in [r_ for r_ in self.rtp if r_ not in rtpmissingfromother]:
            r_other = [r_ for r_ in other.rtp if r.name == r_.name][0]
            if r != r_other:
                rtpdifferent.append((r, r_other))
        if rtpmissingfromthis:
            logger.warning('RTP entries missing from this: ' + ', '.join(sorted([r.name for r in rtpmissingfromthis])))
        if rtpmissingfromother and (not no_fuss_on_missing_from_other):
            logger.debug('RTP entries missing from other: ' + ', '.join(sorted([r.name for r in rtpmissingfromother])))
        if rtpdifferent:
            logger.error(
                'RTP entries defined in both but differently: ' + ', '.join(sorted([r[0].name for r in rtpdifferent])))
            for r1, r2 in rtpdifferent:
                logger.error(r1.diff(r2))
        # now compare the hydrogen addition databases
        logger.info('Checking hydrogen addition databases')
        for residue in [r for r in self.rtp
                        if (r not in rtpmissingfromother)
                           and (r.hydrogenadditionrules is not None)
                           and (other.rtp[r.name].hydrogenadditionrules is not None)]:
            otherresidue = other.rtp[residue.name]

            if len(residue.hydrogenadditionrules) != len(otherresidue.hydrogenadditionrules):
                logger.error(f'Different number of hydrogen addition rules in residue {residue.name}')
                continue
            for hr1, hr2 in zip(sorted(residue.hydrogenadditionrules, key=lambda hr: hr.hname),
                                sorted(otherresidue.hydrogenadditionrules, key=lambda hr: hr.hname)):
                if hr1 != hr2:
                    logger.warning(f'{residue.name:<10s}: {str(hr1)} != {str(hr2)}')

        return True

    def validate(self) -> None:
        """Validate this force field

        :raises ValueError: if validation fails
        :return: None if successful
        """
        self.prminfo.validate()
        rtpnames = [r.name for r in self.rtp]
        duplicatertp = [n for n in set(rtpnames) if rtpnames.count(n) > 1]
        if duplicatertp:
            raise ValueError(f'The following residue names are duplicate: {", ".join(sorted(duplicatertp))}')

    def writeMainFF(self, ffdir: str) -> None:
        """Write the main force field file (forcefield.itp)

        :param ffdir: output directory
        :type ffdir: str
        """
        with open(os.path.join(ffdir, 'forcefield.itp'), 'wt') as f:
            f.write(GMXFileHeader())
            f.write('; please see forcefield.doc for a list of source files and references\n\n')
            f.write('#define _FF_CHARMM\n')
            f.write('\n')
            f.write('[ defaults ]\n')
            f.write('; nbfunc	comb-rule	gen-pairs	fudgeLJ	fudgeQQ\n')
            f.write('       1           2         yes       1.0     1.0\n')
            f.write('\n')
            f.write('#include "ffnonbonded.itp"\n')
            f.write('#include "ffbonded.itp"\n')
            f.write('; implicit solvent parameters are not supported as of GROMACS 2019:\n')
            f.write(
                '; https://manual.gromacs.org/documentation/2019/release-notes/2019/major/removed-functionality.html\n')
            f.write('\n')
            f.write('; The original port supported old CHARMM36 CMAP parameters. We do not do it.\n')
            f.write('#ifdef USE_OLD_C36\n')
            f.write('#error This port does not support the old CHARMM36 CMAP parameters.\n')
            f.write('#endif\n')
            f.write('\n')
            f.write('#include "cmap.itp"\n')
            f.write('\n')
            f.write('#include "nbfix.itp"\n')

    def writeDoc(self, ffdir: str, charmmversion: Optional[str] = None, cgenffversion: Optional[str] = None) -> None:
        """Write forcefield.doc file

        :param ffdir: force field directory
        :type ffdir: str
        :param charmmversion: CHARMM version number to write into the forcefield.doc file
        :type charmmversion: str or None
        :param cgenffversion: CGenFF version number to write into the forcefield.doc file
        :type cgenffversion: str or None
        """
        with open(os.path.join(ffdir, 'forcefield.doc'), 'wt') as f:
            f.write('CHARMM all-atom force field\n\n')
            f.write('**************************************************************************************\n'
                    '* GROMACS port of the CHARMM force field written by Andras Wacha and Justin Lemkul   *\n'
                    '* based on the original work of:                                                     *\n'
                    '* E. Prabhu Raman, Justin A. Lemkul, Robert Best and Alexander D. MacKerell, Jr.     *\n'
                    '*                                                                                    *\n'
                    '* Conversion script available at: https://gitlab.com/awacha/charmm2gmx               *\n'
                    '* On-line documentation         : https://awacha.gitlab.io/charmm2gmx                *\n'
                    '*                                                                                    *\n'
                    '* Please read and cite the following paper:                                          *\n'
                    '*     Andras F. Wacha and Justin A. Lemkul: "charmm2gmx: An Automated Method to Port *\n'
                    '*     the CHARMM Additive Force Field to GROMACS". Journal of Chemical Information   *\n'
                    '*     and Modeling 2023. 63(14), 4246-4252. DOI: 10.1021/acs.jcim.3c00860            *\n'
                    '*                                                                                    *\n'
                    f'* This port was created with charmm2gmx v{VERSION:<s42} *\n'
                    '*                                                                                    *\n'
                    '* Questions, bug reports etc: https://gitlab.com/awacha/charmm2gmx/issues            *\n'
                    '**************************************************************************************\n'
                    )
            f.write('\n')
            if (charmmversion is not None) and (cgenffversion is not None):
                f.write(f'Parameters taken from CHARMM{charmmversion} and CGenFF {cgenffversion}\n\n')
            elif charmmversion is not None:
                f.write(f'Parameters taken from CHARMM{charmmversion}\n\n')
            elif cgenffversion is not None:
                f.write(f'Parameters taken from CGenFF {cgenffversion}\n\n')
            f.write('Parameters and topologies are included from the following files:\n')
            f.write('================================================================\n')
            for molclass in self.moleculetypes():
                f.write(f'\n{molclass}:\n' + '-' * (len(molclass) + 1) + '\n')
                for fi in self.inputfiles:
                    if fi.moleculeclass == molclass:
                        f.write(f'- {fi.path}\n')
            f.write('\nReferences:\n==========\n')
            for molclass in self.moleculetypes():
                f.write(f'\n{molclass}:\n' + '-' * (len(molclass) + 1) + '\n')
                citations = set(
                    itertools.chain(*[fi.citekeys for fi in self.inputfiles if fi.moleculeclass == molclass]))
                for ck, d in [(ck, d) for ck, d in citations if d is None]:
                    f.write('- ' + str(self.citations[ck]) + '\n')
                for description in sorted({d for ck, d in citations if d is not None}):
                    f.write(description + ':\n')
                    for ck in [ck for ck, d in citations if d == description]:
                        f.write('    ' + str(self.citations[ck]) + '\n')

        pass

    def writeRTP(self, ffdir: str) -> None:
        """Write residue topology files

        :param ffdir: force field directory
        :type ffdir: str
        """
        for moleculeclass in self.rtp.moleculeclasses():
            filename = os.path.join(ffdir, moleculeclass + '.rtp')
            with open(filename, 'wt') as f:
                f.write(GMXFileHeader())
                f.write('; Residue topology information from the CHARMM force field\n')
                filenames = {r.filename for r in self.rtp if r.moleculeclass == moleculeclass}
                f.write("""
[ bondedtypes ]
; Col 1: Type of bond 
; Col 2: Type of angles 
; Col 3: Type of proper dihedrals 
; Col 4: Type of improper dihedrals 
; Col 5: Generate all dihedrals if 1, only heavy atoms of 0. 
; Col 6: Number of excluded neighbors for nonbonded interactions 
; Col 7: Generate 1,4 interactions between pairs of hydrogens if 1 
; Col 8: Remove propers over the same bond as an improper if it is 1 
; bonds  angles  dihedrals  impropers  all_dihedrals  nrexcl  HH14  RemoveDih 
    1       5        9          2            1           3      1       0
""")
                for fn in sorted(filenames):
                    f.write(f'\n; residue topologies from file {fn}\n')
                    for rtp in sorted(self.rtp.residues(fn), key=lambda rtp_: rtp_.name):
                        f.write(str(rtp))

    def writeTDB(self, ffdir: str) -> None:
        """Write termini database files

        :param ffdir: force field directory
        :type ffdir: str
        """
        for moleculeclass in self.rtp.moleculeclasses():
            logger.debug(f'Writing TDB for molecule class {moleculeclass}')
            for left in [True, False]:
                filename = os.path.join(ffdir, moleculeclass + f'{".n" if left else ".c"}.tdb')
                logger.debug(
                    [(r.name, r.isleftterminus) for r in self.rtp.patches() if (r.moleculeclass == moleculeclass)])
                termini = [r for r in self.rtp.patches() if
                           (r.moleculeclass == moleculeclass) and (r.isleftterminus == left)]
                logger.debug(f'Termini: {termini}')
                with open(filename, 'wt') as f:
                    logger.debug(f'Writing {filename}')
                    f.write(GMXFileHeader())
                    f.write('; Termini database from the CHARMM force field\n')
                    f.write('\n[ None ]\n; Empty, do-nothing terminus\n')
                    filenames = {r.filename for r in termini}
                    logger.debug(f'File names {filenames}')
                    for fn in sorted(filenames):
                        f.write(f'\n; residue topologies from file {fn}\n')
                        for rtp in [t for t in termini if t.filename == fn]:
                            logger.debug('')
                            f.write('\n' + str(rtp))

    def writeARN(self, ffdir: str) -> None:
        """Write atom renaming database files

        :param ffdir: force field directory
        :type ffdir: str
        """
        for moleculeclass in {a.moleculeclass for a in self.arn}:
            logger.debug(f'Writing ARN for molecule class {moleculeclass}')
            with open(os.path.join(ffdir, f'{moleculeclass}.arn'), 'wt') as f:
                f.write('; atom renaming specification\n')
                f.write('; residue gromacs    forcefield\n')
                for arn in [a for a in self.arn if a.moleculeclass == moleculeclass]:
                    f.write(str(arn))

    def writeWaterModels(self, outputdir: str) -> None:
        """Write the water models in GROMACS format

        :param outputdir: force field directory
        :type outputdir: str
        """
        with open(os.path.join(outputdir, 'watermodels.dat'), 'wt') as f:
            for wm in WATERMODELS.values():
                f.write(f'{os.path.splitext(wm.itpfile)[0]:<10s} {wm.name:<10s} {wm.description}\n')
                shutil.copy(
                    os.path.join(pkg_resources.resource_filename(
                        'charmm2gmx', os.path.join('resource', 'watermodels', wm.itpfile))),
                    os.path.join(outputdir, wm.itpfile))

    def writeGMXFormat(self, outputdir: str, charmmversion: Optional[str] = None,
                       cgenffversion: Optional[str] = None) -> None:
        """Write the parameter set in GROMACS format

        :param outputdir: force field directory (created if not exists)
        :type outputdir: str
        :param charmmversion: CHARMM version number to write into the forcefield.doc file
        :type charmmversion: str or None
        :param cgenffversion: CGenFF version number to write into the forcefield.doc file
        :type cgenffversion: str or None
        """
        os.makedirs(outputdir, exist_ok=True)
        self.prminfo.writeAtomtypes(os.path.join(outputdir, 'atomtypes.atp'))
        self.prminfo.writeNonbonded(os.path.join(outputdir, 'ffnonbonded.itp'))
        self.prminfo.writeNbfix(os.path.join(outputdir, 'nbfix.itp'))
        self.prminfo.writeBonded(os.path.join(outputdir, 'ffbonded.itp'))
        self.prminfo.writeCMAP(os.path.join(outputdir, 'cmap.itp'))
        self.writeDoc(outputdir, charmmversion, cgenffversion)
        self.writeMainFF(outputdir)
        self.writeRTP(outputdir)
        self.writeTDB(outputdir)
        self.writeR2B(outputdir)
        self.writeARN(outputdir)
        self.writeIons(outputdir)
        self.writeWaterModels(outputdir)
        with open(os.path.join(outputdir, 'ffmissingdihedrals.itp'), 'wt') as f:
            f.write(GMXFileHeader())
            f.write("""; Dummy zero dihedral types for those which are missing in CHARMM
; Note: in some residue topologies in CHARMM, especially in the CGenFF part (e.g. C3, BUTY) some dihedrals have no
; corresponding interaction parameters based on the types of the participating atoms (e.g. C1-C2-C3-C4 in BUTY). 
; CHARMM simply ignores these dihedrals, giving you messages like 
; "CHECKDH> dihedral :    1    3    4    7 will NOT be generated". Grompp however gives you an error.
; As a workaround, this file contains [ dihedraltypes ] with zero force constant for these dihedral types.
;
; This file is presently empty, you can populate it by using `charmm2gmx findmissingpars`. 
""")

    def writeR2B(self, ffdir: str) -> None:
        """Write residue to rtp building block tables

        :param ffdir: force field directory
        :type ffdir: str
        """

        for moleculeclass in self.rtp.moleculeclasses():
            filename = os.path.join(ffdir, moleculeclass + '.r2b')
            residuenames = [r.name for r in self.rtp if r.moleculeclass == moleculeclass]
            with open(filename, 'wt') as f:
                f.write(GMXFileHeader())
                f.write('; Residue to rtp building block table\n')
                f.write(';    PDB midchain    start      end     2ter\n')
                r2bs = [r2b for r2b in R2Bdata if any([n in residuenames for n in r2b[1:]])]
                for r2b in r2bs:
                    f.write(f'{r2b.pdb:>8s} {r2b.normal if r2b.normal is not None else r2b.normal:>8s} '
                            f'{r2b.Nter if r2b.Nter is not None else r2b.normal:>8s} '
                            f'{r2b.Cter if r2b.Cter is not None else r2b.normal:>8s} '
                            f'{r2b.twoter if r2b.twoter is not None else r2b.normal:>8s}\n')

    def writeIons(self, ffdir: str) -> None:
        """Write molecule type entries for ions in the ions.itp

        :param ffdir: force field directory
        :type ffdir: str
        """

        with open(os.path.join(ffdir, 'ions.itp'), 'wt') as f:
            f.write(GMXFileHeader())
            f.write('; Molecule types for ions\n\n')
            for resi in self.rtp.residues():
                if (len(resi.atoms) < 5) and (sum([a.pcharge for a in resi.atoms]) != 0):
                    # this is an ion
                    resi.assignAtomTypes(self.prminfo)
                    resi.generateAngles()
                    resi.generateDihedrals()
                    resi.assignParameters(self.prminfo)
                    f.write('\n' + resi.toMoleculeType(resi.name))
