# coding=utf-8
"""GROMACS force field parser"""
import collections
import logging
import os.path
import re
from typing import List, Tuple, Optional, Dict

from .forcefield import ForceField
from ..prminfo import AtomType, BondType, AngleType, DihedralType, ImproperType, CmapType, NbfixType, \
    PairType, ConstraintType
from ..utils.regex import RE_ATOMID, RE_FLOAT, RE_INT, RE_MACRO
from ..residuetopology import ResidueTopology, Atom, Bond, Angle, Dihedral, Improper, Cmap, AtomAdditionRule
from ..utils import splitline

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

DefaultsType = collections.namedtuple('DefaultsType', ['nbfunc', 'combrule', 'genpairs', 'fudgeLJ', 'fudgeQQ'])

BondedTypes = collections.namedtuple('BondedTypes',
                                     ['bonds', 'angles', 'dihedrals', 'impropers', 'all_dihedrals', 'nrexcl', 'HH14',
                                      'RemoveDih'])


class GMXFFParser(ForceField):
    defines: Dict[str, str]
    ifdefs: List[Tuple[str, bool]]
    currentsection: Optional[str] = None
    defaults: DefaultsType = None
    bondedtypes: Optional[BondedTypes] = None
    ffdir: str

    def __init__(self, ffdir: str, defines: List[str]):
        super().__init__()
        self.defines = {}
        self.ifdefs = []
        for macro in defines:
            self.defines[macro] = ''
        self.ffdir = ffdir
        self.readITP(os.path.join(ffdir, 'forcefield.itp'))
        for rtpfile in [os.path.join(ffdir, fn) for fn in os.listdir(ffdir) if fn.endswith('.rtp')]:
            self.readRTP(rtpfile)
        for hdbfile in [os.path.join(ffdir, fn) for fn in os.listdir(ffdir) if fn.endswith('.hdb')]:
            self.readHDB(hdbfile)

    def readHDB(self, filename: str):
        """Read a hydrogen addition database file"""
        with open(filename, 'rt') as f:
            logger.info(f'Reading HDB file {filename}')
            remainder = ''
            residuename = None
            requiredlines = 0
            for i, line in enumerate(f, start=1):
                code, comment, remainder = splitline(remainder + line, ';')
                if not code:
                    continue
                elif (residuename is None) or (requiredlines <= 0):
                    if not (m := re.match(fr'^(?P<resn>{RE_ATOMID})\s+(?P<count>\d+)$', code)):
                        raise ValueError(f'Invalid residue line #{i} in HDB file: {line}')
                    residuename = m['resn']
                    requiredlines = int(m['count'])
                    try:
                        self.rtp[residuename].hydrogenadditionrules = []
                    except ValueError:
                        logger.warning(f'Residue {residuename} does not exist in rtp files but hdb entry found.')
                else:
                    if not (m := re.match(fr'(?P<count>\d+)\s+(?P<rule>\d+)\s+(?P<hname>{RE_ATOMID})((?P<controlatoms>\s+{RE_ATOMID})+)', code)):
                        raise ValueError(f'Invalid hydrogen addition line #{i} in HDB file: {line}')
                    requiredlines -= 1
                    if residuename not in self.rtp:
                        continue
                    self.rtp[residuename].hydrogenadditionrules.append(
                        AtomAdditionRule(
                            int(m['count']), int(m['rule']), m['hname'], tuple(m['controlatoms'].split())))

    def readITP(self, filename):
        logger.info(f'Reading {filename} (cwd={os.getcwd()})')
        oldpwd = os.getcwd()
        try:
            with open(filename, 'rt') as f:
                os.chdir(os.path.split(os.path.abspath(filename))[0])
                remainder = ''
                for i, line in enumerate(f, start=1):
                    code, comment, remainder = splitline(remainder + line, ';', '\\')
                    if not code:
                        # skip empty or pure comment lines
                        continue
                    elif (m := re.match(fr'#ifdef\s+(?P<macro>{RE_MACRO})', code)) is not None:
                        self.ifdefs.append((m['macro'], True))
                    elif (m := re.match(fr'#ifndef\s+(?P<macro>{RE_MACRO})', code)) is not None:
                        self.ifdefs.append((m['macro'], False))
                    elif code == '#else':
                        self.ifdefs[-1] = self.ifdefs[-1][0], not self.ifdefs[-1][1]
                    elif code == '#endif':
                        del self.ifdefs[-1]
                    elif not self.check_ifdefs():
                        # skip this line: some #ifdef clauses are not satisfied
                        continue
                    elif (m := re.match(fr'#define\s+(?P<macro>{RE_MACRO})(\s+(?P<value>.*))?', code)) is not None:
                        self.defines[m['macro']] = m['value'] if m['value'] is not None else ''
                    elif (m := re.match(fr'#undef\s+(?P<macro>{RE_MACRO})', code)) is not None:
                        # check if we have this macro defined
                        del self.defines[m["macro"]]
                    elif (m := re.match(fr'#include\s+"(?P<filename>[-a-zA-Z0-9._]+)"', code)) is not None:
                        # #include "file"
                        self.readITP(m['filename'])
                    elif (m := re.match(fr'#include\s+<(?P<filename>[-a-zA-Z0-9._]+)>', code)) is not None:
                        # #include <file>
                        self.readITP(m['filename'])
                    # at this point all preprocessor directives were handled successfully.
                    elif (code[0] == '[') and (code[-1] == ']'):
                        # this is a section label
                        self.currentsection = code[1:-1].strip()
                    elif (self.currentsection == 'atomtypes') and (
                            (m := re.match(
                                fr'(?P<atomtype>{RE_ATOMID})\s+'
                                fr'(?P<atnum>\d+)\s+'
                                fr'(?P<mass>{RE_FLOAT})\s+'
                                fr'(?P<charge>{RE_FLOAT})\s+'
                                fr'(?P<ptype>[AD])\s+'
                                fr'(?P<sigma>{RE_FLOAT})\s+'
                                fr'(?P<epsilon>{RE_FLOAT})'
                                , code)) is not None):
                        self.prminfo.atomtypes.append(AtomType(
                            m['atomtype'],
                            -1,
                            float(m['mass']),
                            int(m['atnum']),
                            comment,
                            filename
                        ))
                        # ToDo: charge!
                    elif (self.currentsection == 'pairtypes') and (
                            (m := re.match(
                                fr'(?P<at1>{RE_ATOMID})\s+'
                                fr'(?P<at2>{RE_ATOMID})\s+'
                                fr'(?P<functype>\d+)\s+'
                                fr'(?P<sigma>{RE_FLOAT})\s+'
                                fr'(?P<epsilon>{RE_FLOAT})', code)) is not None):
                        self.prminfo.pairtypes.append(PairType(
                            (m['at1'], m['at2']),
                            int(m['functype']),
                            float(m['sigma']),
                            float(m['epsilon']), comment, filename))
                    elif (self.currentsection == 'defaults') and (
                            (m := re.match(
                                fr'(?P<nbfunc>\d+)\s+(?P<combrule>\d+)\s+(?P<genpairs>yes|no)\s+'
                                fr'(?P<fudgeLJ>{RE_FLOAT})\s+(?P<fudgeQQ>{RE_FLOAT})', code)) is not None):
                        self.defaults = DefaultsType(
                            int(m['nbfunc']),
                            int(m['combrule']),
                            m['genpairs'] == 'yes',
                            float(m['fudgeLJ']),
                            float(m['fudgeQQ'])
                        )
                    elif (self.currentsection == 'bondtypes') and (
                            (m := re.match(
                                fr'(?P<at1>{RE_ATOMID})\s+'
                                fr'(?P<at2>{RE_ATOMID})\s+'
                                fr'(?P<functype>\d+)\s+'
                                fr'(?P<b0>{RE_FLOAT})\s+'
                                fr'(?P<kb>{RE_FLOAT})',
                                code
                            )) is not None):
                        self.prminfo.bondtypes.append(BondType(
                            (m['at1'], m['at2']),
                            int(m['functype']),
                            float(m['kb']),
                            float(m['b0']),
                            comment, filename))
                    elif (self.currentsection == 'constrainttypes') and (
                            (m := re.match(
                                fr'(?P<at1>{RE_ATOMID})\s+'
                                fr'(?P<at2>{RE_ATOMID})\s+'
                                fr'(?P<functype>\d+)\s+'
                                fr'(?P<d0>{RE_FLOAT})',
                                code
                            )) is not None):
                        self.prminfo.constrainttypes.append(ConstraintType(
                            (m['at1'], m['at2']),
                            int(m['functype']),
                            float(m['d0']),
                            comment, filename))
                    elif (self.currentsection == 'angletypes') and (
                            (m := re.match(
                                fr'(?P<at1>{RE_ATOMID})\s+'
                                fr'(?P<at2>{RE_ATOMID})\s+'
                                fr'(?P<at3>{RE_ATOMID})\s+'
                                fr'(?P<functype>\d+)\s+'
                                fr'(?P<theta0>{RE_FLOAT})\s+'
                                fr'(?P<ktheta>{RE_FLOAT})\s+'
                                fr'(?P<ub0>{RE_FLOAT})\s+'
                                fr'(?P<kub>{RE_FLOAT})',
                                code
                            )) is not None):
                        self.prminfo.angletypes.append(AngleType(
                            (m['at1'], m['at2'], m['at3']),
                            int(m['functype']),
                            float(m['ktheta']),
                            float(m['theta0']),
                            float(m['kub']),
                            float(m['ub0']),
                            comment, filename))
                    elif (self.currentsection == 'dihedraltypes') and (
                            (m := re.match(
                                fr'(?P<at1>{RE_ATOMID})\s+'
                                fr'(?P<at2>{RE_ATOMID})\s+'
                                fr'(?P<at3>{RE_ATOMID})\s+'
                                fr'(?P<at4>{RE_ATOMID})\s+'
                                fr'(?P<functype>\d+)\s+'
                                fr'(?P<phi0>{RE_FLOAT})\s+'
                                fr'(?P<kphi>{RE_FLOAT})'
                                fr'(\s+(?P<n>\d+))?',
                                code
                            )) is not None):
                        if int(m['functype']) == 2:
                            # this is an improper dihedral type
                            self.prminfo.impropertypes.append(ImproperType(
                                (m['at1'], m['at2'], m['at3'], m['at4']),
                                int(m['functype']),
                                float(m['kphi']),
                                float(m['phi0']), comment, filename))
                        else:
                            self.prminfo.dihedraltypes.append(DihedralType(
                                (m['at1'], m['at2'], m['at3'], m['at4']),
                                int(m['functype']),
                                float(m['kphi']), int(m['n']),
                                float(m['phi0']), comment, filename))
                    elif (self.currentsection == 'cmaptypes') and (
                            (m := re.match(
                                fr'(?P<at1>{RE_ATOMID})\s+'
                                fr'(?P<at2>{RE_ATOMID})\s+'
                                fr'(?P<at3>{RE_ATOMID})\s+'
                                fr'(?P<at4>{RE_ATOMID})\s+'
                                fr'(?P<at5>{RE_ATOMID})\s+'
                                fr'(?P<functype>\d+)\s+'
                                fr'(?P<count1>\d+)\s+'
                                fr'(?P<count2>\d+)'
                                fr'(?P<data>(\s+{RE_FLOAT})+)',
                                code)) is not None):
                        if int(m['count1']) != int(m['count2']):
                            raise ValueError('Not supported CMAP type with count1 != count2')
                        data = tuple([float(x) for x in m['data'].split()])
                        if len(data) != int(m['count1']) ** 2:
                            raise ValueError(f'CMAP data length mismatch: expected '
                                             f'{m["count1"]}×{m["count2"]}={int(m["count1"]) * int(m["count2"])}, '
                                             f'got {len(data)}.')
                        self.prminfo.cmaptypes.append(CmapType(
                            (m['at1'], m['at2'], m['at3'], m['at4'], m['at5']),
                            int(m['functype']),
                            int(m['count1']),
                            data, comment, filename))
                    elif (self.currentsection == 'nonbond_params') and (
                            (m := re.match(
                                fr'(?P<at1>{RE_ATOMID})\s+'
                                fr'(?P<at2>{RE_ATOMID})\s+'
                                fr'(?P<functype>\d+)\s+'
                                fr'(?P<sigma>{RE_FLOAT})\s+'
                                fr'(?P<epsilon>{RE_FLOAT})',
                                code
                            )) is not None):
                        self.prminfo.nbfixtypes.append(NbfixType(
                            (m['at1'], m['at2']),
                            int(m['functype']),
                            float(m['sigma']),
                            float(m['epsilon']),
                            comment, filename))
                    elif (self.currentsection == 'implicit_genborn_params'):
                        # skip this line: implicit solvent parameters are unsupported since GMX 2019
                        continue
                    else:
                        raise ValueError(f'Unknown line in section {self.currentsection}: {line}')
        finally:
            os.chdir(oldpwd)

    def readRTP(self, filename: str):
        logger.info(f'Reading {filename} (cwd={os.getcwd()})')
        moleculeclass = os.path.splitext(os.path.split(filename)[-1])[0]
        with open(filename, 'rt') as f:
            sectionname = None
            currentresidue = None
            for i, line in enumerate(f, start=1):
                code, comment, remainder = splitline(line, ';', None)
                if not code:
                    continue
                elif (code[0] == '[') and (code[-1] == ']'):
                    sectionname = code[1:-1].strip()
                    if sectionname not in ['bondedtypes', 'atoms', 'bonds', 'angles', 'dihedrals', 'cmap', 'impropers']:
                        currentresidue = ResidueTopology(sectionname, 0.0, moleculeclass, filename, i, '')
                        self.rtp.append(currentresidue)
                elif sectionname == 'bondedtypes':
                    bondedtypes = BondedTypes(*[int(x) for x in code.split()])
                    if self.bondedtypes is None:
                        self.bondedtypes = bondedtypes
                    elif self.bondedtypes != bondedtypes:
                        raise ValueError('[ bondedtypes ] with different parameters than previously read.')
                elif (sectionname == 'atoms') and ((m := re.match(
                        fr'(?P<name>{RE_ATOMID})\s+(?P<atomtype>{RE_ATOMID})\s+'
                        fr'(?P<pcharge>{RE_FLOAT})\s+(?P<cgroup>{RE_INT})', code)) is not None):
                    currentresidue.atoms.append(Atom(
                        m['name'], m['atomtype'], float(m['pcharge']), int(m['cgroup']), comment))
                elif (sectionname == 'bonds') and ((m := re.match(
                        fr'(?P<at1>[+-]?{RE_ATOMID})\s+(?P<at2>[+-]?{RE_ATOMID})', code)) is not None):
                    currentresidue.bonds.append(Bond(
                        (m['at1'], m['at2']), 0, comment))
                elif (sectionname == 'angles') and ((m := re.match(
                        fr'(?P<at1>[+-]?{RE_ATOMID})\s+(?P<at2>[+-]?{RE_ATOMID})\s+(?P<at3>[+-]?{RE_ATOMID})',
                        code)) is not None):
                    currentresidue.angles.append(Angle(
                        (m['at1'], m['at2'], m['at3']), comment))
                elif (sectionname == 'dihedrals') and ((m := re.match(
                        fr'(?P<at1>[+-]?{RE_ATOMID})\s+(?P<at2>[+-]?{RE_ATOMID})\s+(?P<at3>[+-]?{RE_ATOMID})\s+(?P<at4>[+-]?{RE_ATOMID})',
                        code)) is not None):
                    currentresidue.dihedrals.append(Dihedral(
                        (m['at1'], m['at2'], m['at3'], m['at4']), comment))
                elif (sectionname == 'impropers') and ((m := re.match(
                        fr'(?P<at1>[+-]?{RE_ATOMID})\s+(?P<at2>[+-]?{RE_ATOMID})\s+(?P<at3>[+-]?{RE_ATOMID})\s+(?P<at4>[+-]?{RE_ATOMID})',
                        code)) is not None):
                    currentresidue.impropers.append(Improper(
                        (m['at1'], m['at2'], m['at3'], m['at4']), comment))
                elif (sectionname == 'cmap') and ((m := re.match(
                        fr'(?P<at1>[+-]?{RE_ATOMID})\s+(?P<at2>[+-]?{RE_ATOMID})\s+(?P<at3>[+-]?{RE_ATOMID})\s+(?P<at4>[+-]?{RE_ATOMID})\s+(?P<at5>[+-]?{RE_ATOMID})',
                        code)) is not None):
                    currentresidue.cmaps.append(Cmap(
                        (m['at1'], m['at2'], m['at3'], m['at4'], m['at5']), comment))
                else:
                    raise ValueError(f'Unknown line in section {sectionname}, residue {self.rtp[-1].name}: {line}')

    def check_ifdefs(self) -> bool:
        for macro, neededstate in self.ifdefs:
            if (not neededstate) and (macro in self.defines):
                # macro is defined but we are after an #ifndef or an #else
                return False
            elif neededstate and (macro not in self.defines):
                return False
        else:
            # all #ifdef clauses satisfied
            return True
