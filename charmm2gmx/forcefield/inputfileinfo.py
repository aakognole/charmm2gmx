# coding=utf-8
"""Input file specifications."""
import enum
from typing import List, Tuple

from ..utils.clashpolicy import ParameterClashPolicy


class InputFileType(enum.Enum):
    CHARMM_PRM = 'prm'
    CHARMM_STR = 'str'
    CHARMM_RTF = 'rtf'
    GROMACS_ITP = 'itp'
    GROMACS_RTP = 'rtp'


class InputFileInfo:
    """Information on the input file

    :ivar str path: path to the file
    :ivar InputFileType filetype: the type of the input file, i.e. how to handle it
    :ivar ParameterClashPolicy clashpolicy: how to handle redefinitions
    :ivar atomrenamelist:
    """
    path: str
    filetype: InputFileType
    clashpolicy: ParameterClashPolicy
    atomtyperenamelist: List[Tuple[str, str]]
    patching: List[Tuple[str, str, str]]
    discardresidues: List[str]
    overrideresidues: List[str]
    renameresidues: List[Tuple[str, str]]
    copyresidues: List[Tuple[str, str]]
    citekeys: List[Tuple[str, str]]
    moleculeclass: str
    terminals: List[Tuple[str, str, str]]
    atomrenamelist: List[Tuple[str, str, str]]
    cter_delete: List[str]
    nter_delete: List[str]

    def __init__(self, path: str, filetype: InputFileType, clashpolicy: ParameterClashPolicy, moleculeclass: str):
        self.path = path
        self.filetype = filetype
        self.clashpolicy = clashpolicy
        self.moleculeclass = moleculeclass
        self.atomtyperenamelist = []
        self.patching = []
        self.discardresidues = []
        self.overrideresidues = []
        self.renameresidues = []
        self.citekeys = []
        self.terminals = []
        self.copyresidues = []
        self.atomrenamelist = []
        self.cter_delete = []
        self.nter_delete = []
