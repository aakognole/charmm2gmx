# coding=utf-8
"""Atom type class"""

import logging
from typing import Optional

from .comparableinteractiontype import ComparableInteractionType
from ..utils.periodic import PERIODIC

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class AtomType(ComparableInteractionType):
    """Representation of an atom type in a force field

    :ivar int numeric: numeric atom type ID, mostly unused
    :ivar str name: atom type name
    :ivar float mass: atom mass in a.m.u.
    :ivar int atnum: atomic number ("Z")
    """
    numeric: int
    mass: float
    atnum: int

    _must_equal = ['name', 'mass', 'atnum']

    def __init__(self, name: str, numeric: int, mass: float, atnum: Optional[int], comment: str, filename: str):
        """Create a new atom type

        :param name: atom type name
        :type name: str
        :param numeric: numeric atom type ID, mostly unused
        :type numeric: int
        :param mass: atom mass in a.m.u.
        :type mass: float
        :param atnum: atomic number ("Z"). If None, autodetected based on the mass.
        :type atnum: int or None
        :param comment: textual description
        :type comment: str
        :param filename: name of the file where this is defined
        :type filename: str
        """
        super().__init__((name,), 0, comment, filename)
        self.numeric = int(numeric)
        self.mass = float(mass)
        if atnum is None:
            if name == 'LPH':  # special case: lone pair
                atnum = 1
            else:
                try:
                    atnum = [element.atnum for element in PERIODIC if abs(element.mass - mass) < 0.1][0]
                except IndexError:
                    logger.warning(f'Warning: cannot find atom number for atom type {name}, mass {mass} a.m.u.')
                    atnum = 0  # dummy atom
        self.atnum = atnum

    @property
    def name(self) -> str:
        """The atom type name"""
        return self.atomtypes[0]

    def __repr__(self) -> str:
        return f'AtomType({self.name}, m={self.mass} amu, Z={self.atnum}, comment={self.comment})'

    def isEquivalent(self, other: "AtomType") -> bool:
        return (self.mass == other.mass) and (self.atnum == other.atnum)
