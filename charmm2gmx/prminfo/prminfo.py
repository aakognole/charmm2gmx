# coding=utf-8
"""Main class for parameter information"""

import logging
from typing import List, Final, TextIO, Tuple

from .atomtype import AtomType
from .interactiontypes import BondType, AngleType, ImproperType, DihedralType, CmapType, NonbondedType, NbfixType, \
    PairType, ConstraintType
from .comparableinteractiontype import ComparableInteractionType
from .validatinginteractionlist import ValidatingInteractionList
from ..utils.clashpolicy import ParameterClashPolicy
from ..utils import GMXFileHeader
from ..watermodels import WATERMODELS

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class PRMInfo:
    """Parameter information loaded from CHARMM .prm files or GROMACS forcefield.itp

    :ivar ValidatingInteractionList atomtypes: list of atom types
    :ivar ValidatingInteractionList bondtypes: list of bond types
    :ivar ValidatingInteractionList angletypes: list of angle types
    :ivar ValidatingInteractionList impropertypes: list of improper dihedral types
    :ivar ValidatingInteractionList dihedraltypes: list of proper dihedral types
    :ivar ValidatingInteractionList cmaptypes: list of cmap interaction types
    :ivar ValidatingInteractionList nonbondedtypes: list of nonbonded types
    :ivar ValidatingInteractionList nbfixtypes: list of nbfix types
    :ivar ValidatingInteractionList pairtypes: list of 1-4 LJ interaction types
    :ivar ValidatingInteractionList constrainttypes: list of constraint types
    :ivar ParameterClashPolicy clashpolicy: what to do when an already existing parameter is to be added
    :cvar list[str] interactionlists: list of interaction list names
    """
    atomtypes: ValidatingInteractionList
    bondtypes: ValidatingInteractionList
    angletypes: ValidatingInteractionList
    impropertypes: ValidatingInteractionList
    dihedraltypes: ValidatingInteractionList
    cmaptypes: ValidatingInteractionList
    nonbondedtypes: ValidatingInteractionList
    nbfixtypes: ValidatingInteractionList
    pairtypes: ValidatingInteractionList
    constrainttypes: ValidatingInteractionList

    clashpolicy: ParameterClashPolicy

    interactionlists: Final[List[str]] = ['atomtypes', 'bondtypes', 'angletypes', 'dihedraltypes', 'impropertypes',
                                          'cmaptypes', 'nonbondedtypes', 'nbfixtypes', 'pairtypes', 'constrainttypes']

    def __init__(self, clashpolicy: ParameterClashPolicy = ParameterClashPolicy.ERROR):
        self.clashpolicy = clashpolicy
        self.atomtypes = ValidatingInteractionList(AtomType)
        self.bondtypes = ValidatingInteractionList(BondType)
        self.angletypes = ValidatingInteractionList(AngleType)
        self.dihedraltypes = ValidatingInteractionList(DihedralType)
        self.impropertypes = ValidatingInteractionList(ImproperType)
        self.cmaptypes = ValidatingInteractionList(CmapType)
        self.nonbondedtypes = ValidatingInteractionList(NonbondedType)
        self.nbfixtypes = ValidatingInteractionList(NbfixType)
        self.constrainttypes = ValidatingInteractionList(ConstraintType)
        self.pairtypes = ValidatingInteractionList(PairType)

    def __iadd__(self, other: "PRMInfo") -> "PRMInfo":
        # add new items for each list while also checking clashes with already present items

        for listname in self.interactionlists:
            originallist: ValidatingInteractionList = getattr(self, listname)
            newlist: ValidatingInteractionList = getattr(other, listname)

            for item in newlist:
                # for each new item, try to find clashes with already present items.
                # Clashing items:
                #    - have the same atomtypes (watch out for AB -> BA reversions)
                #    - some paramters are the same (e.g. "n" for dihedrals)
                #    - some parameters are different (e.g. Kb for bonds)
                compatible = [item_ for item_ in originallist if item.is_compatible(item_)]
                clashing = [item_ for item_ in originallist if item.does_clash(item_)]
                if clashing:
                    message = f'Clash in list {listname} for atom type(s) {", ".join(item.atomtypes)}.\n' + \
                              f'    Values on file: {clashing[0].print_values()} (from {clashing[0].filename}).\n' + \
                              f'    Current values: {item.print_values()} (from {item.filename})'
                    if self.clashpolicy == ParameterClashPolicy.ERROR:
                        raise ValueError(message)
                    elif self.clashpolicy == ParameterClashPolicy.KEEP_NEWER:
                        logger.warning(f'{message}.\nOverriding old values with new ones.')
                        for c in clashing:
                            originallist.remove(c)
                        originallist.append(item)
                    elif self.clashpolicy == ParameterClashPolicy.KEEP_OLDER:
                        logger.warning(f'{message}.\nKeeping old values, discarding new ones.')
                elif compatible:
                    # repeated parameter type with the same values
                    pass
                else:
                    # new parameter type, store it.
                    originallist.append(item)
        return self

    def filenames(self) -> List[str]:
        """Return a list of file names from where the parameters were loaded."""
        files = set()
        for lis in [getattr(self, listname) for listname in self.interactionlists]:
            try:
                files = files.union([item.filename for item in lis])
            except KeyError:
                pass
        return sorted(files)

    def writeGMXHeader(self, f: TextIO) -> List[str]:
        """Write a descriptive header to a GROMACS FF file

        :param f: open file to write the header to.
        :type f: open file
        :return: list of source file names used
        :rtype: list of str
        """
        f.write(GMXFileHeader())
        f.write('; The following files were used: \n')
        inputfiles = self.filenames()
        for fn in sorted(inputfiles):
            f.write(f'; {fn}\n')
        f.write(';\n\n')
        return inputfiles

    def writeAtomtypes(self, filename: str):
        """Write the atomtypes.atp file

        :param filename: name of the file to write
        :type filename: str
        """
        with open(filename, 'wt') as f:
            inputfiles = self.writeGMXHeader(f)
            for fn in sorted(inputfiles):
                f.write(f'\n; from file {fn}\n')
                for atomtype in sorted([at for at in self.atomtypes if at.filename == fn], key=lambda x: x.name):
                    assert isinstance(atomtype, AtomType)
                    f.write(f"{atomtype.name:>6s} {atomtype.mass:>12.5f} ; {atomtype.comment}\n")
            # kludge: non-CHARMM parameters
            f.write('\n;The following are not from CHARMM:\n')
            f.write('   MGA      0.00000 ; Updated Mg2+ parameters by Allner et. al. (DOI: 10.1021/ct3000734)\n')

            f.write('\n;Atom types for various water models\n')
            for name, water in WATERMODELS.items():
                for atomtype in water.atomtypes:
                    if not [a for a in self.atomtypes if a.name == atomtype.name]:
                        f.write(f"{atomtype.name:>6s} {atomtype.mass:>12.5f} ; {atomtype.comment}\n")

            f.write('\n; Atom types for virtual sites')
            f.write('  MNH3      0.00000 ; Vsite (Rigid tetrahedral NH3 group)\n')
            f.write('  MNH2      0.00000 ; Vsite (Rigid NH2 group)\n')
            f.write('  MCH3      0.00000 ; Vsite (Rigid CH3 group)\n')
            f.write(' MCH3S      0.00000 ; Vsite\n')
            f.write('    MW      0.00000 ; Vsite\n')

    def writeNonbonded(self, filename: str):
        """Write ffnonbonded.itp

        :param filename: file name
        :type filename: str
        """
        with open(filename, 'wt') as f:
            inputfiles = self.writeGMXHeader(f)
            f.write('[ atomtypes ]\n')
            for fn in sorted(inputfiles):
                f.write(f'\n; from file {fn}\n')
                f.write('; type atnum         mass   charge ptype           sigma  epsilon\n')
                for at in sorted([at for at in self.atomtypes if at.filename == fn],
                                 key=lambda at: (at.atnum, at.name)):
                    assert isinstance(at, AtomType)
                    try:
                        nbdata = [nb for nb in self.nonbondedtypes if nb.atomtype == at.name][0]
                    except IndexError:
                        raise KeyError(f'Missing nonbonded parameters for atom type "{at.name}".')
                    assert isinstance(nbdata, NonbondedType)

                    # if this atom type is used in a water model, and is an oxygen or a hydrogen,
                    # replicate it in an #ifdef HEAVY_H clause
                    watermodel = [w for w in WATERMODELS.values() if w.hasAtomType(at.name)]

                    if watermodel and (at.atnum==1):
                        wateratomtype = watermodel[0].getAtomType(at.name)
                        # this atom type is a water hydrogen: add HEAVY_H #ifdefs
                        f.write('#ifdef HEAVY_H\n')
                        f.write(
                            f'{at.name:>6s} {at.atnum:>5d} {at.mass*4:>12.6f} {wateratomtype.pcharge:>8.3f}     '
                            f'A {nbdata.sigma:>15.12f} {nbdata.epsilon:>10.7f}  ; {at.comment}\n')
                        f.write('#else\n')
                        f.write(
                            f'{at.name:>6s} {at.atnum:>5d} {at.mass:>12.6f} {wateratomtype.pcharge:>8.3f}     '
                            f'A {nbdata.sigma:>15.12f} {nbdata.epsilon:>10.7f}  ; {at.comment}\n')
                        f.write('#endif\n')
                    elif watermodel and (at.atnum == 8):
                        wateratomtype = watermodel[0].getAtomType(at.name)
                        # this is a water oxygen: add HEAVY_H #ifdefs
                        hydrogentype = [at_ for at_ in watermodel[0].atomtypes if at_.atnum == 1][0]
                        f.write('#ifdef HEAVY_H\n')
                        f.write(
                            f'{at.name:>6s} {at.atnum:>5d} {at.mass -8*hydrogentype.mass:>12.6f} '
                            f'{wateratomtype.pcharge:>8.3f}     '
                            f'A {nbdata.sigma:>15.12f} {nbdata.epsilon:>10.7f}  ; {at.comment}\n')
                        f.write('#else\n')
                        f.write(
                            f'{at.name:>6s} {at.atnum:>5d} {at.mass:>12.6f} {wateratomtype.pcharge:>8.3f}     '
                            f'A {nbdata.sigma:>15.12f} {nbdata.epsilon:>10.7f}  ; {at.comment}\n')
                        f.write('#endif\n')
                    elif watermodel and (at.atnum == 0):
                        # dummy atom / virtual site, e.g. for TIP4P or TIP5P
                        wateratomtype = watermodel[0].getAtomType(at.name)
                        f.write(
                            f'{at.name:>6s} {at.atnum:>5d} {at.mass:>12.6f} {wateratomtype.pcharge:>8.3f}     '
                            f'D {nbdata.sigma:>15.12f} {nbdata.epsilon:>10.7f}  ; {at.comment}\n')
                    else:
                        # the atom does not belong to a water model
                        f.write(
                            f'{at.name:>6s} {at.atnum:>5d} {at.mass:>12.6f} {0:>8.3f}     '
                            f'{"A" if at.atnum > 0 else "D"} {nbdata.sigma:>15.12f} '
                            f'{nbdata.epsilon:>10.7f}  ; {at.comment}\n')

            f.write('\n; The following atom types are NOT part of the CHARMM distribution.\n')
            f.write('; Atomtypes for additional water models\n')
            for wm in WATERMODELS.values():
                if any([bool([a for a in self.atomtypes if a.name == at.name]) for at in wm.atomtypes]):
                    if not all([bool([a for a in self.atomtypes if a.name == at.name]) for at in wm.atomtypes]):
                        raise ValueError(f'Some but not all atom types used by water model {wm.name} are found in the '
                                         f'CHARMM parameter set!')
                    else:
                        # these atom types have already been written.
                        continue
                f.write(f'; {wm.description}\n')
                hydrogen = [at for at in wm.atomtypes if at.atnum == 1][0]
                oxygen = [at for at in wm.atomtypes if at.atnum == 8][0]
                dummies = [at for at in wm.atomtypes if at.atnum == 0]
                f.write('#ifdef HEAVY_H\n')
                f.write(
                    f'{hydrogen.name:>6s} {hydrogen.atnum:>5d} {4*hydrogen.mass:>12.6f} {hydrogen.pcharge:>8.3f}     '
                    f'A {hydrogen.sigma:>15.12f} {hydrogen.epsilon:>10.7f}  ; {hydrogen.comment}\n')
                f.write(
                    f'{oxygen.name:>6s} {oxygen.atnum:>5d} {oxygen.mass - 8*hydrogen.mass:>12.6f} {oxygen.pcharge:>8.3f}     '
                    f'A {oxygen.sigma:>15.12f} {oxygen.epsilon:>10.7f}  ; {oxygen.comment}\n')
                f.write('#else\n')
                f.write(
                    f'{hydrogen.name:>6s} {hydrogen.atnum:>5d} {hydrogen.mass:>12.6f} {hydrogen.pcharge:>8.3f}     '
                    f'A {hydrogen.sigma:>15.12f} {hydrogen.epsilon:>10.7f}  ; {hydrogen.comment}\n')
                f.write(
                    f'{oxygen.name:>6s} {oxygen.atnum:>5d} {oxygen.mass:>12.6f} {oxygen.pcharge:>8.3f}     '
                    f'A {oxygen.sigma:>15.12f} {oxygen.epsilon:>10.7f}  ; {oxygen.comment}\n')
                f.write('#endif\n')
                for dummy in dummies:
                    f.write(
                        f'{dummy.name:>6s} {dummy.atnum:>5d} {dummy.mass:>12.6f} {dummy.pcharge:>8.3f}     '
                        f'D {dummy.sigma:>15.12f} {dummy.epsilon:>10.7f}  ; {dummy.comment}\n')

            f.write('; special dummy-type particles\n')
            f.write(f'  MNH3     0     0.000000     0.000     D  0.000000000000  0.00000\n')
            f.write(f'  MNH2     0     0.000000     0.000     D  0.000000000000  0.00000\n')
            f.write(f'  MCH3     0     0.000000     0.000     D  0.000000000000  0.00000\n')
            f.write(f' MCH3S     0     0.000000     0.000     D  0.000000000000  0.00000\n')
            f.write(f'    MW     0     0.000000     0.000     D  0.000000000000  0.00000\n')

            f.write('\n; Updated Mg2+ parameters by Allner et. al. (DOI: 10.1021/ct3000734)\n')
            f.write(f'   MGA    12    24.305000     0.000     A  0.277000000000  0.0123428\n')

            # NB: we do not need pairtypes for water atom types: they won't be found in 1-4 interactions!
            f.write('\n[ pairtypes ]\n')
            for pt in self.pairtypes:
                assert isinstance(pt, PairType)
                f.write(
                    f'{pt.atomtypes[0]:>6s} {pt.atomtypes[1]:>6s} {pt.functype:>5d} '
                    f'{pt.sigma:>15.12f} {pt.epsilon:>15.12f}\n')

    def generatePairTypes(self) -> None:
        """Generate pair types from CHARMM Nonbonded types"""
        nbsorted = sorted(self.nonbondedtypes, key=lambda nb: nb.atomtype)
        for i, nb1 in enumerate(nbsorted):
            assert isinstance(nb1, NonbondedType)
            # this atom type has special LJ parameters for 1-4 interactions
            for j, nb2 in enumerate(nbsorted[i:]):
                assert isinstance(nb2, NonbondedType)
                if (nb1.atomtype, nb2.atomtype) in self.nbfixtypes:
                    logger.info(f'Using NBFIX between atom types {nb1.atomtype} and {nb2.atomtype} to generate 1-4 pairtype')
                    atomtypes_sorted = (nb1.atomtype, nb2.atomtype) if (nb1.atomtype < nb2.atomtype) else (nb2.atomtype, nb1.atomtype)
                    nbfixes = self.nbfixtypes[atomtypes_sorted]
                    assert len(nbfixes) == 1
                    nbfix: NbfixType = nbfixes[0]
                    epsilon = nbfix.epsilon
                    sigma = nbfix.sigma
                elif (not nb1.hasLJ14Params()) and (not nb2.hasLJ14Params()):
                    # these pairs will be auto-generated
                    continue
                else:
                    # one of the atom types has dedicated parameters for 1-4 LJ interaction and there is no NBFIX
                    # defined between these atom types
                    epsilon = (getattr(nb1, 'epsilon_14' if nb1.hasLJ14Params() else 'epsilon') *
                               getattr(nb2, 'epsilon_14' if nb2.hasLJ14Params() else 'epsilon')) ** 0.5
                    sigma = 0.5 * (getattr(nb1, 'sigma_14' if nb1.hasLJ14Params() else 'sigma') +
                                   getattr(nb2, 'sigma_14' if nb2.hasLJ14Params() else 'sigma'))
                self.pairtypes.append(PairType((nb1.atomtype, nb2.atomtype), 1, epsilon, sigma, '', ''))

    def writeBonded(self, filename: str) -> None:
        """Write ffbonded.itp

        :param filename: file name
        :type filename: str
        """
        with open(filename, 'wt') as f:
            inputfiles = self.writeGMXHeader(f)
            f.write('[ bondtypes ]\n')
            for inputfile in sorted(inputfiles):
                f.write(f'\n; bond parameters from {inputfile}\n')
                f.write(';      i        j  func           b0           kb\n')
                for bt in [bt_ for bt_ in self.bondtypes if bt_.filename == inputfile]:
                    assert isinstance(bt, BondType)
                    f.write(
                        f'{bt.atomtypes[0]:>8s} {bt.atomtypes[1]:>8s} {bt.functype:>5d} {bt.b0:>12.8f} {bt.Kb:>12.2f} ; {bt.comment}\n')
            f.write('\n\n[ angletypes ]\n')
            for inputfile in sorted(inputfiles):
                f.write(f'\n; angle parameters from {inputfile}\n')
                f.write(';      i        j        k  func       theta0       ktheta          ub0          kub\n')
                for at in [at_ for at_ in self.angletypes if at_.filename == inputfile]:
                    assert isinstance(at, AngleType)
                    f.write(
                        f'{at.atomtypes[0]:>8s} {at.atomtypes[1]:>8s} {at.atomtypes[2]:>8s} {at.functype:>5d} {at.Theta0:>12.6f} {at.Ktheta:>12.6f} {at.S0:>12.8f} {at.Kub:12.2f} ; {at.comment}\n')

            f.write('\n\n[ dihedraltypes ]\n')
            for is_wildcard in [False, True]:
                for inputfile in sorted(inputfiles):
                    f.write(
                        f'\n; {"wildcard" if is_wildcard else "specific (non-wildcard)"} dihedral parameters from {inputfile}\n')
                    f.write(';      i        j        k        l  func         phi0         kphi  mult\n')
                    for dt in [dt_ for dt_ in self.dihedraltypes if dt_.filename == inputfile]:
                        assert isinstance(dt, DihedralType)
                        if (is_wildcard and ('X' in dt.atomtypes)) or ((not is_wildcard) and ('X' not in dt.atomtypes)):
                            f.write(
                                f'{dt.atomtypes[0]:>8s} {dt.atomtypes[1]:>8s} {dt.atomtypes[2]:>8s} {dt.atomtypes[3]:>8s} {dt.functype:>5d} {dt.delta:>12.6f} {dt.Kchi:>12.6f} {dt.n:>5d} ; {dt.comment}\n')

            f.write('\n\n; Include dihedral type definitions missing from upstream CHARMM FF.\n')
            f.write('#include "ffmissingdihedrals.itp"\n')

            f.write('\n\n[ dihedraltypes ]\n')
            for is_wildcard in [False, True]:
                for inputfile in sorted(inputfiles):
                    f.write(
                        f'\n; {"wildcard" if is_wildcard else "specific (non-wildcard)"} improper dihedral parameters from {inputfile}\n')
                    f.write(';      i        j        k        l  func         phi0         kphi\n')
                    for dt in [dt_ for dt_ in self.impropertypes if dt_.filename == inputfile]:
                        assert isinstance(dt, ImproperType)
                        if (is_wildcard and ('X' in dt.atomtypes)) or ((not is_wildcard) and ('X' not in dt.atomtypes)):
                            f.write(
                                f'{dt.atomtypes[0]:>8s} {dt.atomtypes[1]:>8s} {dt.atomtypes[2]:>8s} {dt.atomtypes[3]:>8s} {dt.functype:>5d} {dt.psi0:>12.6f} {dt.Kpsi:>12.6f}  ; {dt.comment}\n')

    def writeCMAP(self, filename: str) -> None:
        """Write cmap.itp

        :param filename: file name
        :type filename: str
        """
        with open(filename, 'wt') as f:
            inputfiles = self.writeGMXHeader(f)
            f.write('[ cmaptypes ]\n')
            for inputfile in sorted(inputfiles):
                f.write(f'\n; cmap parameters from {inputfile}\n')
                for cm in [cm_ for cm_ in self.cmaptypes if cm_.filename == inputfile]:
                    assert isinstance(cm, CmapType)
                    f.write(
                        f'\n{cm.atomtypes[0]} {cm.atomtypes[1]} {cm.atomtypes[2]} {cm.atomtypes[3]} {cm.atomtypes[4]} {cm.functype:d} {cm.count:d} {cm.count:d}\\\n')
                    for i in range(len(cm.data) // 10):
                        f.write(' '.join([f'{cm.data[i * 10 + j]:.8f}' for j in range(10)]) + '\\\n')
                    if i * 10 < len(cm.data):
                        # there are still some more left.
                        f.write(' '.join([
                            f'{cm.data[j]:.8f}'
                            for j in range((i + 1) * 10, len(cm.data))]) + '\n')

    def writeNbfix(self, filename: str) -> None:
        """Write nbfix.itp

        :param filename: file name
        :type filename: str
        """
        with open(filename, 'wt') as f:
            inputfiles = self.writeGMXHeader(f)
            f.write('[ nonbond_params ]\n')
            for inputfile in sorted(inputfiles):
                f.write(f'\n\n; NBFIX parameters from {inputfile}\n')
                f.write(';      i        j  func           sigma         epsilon\n')
                for nbf in [nbf_ for nbf_ in self.nbfixtypes if nbf_.filename == inputfile]:
                    assert isinstance(nbf, NbfixType)
                    f.write(
                        f'{nbf.atomtypes[0]:>8s} {nbf.atomtypes[1]:>8s} {nbf.functype:>5d} {nbf.sigma:>15.12f} {nbf.epsilon:>15.12f} ; {nbf.comment}\n')

    def validate(self) -> None:
        """Perform consistency check

        If everything goes well, this function returns nothing.

        :raise ValueError: if consistency errors found
        """
        empty = PRMInfo()
        empty += self

    def compare(self, other: "PRMInfo", warn_superfluous: bool = False) -> \
            Tuple[List[ComparableInteractionType],
                  List[Tuple[ComparableInteractionType, ComparableInteractionType]],
                  List[ComparableInteractionType]]:
        """Compare two `PRMInfo` instances

        This instance is considered as "new", the `other` is the "reference".

        The output is logged using the Python logging facility:
        - Parameters missing from this instance produce warning log messages
        - Parameters defined differently in both instances produce error log messages
        - Superfluous parameters (not present in `other`) produce warning messages, but only if `warn_superfluous` is
          True.

        Differences are also returned in lists

        :param other: the other parameter set
        :type other: :class:`PRMInfo` instance
        :param warn_superfluous: produce warnings if superfluous parameters found
        :type warn_superfluous: bool
        :return: missing, different, superfluous
        :rtype: list of :class:`ComparableInteractionType` instances,
                list of tuples of 2 :class:`ComparableInteractionType` instances,
                list of :class:`ComparableInteractionType` instances
        """
        missing = []
        different_all = []
        superfluous = []
        for listname in self.interactionlists:
            # make copies of the original lists: we will remove paired items. Sort them, to ease search.
            thislist = sorted(getattr(self, listname))
            otherlist = sorted(getattr(other, listname))
            logger.info(
                f'Comparing interaction list "{listname}" ({len(thislist)} entries in this, '
                f'{len(otherlist)} in the other FF)')
            mapping = []
            missingfromother = []
            missingfromthis = []
            while thislist or otherlist:
                # note that the lists are sorted
                if thislist and ((not otherlist) or (thislist[0] < otherlist[0])):
                    # there is no corresponding entry in the other list
                    missingfromother.append(thislist.pop(0))
                elif otherlist and ((not thislist) or (thislist[0] > otherlist[0])):
                    # there is no corresponding entry in this list
                    missingfromthis.append(otherlist.pop(0))
                else:
                    # grab the first elements from each list
                    fromthis = [thislist.pop(0)]
                    fromother = [otherlist.pop(0)]
                    for lis, items, refitem, ffindex in [
                        (thislist, fromthis, fromthis[0], 1),
                        (otherlist, fromother, fromother[0], 2)]:
                        while lis and (refitem.is_compatible(lis[0])):
                            items.append(lis.pop(0))
                        # find duplicates
                        if any([it.does_clash(refitem) for it in items]):
                            raise ValueError(f'Internal inconsistency of FF #{ffindex}: parameter {repr(refitem)} '
                                             f'has the following, clashing duplicates: '
                                             f'{", ".join([repr(it) for it in items])}')
                    mapping.append((fromthis, fromother))
                    # corresponding interaction types found: remove

            different = [(i1[0], i2[0]) for i1, i2 in mapping if i1[0].does_clash(i2[0])]
            if missingfromthis:
                logger.warning('  Missing from this:\n' + '\n'.join([f'   {repr(it)}' for it in missingfromthis]))
            if missingfromother and warn_superfluous:
                logger.warning('  Missing from other:\n' + '\n'.join([f'   {repr(it)}' for it in missingfromother]))
            if different:
                logger.error('  Different parameters:\n' + '\n'.join(
                    [f'    {repr(it[0])} <-> {repr(it[1])}' for it in different]))
            missing.extend(missingfromthis)
            superfluous.extend(missingfromother)
            different_all.extend(different)
        return missing, different_all, superfluous

    def renameAtomType(self, oldname: str, newname: str):
        """Rename an atom type in all interactions

        :param str oldname: original name
        :param str newname: new name
        """
        for listname in self.interactionlists:
            logger.info(f'Renaming atom type from {oldname} to {newname} in list {listname}')
            lis = getattr(self, listname)
            for item in lis:
                item.atomtypes = tuple([newname if a == oldname else a for a in item.atomtypes])
