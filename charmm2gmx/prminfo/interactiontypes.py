# coding=utf-8
"""Definitions of interaction types"""
from typing import Tuple, Optional, List

from .comparableinteractiontype import ComparableInteractionType
from ..utils.interactiontype import InteractionType


class BondType(ComparableInteractionType):
    """Bond type

    :ivar float Kb: force constant (kJ/mol/nm^2)
    :ivar float b0: rest length (nm)
    """
    interactiontype = InteractionType.Bond
    Kb: float
    b0: float
    _must_equal = ['Kb', 'b0']

    def __init__(self, atomtypes: Tuple[str, str], functype: int, Kb: float, b0: float, comment: str, filename: str):
        super().__init__(atomtypes, functype, comment, filename)
        self.Kb = float(Kb)
        self.b0 = float(b0)


class AngleType(ComparableInteractionType):
    """Angle type

    :ivar float Ktheta: force constant (kJ/mol/rad^2)
    :ivar float Theta0: equilibrium angle (deg)
    :ivar float Kub: Urey-Bradley force constant (kJ/mol/nm^2)
    :ivar float S0: equilibrium Urey-Bradley distance (nm)
    """
    interactiontype = InteractionType.Angle
    Ktheta: float
    Theta0: float
    Kub: float
    S0: float
    _must_equal = ['Ktheta', 'Theta0', 'Kub', 'S0']

    def __init__(self, atomtypes: Tuple[str, str, str], functype: int, Ktheta: float, Theta0: float, Kub: float,
                 S0: float,
                 comment: str, filename: str):
        super().__init__(atomtypes, functype, comment, filename)
        self.Ktheta = float(Ktheta)
        self.Theta0 = float(Theta0)
        self.Kub = float(Kub)
        self.S0 = float(S0)


class ImproperType(ComparableInteractionType):
    """Improper dihedral type

    :ivar float Kpsi: force constant (kJ/mol/rad^2)
    :ivar float psi0: equilibrium dihedral angle (deg)
    """
    interactiontype = InteractionType.Improper
    Kpsi: float
    psi0: float
    _must_equal = ['Kpsi', 'psi0']

    def __init__(self, atomtypes: Tuple[str, str, str, str], functype: int, Kpsi: float, psi0: float, comment: str,
                 filename: str):
        super().__init__(atomtypes, functype, comment, filename)
        self.Kpsi = float(Kpsi)
        self.psi0 = float(psi0)

    def isPlanar(self) -> bool:
        """Check if this dihedral interaction potential forces planarity of the participating atoms"""
        return (self.psi0 == 0) or (self.psi0 == 180)

    def atomtypes_match(self, atomtypes: Tuple[str, ...]) -> bool:
        """Check if the atom types of this interaction match with the required ones.

        When checking for match, atom type 'X' in self.atomtypes (not in the `atomtypes` parameter!) is a wildcard.
        Reverse order is also accepted.

        :param atomtypes: atom type names
        :type atomtypes: tuple of str
        :return: True if matches, False if not matches
        :rtype: bool
        """
        if super().atomtypes_match(atomtypes):
            return True
        elif self.atomtypes[1] == self.atomtypes[2] == 'X':
            # wildcard type, central atoms are X, side atoms should match
            return ((self.atomtypes[0] == atomtypes[0]) and (self.atomtypes[3] == atomtypes[3])) or \
                   ((self.atomtypes[0] == atomtypes[3]) and (self.atomtypes[3] == atomtypes[0]))
        else:
            return False

    def isWildcard(self) -> bool:
        """Check if this is a wildcard dihedral type, i.e. any of the atom types is "X".

        :return: True if wildcard, False if not
        :rtype: bool
        """
        return any([at == 'X' for at in self.atomtypes])


class DihedralType(ComparableInteractionType):
    """Improper dihedral type

    :ivar float Kchi: force constant (kJ/mol/rad^2)
    :ivar float n: multiplicity (integer)
    :ivar float delta: equilibrium dihedral angle (deg)
    """
    interactiontype = InteractionType.Dihedral
    Kchi: float
    n: int
    delta: float
    _must_equal = ['Kchi', 'delta']
    _selectors = ['n']

    def __init__(self, atomtypes: Tuple[str, str, str, str], functype: int, Kchi: float, n: int, delta: float,
                 comment: str,
                 filename: str):
        super().__init__(atomtypes, functype, comment, filename)
        self.Kchi = float(Kchi)
        self.delta = float(delta)
        self.n = int(n)

    def isPlanar(self) -> bool:
        """Check if this dihedral interaction potential forces planarity of the participating atoms"""
        if (self.delta != 0) and (self.delta != 180):
            return False
        return (self.n == 1) or (self.n == 2)

    def does_clash(self, other: "ComparableInteractionType") -> bool:
        if super().does_clash(other):
            return True
        elif (self.atomtypes_sorted == other.atomtypes_sorted) and (self.filename != other.filename):
            # we need this check, e.g. CT2-CT2-CT2-CT2 with n=1 from CHARMM22 and n=2,3,6 from CHARMM36 should not be
            # there together!
            return True
        else:
            return False

    def atomtypes_match(self, atomtypes: Tuple[str, ...]) -> bool:
        """Check if the atom types of this interaction match with the required ones.

        When checking for match, atom type 'X' in self.atomtypes (not in the `atomtypes` parameter!) is a wildcard.
        Reverse order is also accepted.

        :param atomtypes: atom type names
        :type atomtypes: tuple of str
        :return: True if matches, False if not matches
        :rtype: bool
        """
        if super().atomtypes_match(atomtypes):
            return True
        elif self.atomtypes[0] == self.atomtypes[3] == 'X':
            # wildcard type, side atoms are X, central atoms should match
            return ((self.atomtypes[1] == atomtypes[1]) and (self.atomtypes[2] == atomtypes[2])) or \
                   ((self.atomtypes[1] == atomtypes[2]) and (self.atomtypes[2] == atomtypes[1]))
        else:
            return False

    def isWildcard(self) -> bool:
        """Check if this is a wildcard dihedral type, i.e. any of the atom types is "X".

        :return: True if wildcard, False if not
        :rtype: bool
        """
        return any([at == 'X' for at in self.atomtypes])


class CmapType(ComparableInteractionType):
    """Correlation map interaction type

    :ivar int count: number of rows (and columns) in the matrix
    :ivar data: the correlation matrix data in a vector
    :type data: list of float
    """
    interactiontype = InteractionType.Cmap
    count: int
    data: List[float]

    def __init__(self, atomtypes: Tuple[str, str, str, str, str], functype: int, count: int, data: Tuple[float, ...],
                 comment: str,
                 filename: str):
        super().__init__(atomtypes, functype, comment, filename)
        self.count = int(count)
        self.data = list(data)
        self.atomtypes_sorted = self.atomtypes  # reset sorting!

    def does_clash(self, other: "CmapType") -> bool:
        """Check if two interaction types clash.

        Two interaction types are said to be clashing when they are *compatible* and they have different numeric values.

        :param other: the other interaction type
        :type other: same as this
        :return: True if clashing, False if not clashing (or incompatible)
        :rtype: bool
        """
        if not self.is_compatible(other):
            return False
        elif self.count != other.count:
            return True
        elif len(self.data) != len(other.data):
            return True
        elif any([s != o for s, o in zip(self.data, other.data)]):
            return True
        return False

    def __repr__(self) -> str:
        """Programmatic representation"""
        return f'CmapType({", ".join(self.atomtypes)}, {self.count}×{self.count} matrix,' \
               f' {len(self.data)} datapoints, from {self.filename})'


class NbfixType(ComparableInteractionType):
    """"Manual" Non-bonded interaction override

    :ivar float epsilon: Epsilon L-J parameter
    :ivar float sigma: Sigma L-J parameter
    """
    interactiontype = InteractionType.NBfix
    epsilon: float
    sigma: float
    _must_equal = ['epsilon', 'sigma']

    def __init__(self, atomtypes: Tuple[str, str], functype: int, epsilon: float, sigma: float, comment: str,
                 filename: str):
        super().__init__(atomtypes, functype, comment, filename)
        self.epsilon = float(epsilon)
        self.sigma = float(sigma)


class PairType(ComparableInteractionType):
    """Modified LJ parameters for 1-4 interactions

    :ivar float epsilon: Epsilon L-J parameter
    :ivar float sigma: Sigma L-J parameter
    """
    interactiontype = InteractionType.LJ14
    epsilon: float
    sigma: float
    _must_equal = ['epsilon', 'sigma']

    def __init__(self, atomtypes: Tuple[str, str], functype: int, epsilon: float, sigma: float, comment: str,
                 filename: str):
        super().__init__(atomtypes, functype, comment, filename)
        self.epsilon = float(epsilon)
        self.sigma = float(sigma)


class ConstraintType(ComparableInteractionType):
    """Bond length constraint type

    :ivar float d0: bond length (nm)
    """
    interactiontype = InteractionType.Constraint
    d0: float
    _must_equal = ['d0']

    def __init__(self, atomtypes: Tuple[str, str], functype: int, d0: float, comment: str, filename: str):
        super().__init__(atomtypes, functype, comment, filename)
        self.d0 = float(d0)


class NonbondedType(ComparableInteractionType):
    """Normal L-J parameters for an atom type

    :ivar float epsilon: Epsilon L-J parameter
    :ivar float sigma: Sigma L-J parameter
    :ivar float epsilon_14: Epsilon L-J parameter to be used in 1-4 interactions
    :ivar float sigma_14: Sigma L-J parameter to be used in 1-4 interactions
    """
    interactiontype = InteractionType.LJ
    epsilon: float
    sigma: float
    epsilon_14: Optional[float]
    sigma_14: Optional[float]

    _must_equal = ['atomtype', 'epsilon', 'sigma', 'epsilon_14', 'sigma_14']

    def __init__(self, atomtype: str, epsilon: float, sigma: float, epsilon_14: Optional[float],
                 sigma_14: Optional[float], comment: str, filename: str):
        super().__init__((atomtype,), 0, comment, filename)
        self.epsilon = epsilon
        self.sigma = sigma
        if ((epsilon_14 is None) and (sigma_14 is not None)) or ((epsilon_14 is not None) and (sigma_14 is None)):
            raise ValueError('epsilon_14 and sigma_14 must be together None or not None')
        self.epsilon_14 = epsilon_14
        self.sigma_14 = sigma_14

    @property
    def atomtype(self) -> str:
        """The atom type"""
        return self.atomtypes[0]

    def hasLJ14Params(self) -> bool:
        """Check if a NonBondedType has parameters for 1-4 interaction"""
        if (self.epsilon_14 is not None) and (self.sigma_14 is not None):
            return True
        elif (self.epsilon_14 is None) and (self.sigma_14 is None):
            return False
        else:
            raise ValueError('epsilon_14 and sigma_14 must both be None or non-None.')
