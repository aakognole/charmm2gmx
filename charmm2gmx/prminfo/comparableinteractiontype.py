# coding=utf-8
"""
Define classes for interaction parameter types
"""

from typing import List, Tuple
from ..utils.interactiontype import InteractionType


class ComparableInteractionType:
    """Representation of an interaction parameter set

    Interaction parameters are numeric values associated to a fixed number of atom types. E.g. in CHARMM, bonds
    are defined between two atoms and have the equilibrium bond length and the force constant as parameters.

    Interaction types are considered *compatible* when they involve the same atom types (sometimes their order can be
    reversed, e.g. for dihedrals: ABCD = DCBA), and other possible selectors (e.g. the multiplicity for proper
    dihedrals) also match.

    Compatible interaction types can be *clashing* when the numeric parameters are different.

    This class is not usable by itself (i.e. is an abstract class), but defines common facilities for its
    subclasses, e.g. BondType, AngleType, etc.

    :ivar str comment: textual description
    :ivar str filename: the name of the file from which this parameter type was loaded
    :ivar int functype: GROMACS function type number. An integer number defining the mathematical form of the potential
    :ivar atomtypes: tuple of the atom types
    :type atomtypes: tuple of strings
    :cvar _selectors: names of attributes which should be used as selectors when deciding parameter type compatibility
    :type _selectors: list of str
    :cvar _must_equal: names of attributes which hold numeric parameter values and will be used when deciding if two
                       parameters are clashing or not
    :type _must_equal: list of str
    :cvar interactiontype: the interaction type
    :type interactiontype: instance of :class:`InteractionType`
    """
    interactiontype: InteractionType
    comment: str
    filename: str
    functype: int
    atomtypes: Tuple[str, ...]
    _selectors: List[str] = []
    _must_equal: List[str] = []
    atomtypes_sorted: Tuple[str, ...]

    def __init__(self, atomtypes: Tuple[str, ...], functype: int, comment: str, filename: str):
        """Create a new interaction type

        :param atomtypes: atom type names
        :type atomtypes: tuple of str
        :param functype: GROMACS function type number
        :type functype: int
        :param comment: textual description
        :type comment: str
        :param filename: name of the file where this is defined
        :type filename: str
        """
        self.atomtypes = tuple(atomtypes)
        self.comment = comment
        self.functype = functype
        self.filename = filename
        self.atomtypes_sorted = atomtypes if atomtypes < atomtypes[::-1] else atomtypes[::-1]

    def does_clash(self, other: "ComparableInteractionType") -> bool:
        """Check if two interaction types clash.

        Two interaction types are said to be clashing when they are *compatible* and they have different numeric values.

        :param other: the other interaction type
        :type other: same as this
        :return: True if clashing, False if not clashing (or incompatible)
        :rtype: bool
        """
        if not self.is_compatible(other):
            return False
        elif any([getattr(self, sel) != getattr(other, sel) for sel in self._must_equal + ['functype', ]]):
            return True
        else:
            return False

    def print_values(self) -> str:
        """Print the comma-separated list of numeric values

        :return: the comma-separated list of numeric values
        :rtype: str
        """
        return ', '.join([str(getattr(self, attr)) for attr in self._must_equal])

    def is_compatible(self, other: "ComparableInteractionType") -> bool:
        """Decide if two interaction types are compatible.

        Two interaction types are said to be compatible if (and only if) they have the same set of atom types,
        allowing in some cases for reverasal (bonds between CT1 and CT2 should have the same parameters as between
        CT2 and CT1)

        :param other: the other interaction type
        :type other: the same as this
        :return: True if they are compatible, False if not
        :rtype: bool
        """
        if not isinstance(other, type(self)):
            raise TypeError('Cannot compare two different interaction types')
        if other.atomtypes not in [self.atomtypes, self.atomtypes[::-1]]:
            return False
        elif any([getattr(self, sel) != getattr(other, sel) for sel in self._selectors]):
            return False
        else:
            return True

    def __repr__(self) -> str:
        return f'{type(self).__name__}({", ".join(self.atomtypes)}, ' \
               f'sel: {[", ".join(["{}={}".format(n, getattr(self, n)) for n in self._selectors])]}, ' \
               f'par: {[", ".join(["{}={}".format(n, getattr(self, n)) for n in self._must_equal])]})'

    def __eq__(self, other: "ComparableInteractionType") -> bool:
        """Two interaction types are equal if they are compatible but not clashing."""
        return self.is_compatible(other) and not self.does_clash(other)

    def __ne__(self, other: "ComparableInteractionType") -> bool:
        return not (self == other)

    def atomtypes_match(self, atomtypes: Tuple[str, ...]) -> bool:
        """Check if the atom types of this interaction match with the required ones.

        Reverse order is also accepted.

        :param atomtypes: atom type names
        :type atomtypes: tuple of str
        :return: True if matches, False if not matches
        :rtype: bool
        """
        return (self.atomtypes == atomtypes) or (self.atomtypes == atomtypes[::-1])

    def __le__(self, other: "ComparableInteractionType") -> bool:
        if type(self) != type(other):
            return NotImplemented
        return self.atomtypes_sorted <= other.atomtypes_sorted

    def __ge__(self, other: "ComparableInteractionType") -> bool:
        if type(self) != type(other):
            return NotImplemented
        return self.atomtypes_sorted >= other.atomtypes_sorted

    def __lt__(self, other: "ComparableInteractionType") -> bool:
        if type(self) != type(other):
            return NotImplemented
        return self.atomtypes_sorted < other.atomtypes_sorted

    def __gt__(self, other: "ComparableInteractionType") -> bool:
        if type(self) != type(other):
            return NotImplemented
        return self.atomtypes_sorted > other.atomtypes_sorted

    def isWildcard(self) -> bool:
        """Check if this is a wildcard dihedral type, i.e. any of the atom types is "X".

        :return: True if wildcard, False if not
        :rtype: bool
        """
        raise NotImplementedError(f'{self.interactiontype.name.upper()}s cannot have wildcard atom types')

