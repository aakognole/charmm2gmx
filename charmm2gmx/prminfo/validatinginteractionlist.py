# coding=utf-8
"""List-like container class for interaction types"""
import logging
from typing import Dict, Union
from typing import List, Iterator, Tuple, Type

from .comparableinteractiontype import ComparableInteractionType
from ..utils.clashpolicy import ParameterClashPolicy

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ValidatingInteractionList:
    """A collection for storing interaction parameters of the same type

    :ivar clashpolicy: what to do when a name clash is found
    :type clashpolicy: instance of ParameterClashPolicy enum
    :ivar type: the interaction type
    :type type: subclass of ComparableInteractionType
    """
    clashpolicy: ParameterClashPolicy
    _dict: Dict[Tuple[str, ...], List[ComparableInteractionType]]
    type: Type[ComparableInteractionType]

    def __init__(self, interactionclass: Type[ComparableInteractionType],
                 clashpolicy: ParameterClashPolicy = ParameterClashPolicy.ERROR):
        self.clashpolicy = clashpolicy
        self._dict = {}
        self.type = interactionclass

    def append(self, _object: ComparableInteractionType) -> None:
        if not isinstance(_object, self.type):
            raise ValueError(f'Cannot add new parameter {repr(_object)}: different type!')
        if _object.atomtypes_sorted not in self._dict:
            self._dict[_object.atomtypes_sorted] = [_object]
            return
        elif clashing := [item for item in self._dict[_object.atomtypes_sorted] if item.does_clash(_object)]:
            if self.clashpolicy == ParameterClashPolicy.ERROR:
                raise ValueError(
                    f'Cannot add new parameter {repr(_object)}: '
                    'clashes with the following, already defined value:\n    ' +
                    repr(clashing[0])
                )
            elif self.clashpolicy == ParameterClashPolicy.KEEP_OLDER:
                logger.warning(
                    f'New parameter {repr(_object)} clashes with already present {repr(clashing[0])}:'
                    'discarding the newer one and keeping the older.'
                )
                return
            elif self.clashpolicy == ParameterClashPolicy.KEEP_NEWER:
                logger.warning(
                    f'New parameter {repr(_object)} clashes with already present {repr(clashing[0])}:'
                    'discarding the older one and adding the newer.'
                )
                for c in clashing:
                    self._dict[_object.atomtypes_sorted].remove(c)
            else:
                assert False
        self._dict[_object.atomtypes_sorted].append(_object)

    def remove(self, item: ComparableInteractionType):
        self._dict[item.atomtypes_sorted].remove(item)

    def __iter__(self) -> Iterator[ComparableInteractionType]:
        for atomtypes in self._dict:
            yield from self._dict[atomtypes]

    def __contains__(self, item: Union[ComparableInteractionType, Tuple[str, ...]]):
        if isinstance(item, ComparableInteractionType):
            return (item.atomtypes_sorted in self._dict) and (item in self._dict[item.atomtypes_sorted])
        elif isinstance(item, tuple) and all([isinstance(x, str) for x in item]):
            itemsorted = item if item < item[::-1] else item[::-1]
            return itemsorted in self._dict


    def __getitem__(self, item: Tuple[str, ...]) -> List[ComparableInteractionType]:
        return self._dict[item]

    def extend(self, other: "ValidatingInteractionList"):
        if isinstance(other, ValidatingInteractionList) and (self.type != other.type):
            raise ValueError(
                f'Cannot extend an interaction parameter list of type {self.type} with one of type {other.type}')
        elif not isinstance(other, ValidatingInteractionList):
            if any([not isinstance(x, self.type) for x in other]):
                raise ValueError(
                    f'Cannot extend an interaction parameter list of type {self.type}: some elements of the other'
                    'list are of different types.'
                )

        for item in other:
            self.append(item)
