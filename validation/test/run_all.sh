#!/bin/bash

export CHARMM=/usr/local/bin/charmm
export GMXDIR=/usr/local/gromacs/bin

# delete duplicate files instead of replacing
export GMX_MAXBACKUP=-1

ver="mar2023"       # when we ran the tests
cver="jul2022"      # CHARMM toppar version
gmxver="gmx2020"    # GROMACS version

if [ -e results_${ver}_${gmxver}.txt ]; then
    rm results_${ver}_${gmxver}.txt
fi

# Loop over all amino acids and run test energy evaluations
# Amino acids here are generated as (NH3+)-Ala-X-Ala-(COO-)
echo "=== Protein ===" > results_${ver}_${gmxver}.txt
echo "AA    CHARMM    GMX" >> results_${ver}_${gmxver}.txt
for res in ala alad arg asp asn cys glu gln gly hsd hse hsp ile leu lys met phe pro ser thr trp tyr val
do
    echo "Running ${res}..."
    if [ ! -d ${res} ]; then
        mkdir ${res}
    fi
    cd ${res}
    cp ../scripts/generate_prot.inp .
    cp ../scripts/single_point.mdp .

    # Run CHARMM
    $CHARMM resi:${res} -i generate_prot.inp -o ${res}_charmm.out
 
    # Run GROMACS
    ln -s ../../gmx/charmm36-${cver}.ff .
    $GMXDIR/gmx pdb2gmx -f ${res}.ini.pdb -p ${res}_gmx.top -water none -ff charmm36-${cver}
    # rename stupid N-terminal H atoms...
    sed "s/HT1/H1 /g" ${res}.ini.pdb > ${res}.ini.gmx.pdb
    sed -i "s/HT2/H2 /g" ${res}.ini.gmx.pdb
    sed -i "s/HT3/H3 /g" ${res}.ini.gmx.pdb

    if [ -e "conf.gro" ];
    then
        $GMXDIR/gmx editconf -f ${res}.ini.gmx.pdb -o ${res}.box.pdb -c -box 300
        $GMXDIR/gmx grompp -f single_point.mdp -c ${res}.box.pdb -p ${res}_gmx.top -o ${res}.tpr
        $GMXDIR/gmx mdrun -nt 1 -deffnm ${res} -rerun ${res}.box.pdb
        echo "Potential" | $GMXDIR/gmx energy -f ${res}.edr &> tmp_${res}.txt

        # get energies
        chener=`grep "ENER>" ${res}_charmm.out | awk '{print $3}'`
        tmpener=`grep "Potential" tmp_${res}.txt | tail -n 1 | awk '{print $2}'`
        gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l` 

        echo "${res}   ${chener}   ${gmxener}" >> ../results_${ver}_${gmxver}.txt

        # clean up
        rm tmp_${res}.txt
    fi

    cd ../
done

# New test 9/17/2022 - homotripeptides
# Here we loop over all the amino acids again but generate (NH2)-X-X-X-(COOH)
# Requires some logic to determine if some amino acids are in terminal positions or not
echo " " >> results_${ver}_${gmxver}.txt
echo "=== Protein Tripeptides ===" >> results_${ver}_${gmxver}.txt
echo "AA    CHARMM    GMX" >> results_${ver}_${gmxver}.txt
for res in ala arg asp asn cys glu gln gly hsd hse hsp ile leu lys met phe ser thr trp tyr val
do
    echo "Running ${res}3..."
    if [ ! -d ${res}3 ]; then
        mkdir ${res}3
    fi
    cd ${res}3
    cp ../scripts/generate_tripep_neutral.inp .
    cp ../scripts/single_point.mdp .

    # Run CHARMM
    $CHARMM resi:${res} -i generate_tripep_neutral.inp -o ${res}_charmm.out

    # Run GROMACS
    ln -s ../../gmx/charmm36-${cver}.ff .
    if [ ${res} = met ]; then
        # for some strange reason, GROMACS offers the MET1 and MET2 termini (methylated caps
        # for poly(ethylene-glycol) in ethers.{n,c}.tdb, completely unrelated) as the 0-th
        # entries in the lists, thus the neutral termini will have index "2".
        echo 2 2 | $GMXDIR/gmx pdb2gmx -f ${res}.ini.pdb -p ${res}_gmx.top -water none -ff charmm36-${cver} -ter
    else
        echo 1 1 | $GMXDIR/gmx pdb2gmx -f ${res}.ini.pdb -p ${res}_gmx.top -water none -ff charmm36-${cver} -ter
    fi
    # rename stupid N-terminal H atoms...
    sed "s/HT1/H1 /g" ${res}.ini.pdb > ${res}.ini.gmx.pdb
    sed -i "s/HT2/H2 /g" ${res}.ini.gmx.pdb
    sed -i "s/HT3/H3 /g" ${res}.ini.gmx.pdb

    if [ -e "conf.gro" ];
    then
        $GMXDIR/gmx editconf -f ${res}.ini.gmx.pdb -o ${res}.box.pdb -c -box 300
        # Using -maxwarn here because of the inadvertent change in HT2 -> H2 for COOH terminus...
        $GMXDIR/gmx grompp -f single_point.mdp -c ${res}.box.pdb -p ${res}_gmx.top -o ${res}.tpr -maxwarn 1
        $GMXDIR/gmx mdrun -nt 1 -deffnm ${res} -rerun ${res}.box.pdb
        echo "Potential" | $GMXDIR/gmx energy -f ${res}.edr &> tmp_${res}.txt

        # get energies
        chener=`grep "ENER>" ${res}_charmm.out | awk '{print $3}'`
        tmpener=`grep "Potential" tmp_${res}.txt | tail -n 1 | awk '{print $2}'`
        gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l`

        echo "${res}   ${chener}   ${gmxener}" >> ../results_${ver}_${gmxver}.txt

        # clean up
        rm tmp_${res}.txt
    fi

    cd ../
done

# nucleic acids: DNA
echo " " >> results_${ver}_${gmxver}.txt
echo "=== DNA ===" >> results_${ver}_${gmxver}.txt
echo "BASE  CHARMM    GMX" >> results_${ver}_${gmxver}.txt
for res in ade cyt gua thy
do
    echo "Running ${res}..."
    if [ ! -d ${res}_dna ]; then
        mkdir ${res}_dna
    fi
    cd ${res}_dna
    cp ../scripts/generate_dna.inp .
    cp ../scripts/single_point.mdp .

    # Run CHARMM
    $CHARMM resi:${res} -i generate_dna.inp -o ${res}_charmm.out

    # Run GROMACS
    ln -s ../../gmx/charmm36-${cver}.ff .

    # Since GROMACS makes life complicated, we have to do a lot of find and replace
    if [ ${res} == "ade" ]; then
        sed "s/ADE     1/DA5     1/g" ${res}.ini.pdb > ${res}.ini.gmx.pdb
        sed -i "s/ADE     2/DA      2/g" ${res}.ini.gmx.pdb
        sed -i "s/ADE     3/DA3     3/g" ${res}.ini.gmx.pdb
    fi
    if [ ${res} == "cyt" ]; then
        sed "s/CYT     1/DC5     1/g" ${res}.ini.pdb > ${res}.ini.gmx.pdb
        sed -i "s/CYT     2/DC      2/g" ${res}.ini.gmx.pdb
        sed -i "s/CYT     3/DC3     3/g" ${res}.ini.gmx.pdb
    fi
    if [ ${res} == "gua" ]; then
        sed "s/GUA     1/DG5     1/g" ${res}.ini.pdb > ${res}.ini.gmx.pdb
        sed -i "s/GUA     2/DG      2/g" ${res}.ini.gmx.pdb
        sed -i "s/GUA     3/DG3     3/g" ${res}.ini.gmx.pdb
    fi
    if [ ${res} == "thy" ]; then
        sed "s/THY     1/DT5     1/g" ${res}.ini.pdb > ${res}.ini.gmx.pdb
        sed -i "s/THY     2/DT      2/g" ${res}.ini.gmx.pdb
        sed -i "s/THY     3/DT3     3/g" ${res}.ini.gmx.pdb
    fi

    # have to manually select termini here
    # we write out a PDB file since GROMACS insists on writing a dumb atom order
    echo 6 5 | $GMXDIR/gmx pdb2gmx -f ${res}.ini.gmx.pdb -p ${res}_gmx.top -water none -ff charmm36-${cver} -ter -o conf.pdb
    # use maxwarn because H2' and H2'' are out of order; it's irrelevant
    if [ -e "conf.pdb" ];
    then
        $GMXDIR/gmx editconf -f conf.pdb -o ${res}.box.pdb -c -box 300
        $GMXDIR/gmx grompp -f single_point.mdp -c ${res}.box.pdb -p ${res}_gmx.top -o ${res}.tpr -maxwarn 1
        $GMXDIR/gmx mdrun -nt 1 -deffnm ${res} -rerun ${res}.box.pdb
        echo "Potential" | $GMXDIR/gmx energy -f ${res}.edr &> tmp_${res}.txt

        # get energies
        chener=`grep "ENER>" ${res}_charmm.out | awk '{print $3}'`
        tmpener=`grep "Potential" tmp_${res}.txt | tail -n 1 | awk '{print $2}'`
        gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l`

        echo "${res}   ${chener}   ${gmxener}" >> ../results_${ver}_${gmxver}.txt

        # clean up
        rm tmp_${res}.txt
    fi

    cd ../

done

# nucleic acids: RNA
echo " " >> results_${ver}_${gmxver}.txt
echo "=== RNA ===" >> results_${ver}_${gmxver}.txt
echo "BASE  CHARMM    GMX" >> results_${ver}_${gmxver}.txt
for res in ade cyt gua ura 
do
    echo "Running ${res}..."
    if [ ! -d ${res}_rna ]; then
        mkdir ${res}_rna
    fi
    cd ${res}_rna
    cp ../scripts/generate_rna.inp .
    cp ../scripts/single_point.mdp .

    # Run CHARMM
    $CHARMM resi:${res} -i generate_rna.inp -o ${res}_charmm.out

    # Run GROMACS
    ln -s ../../gmx/charmm36-${cver}.ff .

    # Since GROMACS patching puts H5T after O5' (not a problem with DNA, because it
    # has hard-coded residues), we have to re-order everything
    # PDB file has 2 REMARK lines, then the next two lines have to be switched
    nline=`wc -l ${res}.ini.pdb | awk '{print $1}'`
    ntail=$(($nline - 4))
    head -n 2 ${res}.ini.pdb > ${res}.ini.gmx.pdb
    head -n 4 ${res}.ini.pdb | tail -n 1 >> ${res}.ini.gmx.pdb
    head -n 4 ${res}.ini.pdb | tail -n 2 | head -n 1 >> ${res}.ini.gmx.pdb
    tail -n $ntail ${res}.ini.pdb >> ${res}.ini.gmx.pdb

    # have to manually select termini here
    echo 5 4 | $GMXDIR/gmx pdb2gmx -f ${res}.ini.pdb -p ${res}_gmx.top -water none -ff charmm36-${cver} -ter
    if [ -e "conf.gro" ];
    then
        $GMXDIR/gmx editconf -f ${res}.ini.gmx.pdb -o ${res}.box.pdb -c -box 300
        $GMXDIR/gmx grompp -f single_point.mdp -c ${res}.box.pdb -p ${res}_gmx.top -o ${res}.tpr
        $GMXDIR/gmx mdrun -nt 1 -deffnm ${res} -rerun ${res}.box.pdb
        echo "Potential" | $GMXDIR/gmx energy -f ${res}.edr &> tmp_${res}.txt

        # get energies
        chener=`grep "ENER>" ${res}_charmm.out | awk '{print $3}'`
        tmpener=`grep "Potential" tmp_${res}.txt | tail -n 1 | awk '{print $2}'`
        gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l`

        echo "${res}   ${chener}   ${gmxener}" >> ../results_${ver}_${gmxver}.txt

        # clean up
        rm tmp_${res}.txt
    fi

    cd ../

done

# Subset of lipids
echo " " >> results_${ver}_${gmxver}.txt
echo "=== LIPID ===" >> results_${ver}_${gmxver}.txt
echo "LIP   CHARMM    GMX" >> results_${ver}_${gmxver}.txt
for res in popc pope pops popa dppc
do
    echo "Running ${res}..."
    if [ ! -d ${res} ]; then
        mkdir ${res}
    fi
    cd ${res}
    cp ../scripts/generate_lipid.inp .
    cp ../scripts/single_point.mdp .

    # Run CHARMM
    $CHARMM resi:${res} -i generate_lipid.inp -o ${res}_charmm.out
 
    # Run GROMACS
    ln -s ../../gmx/charmm36-${cver}.ff .
    $GMXDIR/gmx pdb2gmx -f ${res}.ini.pdb -p ${res}_gmx.top -water none -ff charmm36-${cver} 
    
    if [ -e "conf.gro" ];
    then
        $GMXDIR/gmx editconf -f ${res}.ini.pdb -o ${res}.box.pdb -c -box 300
        $GMXDIR/gmx grompp -f single_point.mdp -c ${res}.box.pdb -p ${res}_gmx.top -o ${res}.tpr
        $GMXDIR/gmx mdrun -nt 1 -deffnm ${res} -rerun ${res}.box.pdb
        echo "Potential" | $GMXDIR/gmx energy -f ${res}.edr &> tmp_${res}.txt

        # get energies
        chener=`grep "ENER>" ${res}_charmm.out | awk '{print $3}'`
        tmpener=`grep "Potential" tmp_${res}.txt | tail -n 1 | awk '{print $2}'`
        gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l` 

        echo "${res}   ${chener}   ${gmxener}" >> ../results_${ver}_${gmxver}.txt

        # clean up
        rm tmp_${res}.txt
    fi

    cd ../
done

# Carbohydates
echo " " >> results_${ver}_${gmxver}.txt
echo "=== CARB ===" >> results_${ver}_${gmxver}.txt
echo "SUG   CHARMM    GMX" >> results_${ver}_${gmxver}.txt
for res in aglc bglc agal bgal 
do
    echo "Running ${res}..."
    if [ ! -d ${res} ]; then
        mkdir ${res}
    fi
    cd ${res}
    cp ../scripts/generate_carb.inp .
    cp ../scripts/single_point.mdp .

    # Run CHARMM
    $CHARMM resi:${res} -i generate_carb.inp -o ${res}_charmm.out
 
    # Run GROMACS
    ln -s ../../gmx/charmm36-${cver}.ff .
    $GMXDIR/gmx pdb2gmx -f ${res}.ini.pdb -p ${res}_gmx.top -water none -ff charmm36-${cver} 

    if [ -e "conf.gro" ];
    then
        $GMXDIR/gmx editconf -f ${res}.ini.pdb -o ${res}.box.pdb -c -box 300
        $GMXDIR/gmx grompp -f single_point.mdp -c ${res}.box.pdb -p ${res}_gmx.top -o ${res}.tpr
        $GMXDIR/gmx mdrun -nt 1 -deffnm ${res} -rerun ${res}.box.pdb
        echo "Potential" | $GMXDIR/gmx energy -f ${res}.edr &> tmp_${res}.txt

        # get energies
        chener=`grep "ENER>" ${res}_charmm.out | awk '{print $3}'`
        tmpener=`grep "Potential" tmp_${res}.txt | tail -n 1 | awk '{print $2}'`
        gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l` 

        echo "${res}   ${chener}   ${gmxener}" >> ../results_${ver}_${gmxver}.txt

        # clean up
        rm tmp_${res}.txt
    fi

    cd ../
done

# CGenFF
if [ ! -e cgenff_seed_atoms.str ]; then
    perl parse_rtf_write_seed.pl ../charmm/toppar_c36_${cver}/top_all36_cgenff.rtf
fi

echo " " >> results_${ver}_${gmxver}.txt
echo "=== CGenFF ===" >> results_${ver}_${gmxver}.txt
echo "AA    CHARMM    GMX" >> results_${ver}_${gmxver}.txt
for res in acet benz buta dmds etam imia mboa mp_0 oxaz urea
do
    echo "Running ${res}..."
    if [ ! -d ${res} ]; then
        mkdir ${res}
    fi
    cd ${res}
    cp ../cgenff_seed_atoms.str .
    cp ../scripts/generate_cgenff.inp .
    cp ../scripts/single_point.mdp .

    # Run CHARMM
    $CHARMM resi:${res} -i generate_cgenff.inp -o ${res}_charmm.out

    # Run GROMACS
    ln -s ../../gmx/charmm36-${cver}.ff .
    echo 1 1 | $GMXDIR/gmx pdb2gmx -f ${res}.ini.pdb -p ${res}_gmx.top -water none -ff charmm36-${cver} -ter

    if [ -e "conf.gro" ];
    then
        $GMXDIR/gmx editconf -f ${res}.ini.pdb -o ${res}.box.pdb -c -box 300
        # Using -maxwarn here because of the inadvertent change in HT2 -> H2 for COOH terminus...
        $GMXDIR/gmx grompp -f single_point.mdp -c ${res}.box.pdb -p ${res}_gmx.top -o ${res}.tpr -maxwarn 1
        $GMXDIR/gmx mdrun -nt 1 -deffnm ${res} -rerun ${res}.box.pdb
        echo "Potential" | $GMXDIR/gmx energy -f ${res}.edr &> tmp_${res}.txt

        # get energies
        chener=`grep "ENER>" ${res}_charmm.out | awk '{print $3}'`
        tmpener=`grep "Potential" tmp_${res}.txt | tail -n 1 | awk '{print $2}'`
        gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l`

        echo "${res}   ${chener}   ${gmxener}" >> ../results_${ver}_${gmxver}.txt

        # clean up
        rm tmp_${res}.txt
    fi

    cd ../
done

# water and NaCl solution
echo " " >> results_${ver}_${gmxver}.txt
echo "=== SOLUTION ===" >> results_${ver}_${gmxver}.txt
echo "SYS   CHARMM   GMX" >> results_${ver}_${gmxver}.txt
cd water
# Run CHARMM
$CHARMM -i generate.inp -o water.out

# Run GROMACS
# Special .mdp file for this system
#cp ../scripts/single_point.mdp .
$GMXDIR/gmx editconf -f water.ini.pdb -o water.box.pdb -c -box 4.0 
# using maxwarn here because of mismatch of atom names (OH2 vs. OW, etc.)
$GMXDIR/gmx grompp -f single_point.mdp -c water.box.pdb -p topol.top -o water.tpr -maxwarn 1
# We do an actual zero-step MD here because PME is not calculated correctly 
# in a rerun, it's all just Coul(SR) instead of actually having a
# mesh term
$GMXDIR/gmx mdrun -deffnm water
echo "Potential" | $GMXDIR/gmx energy -f water.edr &> tmp_water.txt

# get energies
chener=`grep "ENER>" water.out | awk '{print $3}'`
tmpener=`grep "Potential" tmp_water.txt | tail -n 1 | awk '{print $2}'`
gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l`

echo "water   ${chener}   ${gmxener}" >> ../results_${ver}_${gmxver}.txt

# clean up
rm tmp_water.txt
cd ..

cd nacl
# Run CHARMM
$CHARMM -i generate.inp -o nacl.out

# Run GROMACS
# special .mdp file for this system
#cp ../scripts/single_point.mdp .
$GMXDIR/gmx editconf -f nacl.ini.pdb -o nacl.box.pdb -c -box 4.0 
# as above, maxwarn to overcome atom name mismatches
$GMXDIR/gmx grompp -f single_point.mdp -c nacl.box.pdb -p topol.top -o nacl.tpr -maxwarn 1
# We do an actual zero-step MD here because PME is not calculated correctly 
# in a rerun, it's all just Coul(SR) instead of actually having a
# mesh term
$GMXDIR/gmx mdrun -deffnm nacl
echo "Potential" | $GMXDIR/gmx energy -f nacl.edr &> tmp_nacl.txt

# get energies
chener=`grep "ENER>" nacl.out | awk '{print $3}'`
tmpener=`grep "Potential" tmp_nacl.txt | tail -n 1 | awk '{print $2}'`
gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l`

echo "nacl   ${chener}   ${gmxener}" >> ../results_${ver}_${gmxver}.txt

# clean up
rm tmp_nacl.txt
cd ..

exit;
