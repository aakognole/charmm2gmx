#!/bin/bash

export CHARMM=/usr/local/bin/charmm
export GMXDIR=/usr/local/gromacs/bin

# delete duplicate files instead of replacing
export GMX_MAXBACKUP=-1

ver="mar2023"
cver="ljpme-jul2022"
gmxver="gmx2020"

if [ -e results_ljpme_${ver}_${gmxver}.txt ]; then
    rm results_ljpme_${ver}_${gmxver}.txt
fi


# Subset of lipids
echo " " >> results_ljpme_${ver}_${gmxver}.txt
echo "=== LIPID ===" >> results_ljpme_${ver}_${gmxver}.txt
echo "LIP   CHARMM    GMX" >> results_ljpme_${ver}_${gmxver}.txt
for res in popc pope popg dppc dppe dppg dspe lppc sopc
do
    echo "Running ${res}..."
    if [ ! -d ${res}_ljpme ]; then
        mkdir ${res}_ljpme
    fi
    cd ${res}_ljpme
    cp ../scripts/generate_lipid_ljpme.inp .
    cp ../scripts/single_point.mdp .

    # Run CHARMM
    $CHARMM resi:${res} -i generate_lipid_ljpme.inp -o ${res}_charmm.out
 
    # Run GROMACS
    ln -s ../../gmx/charmm36_${cver}.ff .
    $GMXDIR/gmx pdb2gmx -f ${res}.ini.pdb -p ${res}_gmx.top -water none -ff charmm36_${cver} 
    $GMXDIR/gmx editconf -f ${res}.ini.pdb -o ${res}.box.pdb -c -box 300
    $GMXDIR/gmx grompp -f single_point.mdp -c ${res}.box.pdb -p ${res}_gmx.top -o ${res}.tpr
    $GMXDIR/gmx mdrun -nt 1 -deffnm ${res} -rerun ${res}.box.pdb
    echo "Potential" | $GMXDIR/gmx energy -f ${res}.edr &> tmp_${res}.txt

    # get energies
    chener=`grep "ENER>" ${res}_charmm.out | awk '{print $3}'`
    tmpener=`grep "Potential" tmp_${res}.txt | tail -n 1 | awk '{print $2}'`
    gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l` 

    echo "${res}   ${chener}   ${gmxener}" >> ../results_ljpme_${ver}_${gmxver}.txt

    # clean up
    rm tmp_${res}.txt

    cd ../
done

