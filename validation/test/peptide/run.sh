#!/bin/bash

export CHARMM=/usr/local/bin/charmm
export GMXDIR=/sw/gromacs/2018/bin

# delete duplicate files instead of replacing
export GMX_MAXBACKUP=-1

ver="jul2021"
res="ala"

# run CHARMM
echo "RUNNING CHARMM"
charmm resi:${res} -i generate_prot.inp -o ${res}_charmm.out

# make atom name replacements
sed -i "s/CAY ALA     1/CH3 ACE     0/g" ala.ini.pdb
sed -i "s/ HY1 ALA     1/HH31 ACE     0/g" ala.ini.pdb
sed -i "s/ HY2 ALA     1/HH32 ACE     0/g" ala.ini.pdb
sed -i "s/ HY3 ALA     1/HH33 ACE     0/g" ala.ini.pdb
sed -i "s/CY  ALA     1/C   ACE     0/g" ala.ini.pdb
sed -i "s/OY  ALA     1/O   ACE     0/g" ala.ini.pdb

sed -i "s/NT  ALA     3/N   NME     4/g" ala.ini.pdb
sed -i "s/HNT ALA     3/HN  NME     4/g" ala.ini.pdb
sed -i "s/CAT ALA     3/CH3 NME     4/g" ala.ini.pdb
sed -i "s/ HT1 ALA     3/HH31 NME     4/g" ala.ini.pdb
sed -i "s/ HT2 ALA     3/HH32 NME     4/g" ala.ini.pdb
sed -i "s/ HT3 ALA     3/HH33 NME     4/g" ala.ini.pdb

# run GROMACS

cp ../scripts/simple.mdp .
ln -s ../../gmx/charmm36-${ver}.ff .
$GMXDIR/gmx pdb2gmx -f ${res}.ini.pdb -p ${res}_gmx.top -water none -ff charmm36-${ver} -ter
$GMXDIR/gmx grompp -f simple.mdp -c ${res}.ini.pdb -p ${res}_gmx.top -o ${res}.tpr
$GMXDIR/gmx mdrun -nt 1 -deffnm ${res} -rerun ${res}.ini.pdb
echo "Potential" | $GMXDIR/gmx energy -f ${res}.edr &> tmp_${res}.txt

# get energies
chener=`grep "ENER>" ${res}_charmm.out | awk '{print $3}'`
tmpener=`grep "Potential" tmp_${res}.txt | tail -n 1 | awk '{print $2}'`
gmxener=`echo "scale=5; ${tmpener} / 4.184" | bc -l`

echo "CHARMM: $chener GMX: $gmxener"

exit;
